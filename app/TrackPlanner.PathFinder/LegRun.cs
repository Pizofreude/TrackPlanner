﻿using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    public record  struct LegRun<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        public List<StepRun<TNodeId,TRoadId>> Steps { get; }

        public LegRun(List<StepRun<TNodeId,TRoadId>> steps)
        {
            Steps = steps;
        }
    }
    
}