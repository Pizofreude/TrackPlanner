﻿using MathUnit;
using System;

namespace TrackPlanner.PathFinder
{
    public readonly struct StepRun<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        public Placement<TNodeId,TRoadId> Place { get; }
        public TRoadId IncomingRoadMapIndex { get; } // in case of starting point it is actually the outgoing road (because there is no incoming one)
        public RoadCondition IncomingCondition { get; }
        public Length IncomingDistance { get; }
        public TimeSpan IncomingTime { get; }

        public StepRun(Placement<TNodeId,TRoadId> place, TRoadId incomingRoadMapIndex, 
            RoadCondition incomingCondition,
            Length incomingDistance, TimeSpan incomingTime)
        {
            Place = place;
            IncomingRoadMapIndex = incomingRoadMapIndex;
            IncomingCondition = incomingCondition;
            IncomingDistance = incomingDistance;
            IncomingTime = incomingTime;
        }

        public static StepRun<TNodeId,TRoadId> RecreateAsInitial(in StepRun<TNodeId,TRoadId> firstStep, 
            in StepRun<TNodeId,TRoadId> nextStep)
        {
            return new StepRun<TNodeId,TRoadId>(firstStep.Place, nextStep.IncomingRoadMapIndex, 
                nextStep.IncomingCondition, Length.Zero, TimeSpan.Zero);
        }
    }
}