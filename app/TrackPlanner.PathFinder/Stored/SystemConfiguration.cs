﻿using MathUnit;
using TrackPlanner.Elevation;
using TrackPlanner.Mapping.Stored;

namespace TrackPlanner.PathFinder.Stored
{
    public sealed record class SystemConfiguration
    {
        public Length InitSnapProximityLimit { get; set; }
        public Length FinalSnapProximityLimit { get; set; }
        public bool EnableDebugDumping { get; set; }
        public Length HighTrafficProximity { get; set; } // if cycleway is closer than given limit to the road we consider the cycleway as high traffic
        public bool DumpProgress { get; set; }
        public bool CompactPreservesRoads { get; set; }
        public MemorySettings MemoryParams { get; set; }
        public bool DumpLowCost { get; set; }
        public bool DumpTooFar { get; set; }
        public bool DumpInRange { get; set; }
        public bool DumpDangerous { get; set; }

        public SystemConfiguration()
        {
            InitSnapProximityLimit = Length.FromMeters(25);
            FinalSnapProximityLimit = Length.FromKilometers(10);
            HighTrafficProximity = Length.FromMeters(20);
            DumpProgress = false;
            MemoryParams = new MemorySettings();
            #if DEBUG
            MemoryParams.SetEnableOsmId(true);
            #endif
        }

    }
}