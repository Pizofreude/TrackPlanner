﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Geo;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct Placement<TNodeId,TRoadId> : IEquatable<Placement<TNodeId,TRoadId>>,IMapPoint
    where TNodeId: struct
    where TRoadId : struct
    {
        public static Placement<TNodeId,TRoadId> Node(TNodeId nodeId,GeoZPoint point, bool isFinal, bool isSnapped)
        {
            return new Placement<TNodeId,TRoadId>(nodeId,point, isFinal, isSnapped);
        }

        public static Placement<TNodeId,TRoadId> Prestart(GeoZPoint point, bool isFinal)
        {
            return new Placement<TNodeId,TRoadId>(point,associatedRoadId:null, PlaceKind.Prestart | PlaceKind.Snapped, isFinal);
        }
        public static Placement<TNodeId,TRoadId> UserPoint(RoadBucket<TNodeId,TRoadId> bucket)
        {
            return new Placement<TNodeId,TRoadId>(bucket.UserPoint,associatedRoadId:null, PlaceKind.UserPoint | PlaceKind.Snapped, bucket.IsFinal);
        }
        public static Placement<TNodeId,TRoadId> Crosspoint(GeoZPoint point, TRoadId roadId, bool isFinal)
        {
            return new Placement<TNodeId,TRoadId>(point,roadId, PlaceKind.Cross  | PlaceKind.Snapped, isFinal);
        }

        public static Placement<TNodeId,TRoadId> Aggregate(GeoZPoint point,TRoadId roadId)
        {
            return new Placement<TNodeId,TRoadId>(point,roadId, PlaceKind.Aggregate, isFinal:false);
        }

        public GeoZPoint Point { get; }
        private readonly TRoadId? associatedRoadId;
        private readonly PlaceKind kind;
        public bool IsUserPoint => kind.HasFlag(PlaceKind.UserPoint);
        public bool IsCross => kind.HasFlag(PlaceKind.Cross);
        public bool IsNode => kind.HasFlag(PlaceKind.Node);
        public bool IsPrestart => kind.HasFlag(PlaceKind.Prestart);
        public bool IsFinal => kind.HasFlag(PlaceKind.FinalBlob);
        public bool IsSnapped => kind.HasFlag(PlaceKind.Snapped);
        private  readonly TNodeId? nodeId;
        // consumer should check it via IsNode
        public TNodeId NodeId => this.nodeId!.Value;

        public Placement()
        {
            throw new InvalidOperationException();
        }
        
       /* public Placement(TNodeId? nodeId, GeoZPoint point, TRoadId? associatedRoadId,PlaceKind kind)
        {
            this.kind = kind;
            this.associatedRoadId = associatedRoadId;
            this.Point = point;
            this.nodeId = nodeId;
        }
        */
        private Placement(GeoZPoint point,TRoadId? associatedRoadId, PlaceKind kind, bool isFinal)
        {
            if (kind == PlaceKind.Node)
                throw new ArgumentException();
            if (isFinal)
                kind |= PlaceKind.FinalBlob;
            this.associatedRoadId = associatedRoadId;
            this.kind = kind;
            this.Point = point;
            this.nodeId = null;
        }

        private Placement(TNodeId nodeId, GeoZPoint point, bool isFinal,bool isSnapped)
        {
            if (isFinal && !isSnapped)
                throw new ArgumentException();
            
            this.kind = PlaceKind.Node;
            if (isFinal)
                this.kind |= PlaceKind.FinalBlob;
            if (isSnapped)
                this.kind |= PlaceKind.Snapped;

            this.associatedRoadId = null;
            this.Point = point;
            this.nodeId = nodeId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Point, kind, nodeId);
        }

        public override bool Equals(object? obj)
        {
            return obj is Placement<TNodeId,TRoadId> place && Equals(place);
        }

        public bool Equals(Placement<TNodeId,TRoadId> other)
        {
            return Point == other.Point &&
                   kind == other.kind &&
                   EqualityComparer<TNodeId?>.Default.Equals(nodeId, other.nodeId);
        }

        public static bool operator ==(Placement<TNodeId,TRoadId> left, Placement<TNodeId,TRoadId> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Placement <TNodeId,TRoadId>left, Placement<TNodeId,TRoadId> right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            string result;
            if (this.IsNode)
                result = $"nd:{this.NodeId}";
            else
                result = $"pt:{this.Point}";

            return $"{result} k:{this.kind}";
        }
    }


}
