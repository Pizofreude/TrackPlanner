﻿using MathUnit;
using System;
using System.Runtime.InteropServices;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct Weight : IEquatable<Weight>
    {
        public Length RemainingDistance { get; }
        public RunningWeight Current { get; }


        public Weight(RunningWeight current, Length remainingDistance)
        {
            Current = current;
            this.RemainingDistance = remainingDistance;
        }
        
        public static Weight Join(in Weight a, in Weight b)
        {
            return new Weight(RunningWeight.Join( a.Current,b.Current), Length.Zero);
        }

        public TravelCost GetTotalTimeCost(Speed estimatedSpeed)
        {
            return this.Current.TravelCost + TravelCost.Create(RemainingDistance/estimatedSpeed,costScale: 1.0) ;
        }

        public override string ToString()
        {
            return $"W{Current}:{(RemainingDistance.Kilometers.ToString("0.#"))}";
        }

        public override bool Equals(object? obj)
        {
            return obj is Weight weight && Equals(weight);
        }

        public bool Equals(Weight other)
        {
            return RemainingDistance == other.RemainingDistance
                   && Current == other.Current;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(RemainingDistance, Current);
        }

        public static bool operator ==(Weight left, Weight right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Weight left, Weight right)
        {
            return !(left == right);
        }
    }
}