﻿using System;
using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    internal readonly struct WeightSteps<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        public static WeightSteps<TNodeId, TRoadId> Infinity { get; } 
            = new WeightSteps<TNodeId, TRoadId>(ArraySegment<StepRun<TNodeId, TRoadId>>.Empty, RunningWeight.Infinity);
        
        public IReadOnlyList<StepRun<TNodeId,TRoadId>> Steps { get; }
        public RunningWeight Weight { get; }

        public WeightSteps(IReadOnlyList<StepRun<TNodeId,TRoadId>> steps, RunningWeight weight)
        {
            Steps = steps;
            Weight = weight;
        }
        public void Deconstruct(out IReadOnlyList<StepRun<TNodeId, TRoadId>> steps, out RunningWeight weight)
        {
            steps = Steps;
            weight = Weight;
        }


    }
}
