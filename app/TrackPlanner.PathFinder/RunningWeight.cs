﻿using MathUnit;
using System;
using System.Runtime.InteropServices;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct RunningWeight : IEquatable<RunningWeight>
    {
        public static RunningWeight Zero { get; } = new RunningWeight(Length.Zero, Length.Zero, 
            TravelCost.Zero);
        public static RunningWeight Infinity { get; } = new RunningWeight(Length.Zero, Length.Zero, 
            TravelCost.Infinity);
        
        public TravelCost TravelCost { get; }

        // for forbidden parts it is better to use length instead of time, because if we pick such route it is most likely we need to
        // ride around it, or walk through, so then length and not speed (asphalt/sand) is important
        public Length ForbiddenDistance { get; }
        // distanced travelled on unofficial/unstable roads (e.g. forest tracks, wild paths, etc)
        public Length UnstableDistance { get; }

        public bool IsInfinite => this.TravelCost == TravelCost.Infinity;

        public RunningWeight(Length forbiddenDistance,Length unstableDistance, TravelCost travelCost)
        {
            this.UnstableDistance = unstableDistance;
            TravelCost = travelCost;
            ForbiddenDistance = forbiddenDistance;
        }
        
        public static RunningWeight Join(in RunningWeight a, in RunningWeight b)
        {
            return new RunningWeight(a.ForbiddenDistance + b.ForbiddenDistance,
                a.UnstableDistance+b.UnstableDistance,
                a.TravelCost + b.TravelCost);
        }

        public override bool Equals(object? obj)
        {
            return obj is RunningWeight weight && Equals(weight);
        }

        public bool Equals(RunningWeight other)
        {
            return  UnstableDistance==other.UnstableDistance
                && TravelCost == other.TravelCost
                && ForbiddenDistance == other.ForbiddenDistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(UnstableDistance, TravelCost, ForbiddenDistance);
        }

        public static bool operator ==(RunningWeight left, RunningWeight right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(RunningWeight left, RunningWeight right)
        {
            return !(left == right);
        }
        
        public override string ToString()
        {
            return $"W{TravelCost.EquivalentInMinutes} F{(int)ForbiddenDistance.Meters}";
        }
    }
}