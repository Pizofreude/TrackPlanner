﻿using System.Collections.Generic;
using MathUnit;

namespace TrackPlanner.PathFinder
{
    public sealed class WeightComparer : IComparer<Weight>
    {
        private readonly Speed estimatedSpeed;
        private readonly bool useStableRoads;

        public WeightComparer(Speed estimatedSpeed,bool useStableRoads)
        {
            this.estimatedSpeed = estimatedSpeed;
            this.useStableRoads = useStableRoads;
        }

        public int Compare(Weight x, Weight y)
        {
            int comp;
            comp = x.Current.ForbiddenDistance.CompareTo(y.Current.ForbiddenDistance);
            if (comp != 0)
                return comp;
            if (this.useStableRoads)
            {
                comp = x.Current.UnstableDistance.CompareTo(y.Current.UnstableDistance);
                if (comp != 0)
                    return comp;
            }

            comp = x.GetTotalTimeCost(this.estimatedSpeed).CompareTo(y.GetTotalTimeCost(this.estimatedSpeed));
            if (comp != 0)
                return comp;

            return 0;
        }
    }
}