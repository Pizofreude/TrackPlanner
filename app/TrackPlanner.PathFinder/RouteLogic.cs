using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    public class RouteLogic<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly UserRouterPreferences userConfig;
        private readonly Speed fastest;
        private readonly IReadOnlySet<TNodeId> suppressedTraffic;
        private readonly HashSet<TNodeId> DEBUG_lowCostNodes;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;

        public RouteLogic(IWorldMap<TNodeId, TRoadId> map, IGeoCalculator calc, UserRouterPreferences userConfig,
            Speed fastest, IReadOnlySet<TNodeId> suppressedTraffic)
        {
            this.map = map;
            this.calc = calc;
            this.userConfig = userConfig;
            this.fastest = fastest;
            this.suppressedTraffic = suppressedTraffic;
            this.DEBUG_lowCostNodes = new HashSet<TNodeId>();
        }


        private bool tryGetIcomingRoadIds(Placement<TNodeId, TRoadId> current, RoadBucket<TNodeId, TRoadId> bucketA,
            RoadBucket<TNodeId, TRoadId> bucketB, [MaybeNullWhen(false)] out IEnumerable<TRoadId> roadIds)
        {
            if (current.IsUserPoint || current.IsPrestart)
            {
                roadIds = default;
                return false;
            }
            else if (current.IsNode)
            {
                roadIds = map.GetRoadsAtNode(current.NodeId).Select(it => it.RoadMapIndex);
                return true;
            }
            else if (current.IsCross)
            {
                IEnumerable<RoadSnapInfo<TNodeId, TRoadId>> snaps;

                snaps = bucketA.Where(it => current.Point == it.TrackCrosspoint);
                if (snaps.Any())
                {
                    roadIds = snaps.Select(it => it.RoadIdx.RoadMapIndex);
                    return true;
                }

                snaps = bucketB.Where(it => current.Point == it.TrackCrosspoint);
                {
                    roadIds = snaps.Select(it => it.RoadIdx.RoadMapIndex);
                    return true;
                }
            }

            throw new InvalidOperationException("Not possible");
        }

        internal SegmentInfo GetStepInfo(RoadBucket<TNodeId, TRoadId> start, RoadBucket<TNodeId, TRoadId> end,
            TRoadId? incomingRoadMapIndex,
            TRoadId connectingRoadMapIndex,
            Placement<TNodeId, TRoadId> currentPlace, Placement<TNodeId, TRoadId> targetPlace,
            bool reversed)
        {
            var target_point = targetPlace.Point;

            Length flat_step_length = calc.GetFlatDistance(currentPlace.Point.Convert(), 
                target_point.Convert());

            var connecting_road_info = this.map.GetRoad(connectingRoadMapIndex);

            bool is_forbidden = connecting_road_info.Kind <= WayKind.HighwayLink || !connecting_road_info.HasAccess;
            SpeedMode connecting_speed_mode = connecting_road_info.GetRoadSpeedMode();

            double cost_scale_factor = 1.0;

            Risk risk_info = Risk.None;

            {
                // it can be negative
                var cycleway_factor = this.userConfig.AddedCyclewayCostFactor;
                if (connecting_road_info.Kind == WayKind.Cycleway)
                {
                    if (cycleway_factor > 0) // only add this if is positive
                        cost_scale_factor += cycleway_factor;
                }
                // if we would like to promote cycleway we do this by adding penalties
                // to non-cycleways
                else if (cycleway_factor < 0)
                    cost_scale_factor -= cycleway_factor;
            }

            {
                bool is_suppressed(Placement<TNodeId, TRoadId> pl) => (pl.IsSnapped && !pl.IsFinal)
                                                                      || (pl.IsNode && suppressedTraffic.Contains(pl.NodeId));

                if (connecting_road_info.IsDangerous)
                {
                    risk_info |= Risk.Dangerous;


                    if (is_suppressed(currentPlace) && is_suppressed(targetPlace))
                    {
                        ; // default cost
                        if (currentPlace.IsNode)
                            this.DEBUG_lowCostNodes.Add(currentPlace.NodeId);
                        if (targetPlace.IsNode)
                            this.DEBUG_lowCostNodes.Add(targetPlace.NodeId);

                        risk_info |= Risk.Suppressed;
                    }
                    else
                    {
                        cost_scale_factor += this.userConfig.AddedMotorDangerousTrafficFactor;
                    }
                }
                else if (connecting_road_info.IsUncomfortable)
                {
                    risk_info |= Risk.Uncomfortable;

                    if (is_suppressed(currentPlace) && is_suppressed(targetPlace))
                    {
                        ; // default cost
                        risk_info |= Risk.Suppressed;
                    }
                    else
                    {
                        cost_scale_factor += this.userConfig.AddedMotorUncomfortableTrafficFactor;
                    }
                }
                else if (currentPlace.IsNode && targetPlace.IsNode
                                             && this.map.IsBikeFootRoadDangerousNearby( /*roadId: incomingRoadMapIndex, */nodeId: currentPlace.NodeId)
                                             && this.map.IsBikeFootRoadDangerousNearby( /*roadId: incomingRoadMapIndex, */nodeId: targetPlace.NodeId))
                {
                    risk_info |= Risk.HighTrafficBikeLane;
                    cost_scale_factor += this.userConfig.AddedBikeFootHighTrafficFactor;
                    //logger.Info($"Higher cost {cost_factor} for way {incoming_road_id}");
                }
            }

            TimeSpan added_run_time;

            // segment between user point and cross point
            bool is_snap = !currentPlace.IsNode && !targetPlace.IsNode;

            if (is_snap)
            {
                // do not mark snap as forbidden because it would start counting its length
                //    is_forbidden = false;
                // use top speed to prefer slightly longer snap to some node, instead snapping right  to the closest road and then moving by it 1-2 meters, which is absurd
                added_run_time = flat_step_length / this.fastest;
                cost_scale_factor = 1.0;
            }
            else
            {
                added_run_time = this.userConfig.GetRideTime(flat_step_length, connecting_speed_mode,
                    currentPlace.Point.Altitude*(reversed?-1:+1), target_point.Altitude*(reversed?-1:+1), out _);
            }

            TravelCost added_run_cost = TravelCost.Create(added_run_time, cost_scale_factor);

            if (!is_snap)
            {
                // crossing or joining high-traffic road
                if (this.userConfig.JoiningHighTraffic != TimeSpan.Zero
                    // todo: this is odd, we should check the road we came, and the connecting road (as future one)
                    && !connecting_road_info.IsMassiveTraffic
                    && tryGetIcomingRoadIds(targetPlace, start, end, out IEnumerable<TRoadId>? road_ids)
                    && road_ids.Any(it => this.map.GetRoad(it).IsMassiveTraffic)) // we are hitting high-traffic road
                {
                    TimeSpan join_traffic = this.userConfig.JoiningHighTraffic;
                    added_run_time += join_traffic;
                    // we add it in separate step, because cost factor of crossing is constant
                    added_run_cost += TravelCost.Create(join_traffic, 1.0);
                }
                else if (incomingRoadMapIndex != null
                         && !this.map.IsRoadContinuation(incomingRoadMapIndex.Value, connectingRoadMapIndex))
                {
                    added_run_cost += TravelCost.Create(this.userConfig.AddedRoadSwitchingCostValue, 1.0);
                }
            }


            return (SegmentInfo.Create(flat_step_length, isForbidden: is_forbidden, isStable: connecting_road_info.Kind.IsStable(), isSnap: is_snap) with
                {
                    RiskInfo = risk_info,
                    Cost = added_run_cost,
                    Time = added_run_time,
                })
                .WithSpeedMode(connecting_speed_mode)
                ;
        }
    }
}