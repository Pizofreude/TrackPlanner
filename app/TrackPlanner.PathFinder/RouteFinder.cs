﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using TrackPlanner.Shared;
using TrackRadar.Collections;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using SystemConfiguration = TrackPlanner.PathFinder.Stored.SystemConfiguration;

namespace TrackPlanner.PathFinder
{
    // A* algorithm with pairing heap
    // https://en.wikipedia.org/wiki/A*_search_algorithm
    // https://brilliant.org/wiki/pairing-heap/
    // https://en.wikipedia.org/wiki/Pairing_heap
    // http://www.brouter.de/brouter/algorithm.html

    public sealed class RouteFinder<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public static bool TryFindRoute(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            SystemConfiguration sysConfig,
            UserRouterPreferences userConfig, IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken cancellationToken,
            [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> routeSteps,out RunningWeight routeWeight,
            out string? problem)
        {
            var buckets = map.Grid.GetRoadBuckets(userPoints, sysConfig.InitSnapProximityLimit,
                sysConfig.FinalSnapProximityLimit,
                requireAllHits: true, singleMiddleSnaps: true);
            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map, 
                sysConfig, userConfig, buckets, cancellationToken);
            var result = finder.tryFindPath(buckets, out routeSteps,out routeWeight);
            problem = finder.problemMessage;
            return result;
        }


        public static bool TryFindPath(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            SystemConfiguration sysConfig,
            UserRouterPreferences userConfig, IReadOnlyList<MapPoint<TNodeId>> userPoints,
            bool allowSmoothing,
            CancellationToken cancellationToken,
            [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> routeSteps,
            out RunningWeight routeWeight)
        {
            var buckets = new List<RoadBucket<TNodeId, TRoadId>>();
            for (int i = 0; i < userPoints.Count; ++i)
            {
                var is_final = i == 0 || i == userPoints.Count - 1;

                if (userPoints[i].IsNode())
                    buckets.Add(RoadBucket.CreateBucket<TNodeId, TRoadId>(i, userPoints[i].NodeId!.Value, map, map.Grid.Calc,
                        isFinal: is_final, allowSmoothing: allowSmoothing));
                else
                    buckets.Add(map.Grid.GetRoadBucket(i, userPoints[i].Point, sysConfig.InitSnapProximityLimit, sysConfig.FinalSnapProximityLimit,
                        requireAllHits: true,
                        singleSnap: !is_final,
                        isFinal: is_final,
                        allowSmoothing: allowSmoothing)!);
            }

            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map,  sysConfig,
                userConfig, buckets, cancellationToken);
            return finder.tryFindPath(buckets, out routeSteps,out routeWeight);
        }

        public static bool TryFindPath(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            SystemConfiguration sysConfig,
            UserRouterPreferences userConfig, IReadOnlyList<TNodeId> mapNodes,
            bool allowSmoothing,
            CancellationToken cancellationToken,
            [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> routeSteps,
            out RunningWeight routeWeight)
        {
            var buckets = RoadBucket.GetRoadBuckets(mapNodes, map, map.Grid.Calc, allowSmoothing: allowSmoothing);

            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map, sysConfig, userConfig,
                buckets, cancellationToken);
            return finder.tryFindPath(buckets, out routeSteps,out routeWeight);
        }


        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly SystemConfiguration sysConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly IReadOnlySet<TNodeId> suppressedHighTraffic;
        private readonly CancellationToken cancellationToken;
        private string? problemMessage;

        private string? debugDirectory => this.navigator.GetDebug(this.sysConfig.EnableDebugDumping);

        private readonly HashSet<TNodeId> DEBUG_lowCostNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressTooFar;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressInRange;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;

        private readonly RouteLogic<TNodeId, TRoadId> logic;

        private readonly Speed slowest;

        private readonly Speed fastest;
        //private readonly DistancePredictor predictor;

        private RouteFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            SystemConfiguration sysConfig,
            UserRouterPreferences userConfig,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets,
            CancellationToken cancellationToken)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.map = map;
            this.sysConfig = sysConfig;
            this.userConfig = userConfig;
            if (!userConfig.Speeds.Values.SelectMany(it => new[] {it.Max, it.Regular}).TryMinMax(out this.slowest, out this.fastest))
                throw new ArgumentException("There are no speeds given.");
            this.cancellationToken = cancellationToken;

            this.DEBUG_lowCostNodes = new HashSet<TNodeId>();
            this.DEBUG_suppressTooFar = new Dictionary<TNodeId, string>();
            this.DEBUG_suppressInRange = new Dictionary<TNodeId, string>();
            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            this.suppressedHighTraffic = suppressTrafficCost(buckets, userConfig.TrafficSuppression);

            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, this.fastest, this.suppressedHighTraffic);
        }

        private bool tryFindPath(List<RoadBucket<TNodeId, TRoadId>> buckets,
            [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> routeLegs, out RunningWeight routeWeight)
        {
            if (buckets.Count < 2)
            {
                logger.Error($"Insufficient number of anchors were given: {buckets.Count}");
                routeLegs = default;
                routeWeight = default;
                return false;
            }

            var precise_comparer = new WeightComparer(estimatedSpeed: this.fastest, 
                useStableRoads: this.userConfig.UseStableRoads);

            routeLegs = new List<LegRun<TNodeId, TRoadId>>();
            routeWeight = RunningWeight.Zero;
            
            for (int pt_index = 1; pt_index < buckets.Count; ++pt_index)
            {
                WeightSteps<TNodeId, TRoadId>? best_path = null;

                using (var debugFinderHistory = this.sysConfig.DumpProgress
                           ? new DebugFinderHistory<TNodeId, TRoadId>(this.logger, this.map, "normal-fwd", this.debugDirectory)
                           : null)
                using (var finderHistory = this.sysConfig.DumpProgress
                           ? new DebugFinderHistory<TNodeId, TRoadId>(this.logger, this.map, "normal-bwd", this.debugDirectory)
                           : null)
                {
                    if (tryFindLegPath(debugFinderHistory,
                            finderHistory,
                            precise_comparer, buckets[pt_index - 1], buckets[pt_index],
                            "proper-find",
                            out CompStatistics cost_stats, out WeightSteps<TNodeId, TRoadId> cost_path))
                    {
                        best_path = cost_path;
                        // 652_998
                        this.logger.Info(
                            $"Proper path cost {best_path.Value.Weight.TravelCost.EquivalentInMinutes} with {cost_stats.ForwardUpdateCount + cost_stats.BackwardUpdateCount} updates, sub-success {cost_stats.SuccessExactTarget}, sub-fails {cost_stats.FailedExactTarget}");
                    }
                    else if (best_path == null)
                    {
                        routeLegs = null;
                        routeWeight = default;
                        return false;
                    }
                }


                var path = best_path.Value.Steps.ToList();

                // remove user points
                if (!path[0].Place.IsUserPoint)
                    throw new Exception($"Path does not start with user point {path[0].Place}.");
                if (path[0].Place.Point != buckets[pt_index - 1].UserPoint)
                    throw new Exception($"Path start out of sync with bucket {path[0].Place.Point} != {buckets[pt_index - 1].UserPoint}.");
                path.RemoveAt(0);
                path[0] = StepRun<TNodeId, TRoadId>.RecreateAsInitial(path[0], path[1]);
                if (!path[0].Place.IsCross)
                    throw new Exception($"Path start does not follow with cross point {path[0].Place}.");
                if (!path[1].Place.IsNode)
                    throw new Exception($"Path start does not follow with node {path[1].Place}.");

                if (!path[^1].Place.IsUserPoint)
                    throw new Exception($"Path does not end with user point {path[^1].Place}.");
                if (path[^1].Place.Point != buckets[pt_index].UserPoint)
                    throw new Exception($"Path end out of sync with bucket {path[^1].Place.Point} != {buckets[pt_index].UserPoint}.");
                path.RemoveLast();
                if (!path[^1].Place.IsCross)
                    throw new Exception($"Path end does not follow with cross point {path[^1].Place}.");
                if (!path[^2].Place.IsNode)
                    throw new Exception($"Path end does not follow with node {path[^2].Place}.");

                if (path.First().IncomingDistance != Length.Zero || path.First().IncomingTime != TimeSpan.Zero)
                    throw new ArgumentOutOfRangeException($"Initial step should be zero, it is {path.First().IncomingDistance} in {path.First().IncomingTime}");
                routeLegs.Add(new LegRun<TNodeId, TRoadId>(path));
                routeWeight = RunningWeight.Join(routeWeight, best_path.Value.Weight);

                // todo: INCORRECT, it won't give optimal path, fix it 
                // to avoid gaps at the anchors, like this
                // X----xo x----X
                // where X -- user point and cross point at the same time, x -- crosspoint, o -- user point
                // this hack was added, we simply use single snap which was used when finding current path leg
                buckets[pt_index] = buckets[pt_index].RebuildWithSingleSnap(path[^1].Place.Point.Convert(),
                    path[^2].Place.NodeId);
            }

            // note: adjacent legs have shared place
            smoothLegs(buckets, routeLegs);

            dumpPostDebug();

            return true;
        }

        private void smoothLegs(IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets, List<LegRun<TNodeId, TRoadId>> rawLegs)
        {
            int bucket_idx = 0;
            // for looped request do not smooth out start/end and do not force start/end point to be exactly the same
            foreach (var (prev, next) in rawLegs.Slide()) // NOTE: in case of single leg this loop won't run at all... 
            {
                ++bucket_idx;
                smoothLegs(buckets[bucket_idx], prev, next);

                if (prev.Steps.Last().Place != next.Steps.First().Place)
                    this.problemMessage ??= $"Legs {bucket_idx} are not connected";
            }

            // ... so we have to have separate loop just for validation
            foreach (var raw_leg in rawLegs)
            {
                if (raw_leg.Steps.First().IncomingDistance != Length.Zero || raw_leg.Steps.First().IncomingTime != TimeSpan.Zero)
                    this.problemMessage ??= $"Initial step should be zero, it is {raw_leg.Steps.First().IncomingDistance} in {raw_leg.Steps.First().IncomingTime}";
            }
        }

        private void smoothLegs(RoadBucket<TNodeId, TRoadId> previousBucket, in LegRun<TNodeId, TRoadId> previousLeg,
            in LegRun<TNodeId, TRoadId> nextLeg)
        {
            if (!previousBucket.AllowSmoothing)
                return;

            // each legs is stripped from start/end user points already

            // smoothing connections between legs

            // the first and last points are crosspoint, the second to them are real nodes
            // if we share the same node skip both crosspoints and starting (!) shared nodes, to avoid diagrams like this
            //           | 
            //           |
            //  ---------o--* U
            // o node
            // * crosspoint
            // U user point
            // we don't check more shared nodes, because this removal is neglible, but in general user could go to some place and return
            // using partially of the same path

            // the first execution of this condition is when we still have crosspoints at the end/start
            while (EqualityComparer<TNodeId>.Default.Equals(previousLeg.Steps[^2].Place.NodeId, nextLeg.Steps[1].Place.NodeId))
            {
                previousLeg.Steps.RemoveLast();
                nextLeg.Steps.RemoveFirst();
                nextLeg.Steps[0] = StepRun<TNodeId, TRoadId>.RecreateAsInitial(nextLeg.Steps[0], nextLeg.Steps[1]);

                // keep removing same nodes as long the nodes were in snap-range 
                if (previousLeg.Steps.Count < 2 || nextLeg.Steps.Count < 2
                                                // the current one has to be within snap
                                                || !previousBucket.ReachableNodes.Contains(previousLeg.Steps[^1].Place.NodeId))
                    break;
            }
        }

        private void dumpPostDebug()
        {
            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpLowCost)
            {
                var input = new TrackWriterInput();
                foreach (var node in DEBUG_lowCostNodes)
                    input.AddPoint(map.GetPoint(node), icon: PointIcon.DotIcon);

                string filename = DiskHelper.GetUniquePath(this.navigator.GetDebug(), $"low-cost.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpTooFar)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressTooFar)
                {
                    if (!this.DEBUG_suppressInRange.ContainsKey(entry.Key))
                        input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.CircleIcon);
                }

                string filename = DiskHelper.GetUniquePath(this.debugDirectory!, $"too-far.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpInRange)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressInRange)
                    input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.DotIcon);

                string filename = DiskHelper.GetUniquePath(this.debugDirectory!, $"in-range.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpDangerous)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_dangerousNodes)
                    input.AddPoint(map.GetPoint(entry), label: "noname", icon: PointIcon.DotIcon);

                string filename = DiskHelper.GetUniquePath(this.debugDirectory!, $"dangerous.kml");
                input.BuildDecoratedKml().Save(filename);
            }
        }


        private List<StepRun<TNodeId, TRoadId>> reversePathDirection(IReadOnlyList<StepRun<TNodeId, TRoadId>> path)
        {
            var result = new List<StepRun<TNodeId, TRoadId>>(capacity: path.Count);

            Length incoming_distance = Length.Zero;
            TimeSpan incoming_time = TimeSpan.Zero;

            // the first point does not havy any incoming road so we copy data for this from the next point
            var incoming_road_map_index = path.Last().IncomingRoadMapIndex;
            RoadCondition incoming_condition = path.Last().IncomingCondition;

            for (int i = path.Count - 1; i >= 0; --i)
            {
                result.Add(new StepRun<TNodeId, TRoadId>(path[i].Place, incoming_road_map_index, incoming_condition,
                    incoming_distance, incoming_time));

                incoming_distance = path[i].IncomingDistance;
                incoming_time = path[i].IncomingTime;
                incoming_road_map_index = path[i].IncomingRoadMapIndex;
                incoming_condition = path[i].IncomingCondition;
            }

            return result;
        }

        private bool tryFindLegPath(
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            WeightComparer weightComparer,
            RoadBucket<TNodeId, TRoadId> stageStart, RoadBucket<TNodeId, TRoadId> stageEnd,
            string modeLabel,
            out CompStatistics stats,
            out WeightSteps<TNodeId, TRoadId> resultPath)
        {
            stats = new CompStatistics();
            logger.Info($"Start at legal {stageStart.Any(it => this.map.GetRoad(it.RoadIdx.RoadMapIndex).HasAccess)}, end at legal {stageEnd.Any(it => this.map.GetRoad(it.RoadIdx.RoadMapIndex).HasAccess)}");

            int rejected = 0;

            // node id -> source node id
            var forward_backtrack = new Dictionary<Placement<TNodeId, TRoadId>, (BacktrackInfo<TNodeId, TRoadId> info, Weight weight)>();
            var backward_backtrack = new Dictionary<Placement<TNodeId, TRoadId>, (BacktrackInfo<TNodeId, TRoadId> info, Weight weight)>();

            // node id -> ESTIMATE length (i.e. current length + direct distance), info
            var forward_heap = MappedPairingHeap.Create<Placement<TNodeId, TRoadId>, Weight, 
                BacktrackInfo<TNodeId, TRoadId>>(keyComparer: null, weightComparer: weightComparer);
            var backward_heap = MappedPairingHeap.Create<Placement<TNodeId, TRoadId>, Weight, 
                BacktrackInfo<TNodeId, TRoadId>>(keyComparer: null, weightComparer: weightComparer);

            {
                Length remaining_direct_distance = calc.GetDistance(stageStart.UserPoint, stageEnd.UserPoint);

                forward_heap.TryAddOrUpdate(Placement<TNodeId, TRoadId>.UserPoint(stageStart),
                    new Weight(new RunningWeight( forbiddenDistance: Length.Zero, unstableDistance: Length.Zero,
                        travelCost: TravelCost.Zero),
                        remainingDistance: remaining_direct_distance),
                    new BacktrackInfo<TNodeId, TRoadId>(source: Placement<TNodeId, TRoadId>.Prestart(new GeoZPoint(), stageStart.IsFinal),
                        null,
                        null,
                        Length.Zero,
                        TimeSpan.Zero));

                backward_heap.TryAddOrUpdate(Placement<TNodeId, TRoadId>.UserPoint(stageEnd),
                    new Weight(new RunningWeight( forbiddenDistance: Length.Zero, unstableDistance: Length.Zero,
                        travelCost: TravelCost.Zero),
                        remainingDistance: remaining_direct_distance),
                    new BacktrackInfo<TNodeId, TRoadId>(source: Placement<TNodeId, TRoadId>.Prestart(new GeoZPoint(), stageEnd.IsFinal),
                        null,
                        null,
                        Length.Zero,
                        TimeSpan.Zero));
            }

            stats.ForwardUpdateCount += 1;
            stats.BackwardUpdateCount += 1;

            bool is_forward_side = true;

            (Placement<TNodeId, TRoadId> place, Weight weight)? joint = null;

            while (true)
            {
                this.cancellationToken.ThrowIfCancellationRequested();

                is_forward_side = !is_forward_side;

                var backtrack = is_forward_side ? forward_backtrack : backward_backtrack;
                var opposite_backtrack = is_forward_side ? backward_backtrack : forward_backtrack;
                var heap = is_forward_side ? forward_heap : backward_heap;
                var start_bucket = is_forward_side ? stageStart : stageEnd;
                var end_bucket = is_forward_side ? stageEnd : stageStart;

                if (false && backtrack.Count % 1_000 == 0)
                {
                    this.logger.Verbose($"Routed through {backtrack.Count} places");
                }

                if (!heap.TryPop(out Placement<TNodeId, TRoadId> current_place, 
                        out Weight current_weight,
                        out BacktrackInfo<TNodeId, TRoadId> current_info))
                {
                    if (joint == null)
                    {
                        this.logger.Info($"Finding path in {modeLabel} failed , fwd: {stats.ForwardUpdateCount}, bwd {stats.BackwardUpdateCount}");
                        resultPath = default;
                        return false;
                    }
                    else
                    {
                        var forward_steps = recreatePath(forward_backtrack, Placement<TNodeId, TRoadId>.UserPoint(stageStart), joint.Value.place);
                        var backward_steps = recreatePath(backward_backtrack, Placement<TNodeId, TRoadId>.UserPoint(stageEnd), joint.Value.place);
                        // skip first place, because it is shared with the result
                        forward_steps.AddRange(reversePathDirection(backward_steps).Skip(1));

                        resultPath = new WeightSteps<TNodeId, TRoadId>(forward_steps, weight: joint.Value.weight.Current);

                        this.logger.Info($"We have joint in {modeLabel}");

                        return true;
                    }
                }

                /*{
                    var refreshed_cost = computePredictedTimeCost(current_info.DirectDistanceToEnd);
                    if (refreshed_cost != current_weight.PredictedRemainingTimeCost)
                    {
                        if (!heap.TryAddOrUpdate(current_place,
                                new Weight(current_weight.RunningTimeCost, refreshed_cost, current_weight.RunningForbidden), current_info))
                            throw new Exception("Unable to refresh heap");
                        continue;
                    }
                }*/

                if (is_forward_side)
                {
                    forwardDebugHistory?.Add(current_place, current_weight, current_info);
                }
                else
                {
                    backwardDebugHistory?.Add(current_place, current_weight, current_info);
                }

                backtrack.Add(current_place, (current_info, current_weight));

                if (current_place.IsNode && map.GetRoad(current_info.IncomingRoadId!.Value).IsDangerous)
                {
                    this.DEBUG_dangerousNodes.Add(current_place.NodeId);
                }

                {
                    if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                    {
                        logger.Info($"Coming to hot node {current_place.NodeId}/{comment} using road {current_info.IncomingRoadId}");
                    }
                }

                //if (backtrack.Count % 1_000 == 0)
                //  logger.Info($"Backtrack size {backtrack.Count}");

                // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point

                if (current_place.IsUserPoint && current_place.Point == end_bucket.UserPoint)
                {
                    // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point
                    var route_steps = recreatePath(backtrack, Placement<TNodeId, TRoadId>.UserPoint(start_bucket), 
                        current_place);
                    if (!is_forward_side)
                        route_steps = reversePathDirection(route_steps);
                    resultPath = new WeightSteps<TNodeId, TRoadId>(route_steps, weight: current_weight.Current);

                    logger.Info($"BOOM, direct hit with weight {current_weight} in {modeLabel}");
                    forwardDebugHistory?.DumpLastData();
                    backwardDebugHistory?.DumpLastData();
                    stats.RejectedNodes = rejected;

                    return true;
                }
                else if (opposite_backtrack.TryGetValue(current_place, out var opposite_info))
                {
                    this.logger.Info("Joint point found");
                    var new_joined_weight = Weight.Join(opposite_info.weight, current_weight);
                    if (joint == null || weightComparer.Compare(new_joined_weight, joint.Value.weight) < 0)
                        joint = (current_place, new_joined_weight);
                }

                int adjacent_count = 0;

                foreach ((var adj_place, TRoadId connecting_road_map_index) in getAdjacent(current_place, start_bucket, end_bucket))
                {
                    ++adjacent_count;

                    if (backtrack.ContainsKey(adj_place))
                    {
                        if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                        {
                            logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is already used by outgoing road {connecting_road_map_index}@{adj_place.NodeId}");
                        }

                        continue;
                    }

                    Length? remaining_direct_distance = null;
                    {
                        if (heap.TryGetData(adj_place, out var adj_weight, out var adj_info))
                        {
                            remaining_direct_distance = adj_weight.RemainingDistance;
                        }
                    }

                    // after initial join, we work only in join mode, meaning we accepts only updates on our side, and we
                    // need to hit the opposite already fixed places
                    if (joint != null && (remaining_direct_distance == null || !opposite_backtrack.ContainsKey(adj_place)))
                        continue;

                    if (remaining_direct_distance == null)
                    {
                        if (adj_place.IsNode && this.userConfig.HACK_ExactToTarget)
                        {
                            var adj_bucket = RoadBucket.GetRoadBuckets(new[] {adj_place.NodeId}, map, this.map.Grid.Calc, allowSmoothing: false).Single();

                            var sub_buckets = new List<RoadBucket<TNodeId, TRoadId>>() {adj_bucket, end_bucket};
                            var worker = new RouteFinder<TNodeId, TRoadId>(logger, this.navigator, map, 
                                sysConfig with {DumpProgress = false},
                                new UserRouterPreferences() {HACK_ExactToTarget = false}.SetUniformSpeeds(),
                                sub_buckets, cancellationToken);
                            if (worker.tryFindPath(sub_buckets, out List<LegRun<TNodeId, TRoadId>>? remaining,out _))
                            {
                                ++stats.SuccessExactTarget;
                                remaining_direct_distance = Length.FromMeters(remaining.SelectMany(x => x.Steps).Sum(it => it.IncomingDistance.Meters));
                            }
                            else
                            {
                                //throw new InvalidOperationException($"Sub-path failed from n#{adj_place.NodeId}@{this.map.Nodes[adj_place.NodeId.Value]} to {end}");
                                ++stats.FailedExactTarget;
                            }
                        }

                        if (remaining_direct_distance == null)
                            remaining_direct_distance = calc.GetFlatDistance(adj_place.Point.Convert(),
                                end_bucket.UserPoint.Convert());
                    }

                    var step_info = this.logic.GetStepInfo(start_bucket, end_bucket, current_info.IncomingRoadId,
                        connecting_road_map_index, current_place, adj_place, reversed: !is_forward_side);

                    Length new_run_dist = current_info.RunningRouteDistance + step_info.SegmentLength;
                    Length new_forbidden_dist = current_weight.Current.ForbiddenDistance + step_info.ForbiddenLength;
                    Length new_unstable_dist = current_weight.Current.UnstableDistance + step_info.UnstableLength;

                    Weight outgoing_weight = new Weight(new RunningWeight(forbiddenDistance: new_forbidden_dist,
                        unstableDistance: new_unstable_dist,
                        travelCost: current_weight.Current.TravelCost + step_info.Cost),
                        remainingDistance: remaining_direct_distance.Value);

                    {
                        var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(current_place,
                            connecting_road_map_index,
                            new RoadCondition(step_info.SpeedMode, step_info.RiskInfo, step_info.IsForbidden, isSnap: step_info.IsSnap),
                            new_run_dist,
                            current_info.RunningTime + step_info.Time);

                        bool updated = heap.TryAddOrUpdate(adj_place, outgoing_weight, outgoing_info);
                        if (is_forward_side)
                            ++stats.ForwardUpdateCount;
                        else
                            ++stats.BackwardUpdateCount;

                        {
                            if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                            {
                                logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is by outgoing road {connecting_road_map_index}@{adj_place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoing_weight}, updated {updated}");
                            }
                        }
                    }
                }

                stats.AddNode(degree: adjacent_count);
            }
        }


        /*        private TimeCost computePredictedTimeCost(Length directDistance)
        {
            return new TimeCost(directDistance / this.userConfig.Fastest, costFactor: 1.0);
        }
*/
        private List<StepRun<TNodeId, TRoadId>> recreatePath(
            IReadOnlyDictionary<Placement<TNodeId, TRoadId>, (BacktrackInfo<TNodeId, TRoadId> info, Weight weight)> backtrack,
            Placement<TNodeId, TRoadId> startPlace, Placement<TNodeId, TRoadId> endPlace)
        {
            if (endPlace == startPlace)
                throw new ArgumentException("Same place");

            var result_path = new List<StepRun<TNodeId, TRoadId>>();

            Placement<TNodeId, TRoadId> current_place = endPlace;
            var current_info = backtrack[current_place].info;

            var last_incoming_road_id = current_info.IncomingRoadId!.Value;
            RoadCondition last_incoming_road_condition = current_info.IncomingCondition!.Value;
            Length total_routing_distance = current_info.RunningRouteDistance;

            while (true)
            {
                var source_place = current_info.Source;
                BacktrackInfo<TNodeId, TRoadId> source_info;

                if (source_place.IsPrestart)
                {
                    source_info = new BacktrackInfo<TNodeId, TRoadId>(source_place, null, null,
                        Length.Zero, TimeSpan.Zero);
                }
                else
                {
                    source_info = backtrack[source_place].info;
                }

                //if (!current_place.IsUserPoint) // do not add user start/end point
                {
                    var step_distance = current_info.RunningRouteDistance - source_info.RunningRouteDistance;
                    var step_time = current_info.RunningTime - source_info.RunningTime;
                    result_path.Add(new StepRun<TNodeId, TRoadId>(current_place,
                        current_info.IncomingRoadId ?? last_incoming_road_id, // null is only for segment from pre-start to starting point,
                        current_info.IncomingCondition ?? last_incoming_road_condition,
                        step_distance,
                        step_time));
                }

                if (current_place == startPlace)
                {
                    // remove user points, so the route is always bound to real roads
                    // NOTE: the starting/ending point is not added at all, so we don't have to remove it

                    result_path.Reverse();
                    var init_step_distance = result_path.First().IncomingDistance;
                    var init_step_time = result_path.First().IncomingTime;
                    if (init_step_distance != Length.Zero || init_step_time != TimeSpan.Zero)
                        throw new ArgumentOutOfRangeException($"Initial step {TrackPlanner.Shared.Data.DataFormat.FormatDistance(init_step_distance, true)} in {TrackPlanner.Shared.Data.DataFormat.Format(init_step_time)}, expected zero.");

                    return result_path;
                }

                last_incoming_road_id = current_info.IncomingRoadId!.Value;
                last_incoming_road_condition = current_info.IncomingCondition!.Value;

                current_place = source_place;
                current_info = source_info;
            }
        }

        private void addAdjacentToPoint(Dictionary<Placement<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> place, RoadBucket<TNodeId, TRoadId> bucket)
        {
            if (place.IsUserPoint && place.Point == bucket.UserPoint)
            {
                foreach (var snap in bucket)
                    adjacent.TryAdd(Placement<TNodeId, TRoadId>.Crosspoint(snap.TrackCrosspoint, snap.RoadIdx.RoadMapIndex, place.IsFinal), snap.RoadIdx.RoadMapIndex);
            }

            foreach (var snap in bucket)
                if (place.IsCross && place.Point == snap.TrackCrosspoint)
                {
                    var snap_node = this.map.GetNode(snap.RoadIdx);
                        adjacent.TryAdd(Placement<TNodeId, TRoadId>.Node(snap_node, this.map.GetPoint(snap_node), 
                            place.IsFinal, isSnapped: true), snap.RoadIdx.RoadMapIndex);
                    adjacent.TryAdd(Placement<TNodeId, TRoadId>.UserPoint(bucket), snap.RoadIdx.RoadMapIndex);
                }
        }

        private IEnumerable<(Placement<TNodeId, TRoadId> point, TRoadId roadId)> getAdjacent(Placement<TNodeId, TRoadId> current,
            RoadBucket<TNodeId, TRoadId> bucketA, RoadBucket<TNodeId, TRoadId> bucketB)
        {
            // point -> incoming road id
            var adjacent = new Dictionary<Placement<TNodeId, TRoadId>, TRoadId>();
            if (current.IsNode)
            {
                addAdjacentCrosspointsToNode(adjacent, current, bucketA);
                addAdjacentCrosspointsToNode(adjacent, current, bucketB);

                bool has_adjacent_crosspoints = adjacent.Count > 0;

                foreach (var adj_road_idx in map.GetAdjacentRoads(current.NodeId))
                {
                    var adj_node_id = this.map.GetNode(adj_road_idx);

                    bool is_in_a = bucketA.Contains(adj_road_idx);
                    bool is_in_b = bucketB.Contains(adj_road_idx);
                    bool is_final = (bucketA.IsFinal && is_in_a) || (bucketB.IsFinal && is_in_b);
                    var is_snapped = is_in_a || is_in_b;
                    /*if (!has_adjacent_crosspoints && !is_snapped)
                    {
                        // todo: jak tylko bedziemy mieli pamiec, po prostu zwracac tu ireadonlylist i sprawdz count,
                        // chodzi o to, ze musimy miec wylacznie jedna droge
                        if (!this.map.GetRoads(this.map.GetNode(adj_road_idx)).Skip(1).Any())
                        {
                            var current_road_indices = this.map.GetRoads(current.NodeId.Value).Where(it => it.RoadMapIndex == adj_road_idx.RoadMapIndex).ToArray();
                            if (current_road_indices.Length == 1)
                            {
                                
                            }
                        }
                    }*/
                    adjacent.TryAdd(Placement<TNodeId, TRoadId>.Node(adj_node_id, this.map.GetPoint(adj_node_id), is_final,
                        isSnapped: is_snapped), adj_road_idx.RoadMapIndex);
                }
            }
            else
            {
                addAdjacentToPoint(adjacent, current, bucketA);
                addAdjacentToPoint(adjacent, current, bucketB);
            }

            return adjacent.Select(it => (it.Key, it.Value));
        }


        private void addAdjacentCrosspointsToNode(Dictionary<Placement<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> place,
            RoadBucket<TNodeId, TRoadId> bucket)
        {
            foreach (var snap in bucket)
            {
                if (EqualityComparer<TNodeId>.Default.Equals(place.NodeId, snap.NodeId))
                {
                    adjacent.TryAdd(Placement<TNodeId, TRoadId>.Crosspoint(snap.TrackCrosspoint,
                        snap.RoadIdx.RoadMapIndex, place.IsFinal), snap.RoadIdx.RoadMapIndex);
                }
            }
        }

        private IReadOnlySet<TNodeId> suppressTrafficCost(IEnumerable<RoadBucket<TNodeId, TRoadId>> buckets, Length suppressionRange)
        {
            var suppressed = new HashSet<TNodeId>();

            if (suppressionRange != Length.Zero)
            {
                // assume that for all mid-points user selected, she/he is aware that she/he places point on a high-traffic road
                // in such case assume that for given distance user is OK to ride on such rode, so do not add any "penalties"
                // while searching for a route
                foreach (var bucket in buckets.Skip(1).SkipLast(1))
                {
                    foreach (var snap in bucket)
                    {
                        var road = map.GetRoad(snap.RoadIdx.RoadMapIndex);
                        if (road.IsDangerous || road.IsUncomfortable)
                        {
                            suppressed.AddRange(suppressTrafficCost(map.GetNode(snap.RoadIdx), suppressionRange));
                        }
                    }
                }
            }

            return suppressed;
        }

        private IEnumerable<TNodeId> suppressTrafficCost(TNodeId startNodeId, Length suppressionRange)
        {
            var suppressed = new HashSet<TNodeId>();
            // node id -> distance -> incoming road
            var heap = MappedPairingHeap.Create<TNodeId, Length, TRoadId>();

            heap.TryAddOrUpdate(startNodeId, Length.Zero, default);

            while (heap.TryPop(out TNodeId curr_node_id, out Length curr_dist, out TRoadId incoming_road_id))
            {
                // please note this collection is not "polluted" with data coming from other mid-points selected by user
                suppressed.Add(curr_node_id);
                this.DEBUG_suppressInRange.TryAdd(curr_node_id, $"#{incoming_road_id} {curr_dist}");

                GeoZPoint current_point = map.GetPoint(curr_node_id);

                foreach (var adj_idx in map.GetAdjacentRoads(curr_node_id))
                {
                    var adj_node_id = map.GetNode(adj_idx);

                    if (suppressed.Contains(adj_node_id))
                        continue;

                    var adj_road_info = this.map.GetRoad(adj_idx.RoadMapIndex);
                    if (adj_road_info.IsDangerous || adj_road_info.IsUncomfortable)
                    {
                        Length adj_total_dist = curr_dist + calc.GetDistance(current_point, map.GetPoint(adj_node_id));
                        if (adj_total_dist <= suppressionRange)
                        {
                            heap.TryAddOrUpdate(adj_node_id, adj_total_dist, adj_idx.RoadMapIndex);
                        }
                        else
                            this.DEBUG_suppressTooFar.TryAdd(adj_node_id, $"#{adj_idx.RoadMapIndex} {adj_total_dist}");
                    }
                }
            }

            return suppressed;
        }
    }
}