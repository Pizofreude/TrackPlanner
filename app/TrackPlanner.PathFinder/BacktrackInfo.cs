﻿using MathUnit;
using System;
using System.Runtime.InteropServices;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    internal readonly struct BacktrackInfo<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        public Placement<TNodeId,TRoadId> Source { get; }
        public TRoadId? IncomingRoadId { get; }
        public RoadCondition? IncomingCondition { get; }
        public Length RunningRouteDistance { get; }
        public TimeSpan RunningTime { get; }

        public BacktrackInfo(Placement<TNodeId,TRoadId> source,
            TRoadId? incomingRoadId, RoadCondition? incomingCondition, Length runningRouteDistance, TimeSpan runningTime)
        {
            IncomingCondition = incomingCondition;
            Source = source;
            IncomingRoadId = incomingRoadId;
            RunningRouteDistance = runningRouteDistance;
            this.RunningTime = runningTime;
        }
    }

   

}