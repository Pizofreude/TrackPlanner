using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    public class RouteCompactor<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        private readonly ILogger logger;
        private readonly IWorldMap<TNodeId,TRoadId> map;
        private readonly UserRouterPreferences userPrefs;
        private readonly bool compactPreservesRoads;
        private readonly ApproximateCalculator calc;

        public RouteCompactor(ILogger logger, IWorldMap<TNodeId,TRoadId> map,UserRouterPreferences userPrefs, bool compactPreservesRoads)
        {
            this.logger = logger;
            this.map = map;
            this.userPrefs = userPrefs;
            this.compactPreservesRoads = compactPreservesRoads;
            this.calc = new ApproximateCalculator();
        }
        
        public void FlattenRoundabouts(List<LegRun<TNodeId,TRoadId>> legs)
        {
            foreach (var leg in legs)
                flattenRoundabouts(leg.Steps);
        }
        
        public RoutePlan<TNodeId,TRoadId> Compact(List<LegRun<TNodeId,TRoadId>> legs)
        {
            var plan = splitLegs(legs);
            
            if (this.userPrefs.CompactingDistanceDeviation== Length.Zero 
                || this.userPrefs.CompactingAngleDeviation== Angle.Zero)
                this.logger.Verbose("Skipping simplification");
            else
            {
                int removed = 0;
                foreach (var leg in plan.Legs)
                {
                    foreach (var fragment in leg.Fragments)
                       removed+= simplifySegment(fragment);
                }
                
                this.logger.Verbose($"{removed} nodes removed when simplifying route with {this.userPrefs.CompactingAngleDeviation}, {this.userPrefs.CompactingDistanceDeviation}m.");
            }
            
            this.logger.Info($"{plan.Legs.SelectMany(l => l.AllSteps()).Count()} points on the track.");
            
            return plan;
        }

        private int simplifySegment(LegFragment<TNodeId,TRoadId> fragment)
        {
            int removed_count = 0;
            for (int start_idx = 0; start_idx < fragment.Steps.Count - 2; ++start_idx)
            {
                int end_idx;
                // we cannot remove the ending point
                for (end_idx = start_idx + 2; end_idx < fragment.Steps.Count-1; ++end_idx)
                {
                    Angle base_bearing = this.calc.GetBearing(fragment.Steps[start_idx].Point, fragment.Steps[end_idx].Point);

                    for (int i = start_idx; i < end_idx; ++i)
                    {
                        Angle curr_bearing = this.calc.GetBearing(fragment.Steps[i].Point, fragment.Steps[i + 1].Point);

                        if (this.calc.GetAbsoluteBearingDifference(curr_bearing, base_bearing) > this.userPrefs.CompactingAngleDeviation)
                        {
                            goto END_IDX_LOOP;
                        }
                    }

                    for (int i = start_idx + 1; i < end_idx; ++i) // the loop ranges are different than above
                    {
                        (Length dist, _, _) = this.calc.GetDistanceToArcSegment(fragment.Steps[i].Point, fragment.Steps[end_idx].Point, fragment.Steps[start_idx].Point);

                        if (dist > this.userPrefs.CompactingDistanceDeviation)
                        {
                            goto END_IDX_LOOP;
                        }
                    }
                }

                END_IDX_LOOP: ;

                // in both cases we have to decrease end_idx -- either we broke through the Count limit or we failed
                // to stick within the limits for it (but previous value was OK)
                --end_idx;

                int count = end_idx - start_idx - 1;
                if (count > 0)
                {
                    var dist_replacement = fragment.Steps.Skip(start_idx+1).Take(count+1).Select(it => it.IncomingFlatDistance).Sum();
                    fragment.Steps.RemoveRange(start_idx + 1, count);
                    fragment.Steps[start_idx+1] = fragment.Steps[start_idx+1] with
                    {
                        IncomingFlatDistance = dist_replacement
                    };
                    removed_count += count;
                }
            }

            return removed_count;
        }

        private void flattenRoundabouts(List<StepRun<TNodeId,TRoadId>> pathSteps)
        {
            legCheck(pathSteps);
            
            for (int step_idx = pathSteps.Count - 1; step_idx >= 0; )
            {
                var path_step = pathSteps[step_idx];
                
                var incoming_road_map_index = path_step.IncomingRoadMapIndex;
                var roundabout_road = map.GetRoad( incoming_road_map_index);
                if (!roundabout_road.IsRoundabout)
                {
                    --step_idx;
                    continue;
                }

                RoadCondition condition =  path_step.IncomingCondition;
                TimeSpan time = TimeSpan.Zero;
                Length distance = Length.Zero;
                
                int entry_idx;
                // take all steps covering roundabout
                for (entry_idx = step_idx; entry_idx >= 0 && map.GetRoad( pathSteps[entry_idx].IncomingRoadMapIndex).IsRoundabout; --entry_idx)
                {
                    var curr_step = pathSteps[entry_idx];
                    
                    if (condition != curr_step.IncomingCondition)
                        throw new NotSupportedException();
                    
                    time+=curr_step.IncomingTime;
                    distance+=curr_step.IncomingDistance;
                    
                }

                if (entry_idx == -1) // it could be that our leg start right at roundabout
                    entry_idx = 0;
                
                // entry_idx is first point of roundabout and step_idx is the last

                distance /= 2;
                time /= 2;
                pathSteps[step_idx] = new StepRun<TNodeId,TRoadId>(pathSteps[step_idx].Place, incoming_road_map_index,
                    condition, distance, time);
                    // remove entire roundabout trip, keep entrance and exit
                pathSteps.RemoveRange(entry_idx+1,step_idx-entry_idx-1);
                pathSteps.Insert(entry_idx+1,new StepRun<TNodeId,TRoadId>(
                    Placement<TNodeId,TRoadId>.Aggregate(this.map.GetRoundaboutCenter(this.calc,incoming_road_map_index),
                        incoming_road_map_index) , 
                    incoming_road_map_index, condition, 
                    distance,time));
             //   Console.WriteLine($"Roundabout with new step at {entry_idx+1} and replacement at {step_idx}");
                step_idx=entry_idx-1;
            }
          
            legCheck(pathSteps);
        }

        private void legCheck(IReadOnlyList<StepRun<TNodeId,TRoadId>> pathSteps)
        {
                var init_step = pathSteps.First();
                if (init_step.IncomingDistance != Length.Zero)
                    throw new ArgumentOutOfRangeException($"Initial step is expected to be zero length, it is {init_step.IncomingDistance}."); 
        }

        private IEnumerable<LegPlan<TNodeId,TRoadId>> splitSteps(IReadOnlyList<StepRun<TNodeId,TRoadId>> pathSteps)
        {
            legCheck(pathSteps);

            TimeSpan leg_time_limit;
            int expected_leg_pieces;
            if (this.userPrefs.CheckpointIntervalLimit == TimeSpan.Zero)
            {
                    leg_time_limit = TimeSpan.Zero;
                    expected_leg_pieces = 0;
            }
            else
            {
                var time_total = pathSteps.Select(it => it.IncomingTime).Sum();
                expected_leg_pieces = (int) Math.Ceiling(time_total / this.userPrefs.CheckpointIntervalLimit);
                leg_time_limit = time_total / expected_leg_pieces;
                
                this.logger.Info($"Leg total time {time_total} with limit {this.userPrefs.CheckpointIntervalLimit} split into {expected_leg_pieces} pieces, each {leg_time_limit}");
            }

            var leg = new LegPlan<TNodeId,TRoadId>() {AutoAnchored = false};
            TimeSpan running_time = TimeSpan.Zero;

            MapPoint<TNodeId>? last_point = null;
            
            foreach (var step_run in pathSteps)
            {
                GeoZPoint current_point = step_run.Place.Point;

                if (leg.Fragments.Any())
                {
                    if (leg.LastStep().Point == current_point) // skip repeated points
                        continue;

                    // if the current leg part is too long, we have to return it and create another leg
                    if (expected_leg_pieces > 1 // remember, we will return tail leg at the end (out of the loop)
                        && running_time + step_run.IncomingTime > leg_time_limit)
                    {
                        --expected_leg_pieces;
                        leg.ComputeLegTotals();
                        logger.Info($"Returning split leg {leg.RawTime}");
                        yield return leg;

                        leg = new LegPlan<TNodeId,TRoadId>() {AutoAnchored = true};
                        running_time = TimeSpan.Zero;
                    }
                }

                var speed_mode = step_run.IncomingCondition.Mode;

                if (!leg.Fragments.Any()
                    || (this.compactPreservesRoads && !EqualityComparer<TRoadId>.Default.Equals( leg.Fragments.Last().RoadIds.Single() , step_run.IncomingRoadMapIndex))
                    || (!this.compactPreservesRoads && leg.Fragments.Last().Mode != speed_mode))
                {
                    var new_fragment = new LegFragment<TNodeId,TRoadId>()
                    {
                        Risk = step_run.IncomingCondition.Risk,
                        IsForbidden = step_run.IncomingCondition.IsForbidden,
                    }
                        .SetSpeedMode(speed_mode)
                        ;

                    if (last_point is {} last_pt)
                    {
                        // we need to draw it, so the beginning of the new segment has to start where the last one ended
                        new_fragment.Steps.Add(new FragmentStep<TNodeId>( last_pt,Length.Zero));
                    }

                    leg.Fragments.Add(new_fragment);
                }

                var fragment = leg.Fragments.Last();
                fragment.Steps.Add(new FragmentStep<TNodeId>(current_point, 
                    step_run.Place.IsNode ? step_run.Place.NodeId:null,
                    step_run.IncomingDistance));
                fragment.RoadIds.Add(step_run.IncomingRoadMapIndex);
                fragment.RawTime += step_run.IncomingTime;
                fragment.UnsimplifiedFlatDistance += step_run.IncomingDistance;

                running_time += step_run.IncomingTime;

                last_point = fragment.Steps.Last().GetMapPoint();
            }


            if (leg.Fragments.Any())
            {
                leg.ComputeLegTotals();
                logger.Info($"Returning last leg, auto = {leg.AutoAnchored}, time {leg.RawTime}");
                yield return leg;
            }
        }

        private RoutePlan<TNodeId,TRoadId> splitLegs(IReadOnlyList<LegRun<TNodeId,TRoadId>> pathLegs)
        {
            var route = new RoutePlan<TNodeId, TRoadId>();

            foreach (var leg_run in pathLegs)
            {
                var plan_legs = splitSteps(leg_run.Steps);
                route.Legs.AddRange(plan_legs);
            }

            return route;
        }

    }
}