﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using SystemConfiguration = TrackPlanner.PathFinder.Stored.SystemConfiguration;


namespace TrackPlanner.PathFinder
{
    public sealed class RouteManager<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        public static IDisposable Create(ILogger? logger, Navigator navigator,
            string osmSubdirectory, SystemConfiguration systemConfiguration,
            out RouteManager<TNodeId, TRoadId> manager)
        {
            manager = new RouteManager<TNodeId, TRoadId>(logger, navigator, worldMap: null, osmSubdirectory,
                systemConfiguration, out IDisposable disp);
            return disp;
        }

        public static IDisposable Create(ILogger? logger, Navigator navigator,
            IWorldMap<TNodeId, TRoadId> worldMap, SystemConfiguration systemConfiguration,
            out RouteManager<TNodeId, TRoadId> manager)
        {
            manager = new RouteManager<TNodeId, TRoadId>(logger, navigator, worldMap, osmMapSubdirectory: null,
                systemConfiguration, out IDisposable disp);
            return disp;
        }

        private readonly ILogger logger;
        public IWorldMap<TNodeId, TRoadId> Map { get; }
        private readonly Navigator navigator;
        public SystemConfiguration SysConfig { get; }
        public IGeoCalculator Calculator { get; set; }

        public string? DebugDirectory { get; }

        private RouteManager(ILogger? logger, Navigator navigator,
            IWorldMap<TNodeId, TRoadId>? worldMap,
            string? osmMapSubdirectory,
            SystemConfiguration systemConfiguration,
            out IDisposable disposable)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            this.logger = logger;

            this.navigator = navigator;
            logger.Info($"{this} with baseDirectory: {navigator.BaseDirectory}");

            this.SysConfig = systemConfiguration;

            this.Calculator = new ApproximateCalculator();

            this.DebugDirectory = navigator.GetDebug(this.SysConfig.EnableDebugDumping);

            if (worldMap != null)
            {
                this.Map = worldMap;
                disposable = CompositeDisposable.None;
            }
            else
            {
                var osm_reader = new OsmReader(logger, Calculator, this.SysConfig.MemoryParams,
                    highTrafficProximity: this.SysConfig.HighTrafficProximity, DebugDirectory);
                {
                    double start = Stopwatch.GetTimestamp();

                    disposable = osm_reader.ReadMap<TNodeId, TRoadId>(System.IO.Path.Combine(navigator.GetOsmMaps(), osmMapSubdirectory!),
                        navigator.GetSrtmMaps(),
                        navigator.GetCustomMaps(),
                        onlyRoads: true, out var out_map);
                    this.Map = out_map;
                    disposable = CompositeDisposable.Stack(disposable,
                        () => { logger.Info($"STATS {nameof(this.Map)} {this.Map.GetStats()}"); });
                    Console.WriteLine($"Loading map in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");
                }
            }
        }

        public bool TryFindFlattenRoute(UserRouterPreferences userConfig, IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token, [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> route,
            out string? problem)
        {
           return TryFindFlattenRoute( userConfig,  userPoints, token, out route, out _, out  problem);
        }
        internal bool TryFindFlattenRoute(UserRouterPreferences userConfig, 
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token, [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> route,
            out RunningWeight routeWeight,
            out string? problem)
        {
            // the last point of given leg is repeated as the first point of the following leg
            var result = TryFindVanillaRoute(userConfig, userPoints, token, out route, out routeWeight, out problem);
            if (result)
            {
                var compactor = new RouteCompactor<TNodeId, TRoadId>(this.logger, this.Map, userConfig,
                    this.SysConfig.CompactPreservesRoads);

                compactor.FlattenRoundabouts(route!);
            }

            return result;
        }

        internal bool TryFindVanillaRoute(UserRouterPreferences userConfig, 
            IReadOnlyList<RequestPoint<TNodeId>> userPoints, CancellationToken token, 
            [MaybeNullWhen(false)]   out List<LegRun<TNodeId, TRoadId>> route, 
            out RunningWeight routeWeight, out string? problem)
        {
            var result = RouteFinder<TNodeId, TRoadId>.TryFindRoute(this.logger, this.navigator,
                this.Map, this.SysConfig, userConfig,
                userPoints, token, out route, out routeWeight, out problem);
            return result;
        }

        public bool TryFindCompactRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token, [MaybeNullWhen(false)] out RoutePlan<TNodeId, TRoadId> route)
        {
            return TryFindCompactRoute(userConfig, userPoints, token, out  route,out _);
        }

        internal bool TryFindCompactRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token, [MaybeNullWhen(false)] out RoutePlan<TNodeId, TRoadId> route,
            out RunningWeight routeWeight)
        {
            if (!TryFindFlattenRoute(userConfig, userPoints, token, out var legs,out routeWeight, out var problem))
            {
                route = default;
                return false;
            }

            route = CompactFlattenRoute(userConfig, legs);
            if (problem != null)
                route.ProblemMessage = problem;

            return true;
        }

        public RoutePlan<TNodeId, TRoadId> CompactFlattenRoute(UserRouterPreferences userConfig, List<LegRun<TNodeId, TRoadId>> legs)
        {
            var compactor = new RouteCompactor<TNodeId, TRoadId>(this.logger, this.Map, userConfig, 
                this.SysConfig.CompactPreservesRoads);

            return compactor.Compact(legs);
        }

        public bool TryFindRoute(UserRouterPreferences userConfig, IReadOnlyList<MapPoint<TNodeId>> userPlaces, bool allowSmoothing,
            CancellationToken token, [MaybeNullWhen(false)] out RoutePlan<TNodeId, TRoadId> route)
        {
            if (!RouteFinder<TNodeId, TRoadId>.TryFindPath(logger, this.navigator, this.Map, this.SysConfig, 
                    userConfig, userPlaces, allowSmoothing: allowSmoothing, token, out var legs,out _))
            {
                route = default;
                return false;
            }

            var compactor = new RouteCompactor<TNodeId, TRoadId>(logger, this.Map, userConfig, this.SysConfig.CompactPreservesRoads);
            compactor.FlattenRoundabouts(legs);
            route = compactor.Compact(legs);
            return true;
        }

        public bool TryFindRoute(UserRouterPreferences userConfig, IReadOnlyList<TNodeId> mapNodes,
            bool allowSmoothing,
            CancellationToken token, [MaybeNullWhen(false)] out RoutePlan<TNodeId, TRoadId> route)
        {
            bool DUMMY_ALLOW_SMOOTHING = false;
            if (!RouteFinder<TNodeId, TRoadId>.TryFindPath(logger, this.navigator, this.Map, 
                    this.SysConfig, userConfig, mapNodes, allowSmoothing, token, out var legs,out _))
            {
                route = default;
                return false;
            }

            var compactor = new RouteCompactor<TNodeId, TRoadId>(logger, this.Map, userConfig, this.SysConfig.CompactPreservesRoads);
            compactor.FlattenRoundabouts(legs);
            route = compactor.Compact(legs);
            return true;
        }


        public IEnumerable<(GeoPoint point, CityInfo info, Length distance)> GetCities(GeoPoint point, Length range)
        {
            var boundary = Calculator.GetBoundary(range, point);

            foreach (var (_, pt, city) in Map.GetCitiesWithin(boundary))
            {
                if (city.Name == null)
                    continue;

                var dist = Calculator.GetDistance(pt.Convert(), point.Convert());
                if (dist <= range)
                    yield return (pt, city, dist);
            }
        }

        public List<string?> FindNames(PlanRequest<TNodeId> request, RoutePlan<TNodeId, TRoadId> plan)
        {
            var names = new List<string?>();
            var range = request.RouterPreferences.NamesSearchRange;

            foreach (var req in request.GetSequenceWithAutoPoints(plan))
            {
                if (!req.FindLabel)
                    names.Add(null);
                else
                {
                    string? best_name = null;
                    Length best_dist = Length.MaxValue;

                    foreach (var (_, city, dist) in this.GetCities(req.UserPoint, range))
                    {
                        if (dist < best_dist)
                        {
                            best_dist = dist;
                            best_name = city.Name;
                        }
                    }

                    names.Add(best_name);
                }
            }

            return names;
        }

        public IEnumerable<MapPoint<TNodeId>> FindPeaks(GeoPoint focusPoint, Length serchRange, Length separationDistance, int count)
        {
            var boundary = Calculator.GetBoundary(serchRange, focusPoint);

            var peaks = new List<(TNodeId node_id,GeoZPoint point)>();
            foreach (var (node_id,node_point,_) in Map.GetNodesWithin(boundary)
                         .Where(it => Map.IsPeak(it.nodeId, it.nodeInfo.Point.Altitude))
                         .OrderByDescending(it => it.nodeInfo.Point.Altitude)
                         .Select(it => (it.nodeId, it.nodeInfo.Point, dist: Calculator.GetFlatDistance(focusPoint, it.nodeInfo.Point.Convert())))
                         .Where(it => it.dist <= serchRange))
            {
                if (peaks.Any(it => Calculator.GetFlatDistance(it.point.Convert(),node_point.Convert())<=separationDistance))
                    continue;
                peaks.Add((node_id,node_point));
                if (peaks.Count == count)
                    break;
            }
            
            return peaks.Select(it => new MapPoint<TNodeId>(it.point, it.node_id));
        }
    }
}
