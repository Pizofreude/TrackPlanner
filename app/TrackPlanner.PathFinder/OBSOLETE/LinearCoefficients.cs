﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;


namespace TrackPlanner.PathFinder
{
  
    [Obsolete]
    public readonly struct LinearCoefficients<TInput>
    {
        //public static LinearCoefficients Identty => new LinearCoefficients(1, 0);
        public static LinearCoefficients<TInput> Constant(double c) => new LinearCoefficients<TInput>(0, c, _ => 0);

        private readonly double m;
        private readonly double b;
        private readonly Func<TInput, double> selector;

        public LinearCoefficients(double m, double b, Func<TInput, double> selector)
        {
            this.m = m;
            this.b = b;
            this.selector = selector;
        }

        public double Compute(TInput x)
        {
            return m * this.selector(x) + this.b;
        }

        public override string ToString()
        {
            return $"m: {m}, b: {b}";
        }
    }
}