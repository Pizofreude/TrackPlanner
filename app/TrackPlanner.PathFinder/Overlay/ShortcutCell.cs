using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder.Overlay
{
    public sealed class ShortcutCell<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly Dictionary<TNodeId,int> boundaryIndices;
        private readonly WeightSteps<TNodeId,TRoadId>[,] weights;

        public ShortcutCell(RouteManager<TNodeId,TRoadId> manager,  UserRouterPreferences routerConfig,
            IReadOnlyList<TNodeId> boundaryNodes,CancellationToken token)
        {
            this.boundaryIndices = new Dictionary<TNodeId, int>(capacity: boundaryNodes.Count);
            for (int i=0;i<boundaryNodes.Count;++i)
                this.boundaryIndices.Add(boundaryNodes[i],i);
            this.weights = new  WeightSteps<TNodeId,TRoadId>[boundaryNodes.Count, boundaryNodes.Count];

            for (int start_idx = 0; start_idx < boundaryNodes.Count; ++start_idx)
            {
                var start_req = new RequestPoint<TNodeId>(manager.Map.GetPoint( boundaryNodes[start_idx]).Convert(),
                    allowSmoothing: false, findLabel:true);
                for (int end_idx = 0; end_idx < boundaryNodes.Count; ++end_idx)
                {
                    if (start_idx == end_idx)
                        continue;
                    
                    var end_req = new RequestPoint<TNodeId>(manager.Map.GetPoint( boundaryNodes[end_idx]).Convert(),
                        allowSmoothing: false, findLabel:true);
                    
                    if (manager.TryFindVanillaRoute(routerConfig, new[] {start_req, end_req},
                            token, out var route, out RunningWeight weight, out _))
                        this.weights[start_idx, end_idx] = new WeightSteps<TNodeId, TRoadId>(route.Single().Steps, weight);
                    else
                        this.weights[start_idx, end_idx] = WeightSteps<TNodeId,TRoadId>.Infinity;
                }
            }
        }

        internal bool TryFindRoute(TNodeId start,TNodeId end, 
            [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> route,
            out RunningWeight routeWeight)
        {
            if (!this.boundaryIndices.TryGetValue(start, out var start_idx)
                || !this.boundaryIndices.TryGetValue(end, out var end_idx))
            {
                route = null;
                routeWeight =  RunningWeight.Infinity;
                return false;
            }

            (var steps,routeWeight) = this.weights[start_idx, end_idx];
            if (routeWeight.IsInfinite)
            {
                route = null;
                return false;
            }

            route = new List<LegRun<TNodeId, TRoadId>>() { new LegRun<TNodeId, TRoadId>(steps.ToList())};
            return true;
        }
        
    }
}