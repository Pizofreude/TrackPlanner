using System;
using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Structures;

namespace TrackPlanner.Elevation
{
    public sealed class ElevationMap : IDisposable, IElevationMap
    {
        private readonly string directory;
        private const string dataFilePattern = "*.hgt";
        private readonly CacheMap<(int lat, int lon), (IHgtReader reader,IDisposable disposable)> readers;
        private readonly IReadOnlyDictionary<(int lat, int lon), string> files;

        public ElevationMap(string directory,int cellLimit)
        {
            this.directory = directory;
            this.readers = new CacheMap<(int lat, int lon), 
                (IHgtReader reader,IDisposable disposable)>(r => r.disposable.Dispose(),cellLimit);
            this.files = System.IO.Directory.GetFiles(directory, dataFilePattern)
                .ToDictionary(fn =>
                {
                    if (!HgtReader.TryGetFileCoordinates(fn, out var lat, out var lon))
                        throw new ArgumentException(fn);
                    return (lat, lon);
                }, fn => fn);
        }

        public void Dispose()
        {
            this.readers.Clear(); // they will be disposed through provided disposer
        }

        public bool TryGetHeight(GeoPoint point, out Length height)
        {
            var coords = ((int) Math.Floor(point.Latitude.Degrees), (int) Math.Floor(point.Longitude.Degrees));
            if (!this.readers.TryGetValue(coords, out var reader_entry))
            {
                if (!this.files.TryGetValue(coords, out var fn))
                {
                    height = default;
                    return false;
                }

                var disp = HgtReader.Create(fn,out var reader, out _,out _);
                reader_entry = (reader, disp);
                this.readers.Add(coords, reader_entry);
            }

            height = reader_entry.reader.GetHeight(point);
            return true;
        }

        public static GeoPoint SourceCoordinate(GeoPoint point)
        {
            return GeoPoint.FromDegrees(Math.Floor(point.Latitude.Degrees), Math.Floor(point.Longitude.Degrees));
        }

        public void ThrowIfEmpty()
        {
            if (!this.files.Any())
                throw new ArgumentException($"There are no {dataFilePattern} files in {this.directory} directory.");
        }
    }
}