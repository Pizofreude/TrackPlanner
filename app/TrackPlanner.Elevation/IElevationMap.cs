using Geo;
using MathUnit;

namespace TrackPlanner.Elevation
{
    public interface IElevationMap
    {
        bool TryGetHeight(GeoPoint point, out Length height);
    }
}