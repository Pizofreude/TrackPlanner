using System;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Force.DeepCloner;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Dialogs;

namespace TrackPlanner.WebUI.Client
{
    public static class ModalServiceDialogs
    {
        public static IDisposable ShowGuardDialog(this IModalService modalService, string message)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
                HideHeader = true,
                HideCloseButton = true,

            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(ProgressDialog.Message), message);
            var dialog = modalService.Show<ProgressDialog>(title: null, parameters, options);
            return new CompositeDisposable(() => dialog.Close());
        }

        public static async ValueTask<string?> ShowFileDialogAsync(this IModalService modalService, string title, FileDialog.DialogKind kind)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(FileDialog.Kind), kind);
            var dialog = modalService.Show<FileDialog>(title, parameters, options);
            var modal_result = await dialog.Result;

            if (modal_result.Cancelled)
                return null;
            else
                return $"{modal_result.Data}";
        }

     
        public static async ValueTask<ScheduleSettings?> ShowPreferencesDialogAsync(this IModalService modalService, string title, ScheduleSettings prefs)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(PreferencesDialog.Settings), prefs.DeepClone());
            var dialog = modalService.Show<PreferencesDialog>(title, parameters, options);
            var modal_result = await dialog.Result;

            return modal_result.Data as ScheduleSettings;
        }

        public static async ValueTask<object?> ShowAnchorDialogAsync(this IModalService modalService, string title,
            IReadOnlySchedule schedule, int dayIndex, int checkpointIndex)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(AnchorDialog.Schedule), schedule);
            parameters.Add(nameof(AnchorDialog.DayIndex), dayIndex);
            parameters.Add(nameof(AnchorDialog.CheckpointIndex), checkpointIndex);
            var dialog = modalService.Show<AnchorDialog>(title, parameters, options);
            var modal_result = await dialog.Result;

            if (modal_result.Cancelled)
                return null;
            else
                return modal_result.Data;
        }
    }
}