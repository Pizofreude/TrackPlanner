using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.Modal;
using Force.DeepCloner;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.WebUI.Client
{
    public class Program
    {
        public static GlobalSettings GlobalSettings { get; private set; } = new GlobalSettings();
        public static ScheduleSettings ScheduleSettings { get; private set; } = new ScheduleSettings();
        public static SystemConfiguration SystemConfiguration { get; private set; } = new SystemConfiguration();
        public static UserPlannerPreferences InitUserPlannerPrefs { get; private set; } = default!;

        private static readonly ProxySerializer Serializer = new ProxySerializer();
        
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

          /*  builder.Services.AddSingleton<ILoggerFactory>(sp =>
            LoggerFactory.Create(builder =>
                builder.AddSimpleConsole(options =>
                {
                    options.IncludeScopes = false;
                    options.SingleLine = true;
                    options.TimestampFormat = "hh:mm:ss ";
                })));*/

          builder.Services
              .AddBlazoredModal()
              ;

            var http = new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)};
            JObject config ;
            using (var response = await http.GetAsync(Constants.ConfigFilename))
            {
                await using (var stream = await response.Content.ReadAsStreamAsync())
                {
                    using (var reader = new StreamReader(stream, leaveOpen: true))
                    {
                        var config_content = await reader.ReadToEndAsync().ConfigureAwait(false);
                         config = JObject.Parse(config_content)!;
                    }

                    stream.Position = 0;
                    builder.Configuration.AddJsonStream(stream);
                }
            }

            SystemConfiguration = GetConfig<SystemConfiguration>(SystemConfiguration.SectionName,config);
            SystemConfiguration.Check();

            ScheduleSettings = GetConfig<ScheduleSettings>(ScheduleSettings.SectionName,config);
            ScheduleSettings.Check();

            GlobalSettings = GetConfig<GlobalSettings>(GlobalSettings.SectionName,config);
            GlobalSettings.Check();

            InitUserPlannerPrefs = ScheduleSettings.PlannerPreferences.DeepClone();


            builder.Services.AddScoped(sp => http);
            builder.Services.AddScoped(typeof(JsonRestClient));
            builder.Services.AddBlazorDownloadFile();

            builder.Logging.SetMinimumLevel(LogLevel.Debug);
            builder.Logging.AddFilter((provider, category, logLevel) =>category=="Planner" || category=="Markers");
            
            await builder.Build().RunAsync();
        }

        private static T GetConfig<T>(string sectionName, JObject config)
        {
            // ConfigurationBinder is buggy as hell
            return Serializer.Deserialize<T>( config[sectionName]!.ToString())!;
        }
    }
}