using System.Drawing;
using BlazorLeaflet.Models;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client.Widgets
{
    public sealed class AnchorMarker : Marker
    {
        public IAnchor Anchor { get; }

        public AnchorMarker(IAnchor anchor, LatLng latLng) : base(latLng)
        {
            Anchor = anchor;
        }

        public AnchorMarker(IAnchor anchor, float x, float y) : base(x, y)
        {
            Anchor = anchor;
        }

        public AnchorMarker(IAnchor anchor, PointF position) : base(position)
        {
            Anchor = anchor;
        }
    }
}