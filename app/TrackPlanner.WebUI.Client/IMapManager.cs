using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorLeaflet;
using BlazorLeaflet.MarkerCluster;
using BlazorLeaflet.Models;
using MathUnit;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client
{
    public interface IMapManager<TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId: struct
    {
        Map Map { get; }
        MarkerClusterGroup MarkerCluster { get; }
        RestWorker<TNodeId,TRoadId> RestWorker { get; } 

        IReadOnlyList<(string label,Length distance, Length height)> ElevationPoints { get; set; }
        IReadOnlyList<(string? label,Length distance, GeoZPoint point)> ElevationLine { get; set; }
        
        ValueTask<bool> RebuildNeededAsync();
        ValueTask MarkerAddedAsync(Marker marker);
        ValueTask MarkerDeletedAsync(Marker marker);
        ValueTask BeforeAnchorMarkersRemovedAsync();
        ValueTask RecreateMarkersAsync(int dayIndex, int anchorIndex);
    }
}