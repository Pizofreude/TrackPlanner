using System;
using Geo;

namespace TrackPlanner.WebUI.Client.Dialogs
{
    public readonly record struct  AnchorEdit
    {
        public TimeSpan? StartTime { get; }
        public TimeSpan? BreakTime { get; }
        public GeoPoint? Point { get; }
        
        public AnchorEdit(TimeSpan ?startTime, TimeSpan? breakTime,GeoPoint? point)
        {
            this.StartTime = startTime;
            BreakTime = breakTime;
            Point = point;
        }
    }
}