using System;
using System.Linq;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Geo;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client.Dialogs
{
   
   
    public partial class AnchorDialog
    {
        [CascadingParameter] private BlazoredModalInstance ModalInstance { get; set; } = default!;
        private readonly EditContext editContext;
        private TimeSpan initStartTime;
        private TimeSpan initBreakTime;
        private GeoPoint initPoint = default!;
        private string point { get; set; } = default!;
        private TimeSpan startTime { get; set; }
        public TimeSpan breakTime { get; set; }
        
        [Parameter] [EditorRequired] public IReadOnlySchedule Schedule { get; set; } = default!;
        [Parameter] [EditorRequired] public int DayIndex { get; set; } = default!;
        [Parameter] [EditorRequired] public int CheckpointIndex { get; set; } = default!;

        public AnchorDialog()
        {
            this.editContext = new EditContext(this);
        }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            this.initStartTime = Schedule.Days[DayIndex].Start;
            this.startTime = initStartTime;
            var (day_anchor_idx, anchor_idx, _) = Schedule.CheckpointIndexToAnchor(DayIndex, CheckpointIndex);
            this.initBreakTime = Schedule.Days[day_anchor_idx].Anchors[anchor_idx].UserBreak;
            this.breakTime = initBreakTime;
            this.initPoint =  Schedule.Days[day_anchor_idx].Anchors[anchor_idx].UserPoint;
            this.point = DataFormat.FormatUserInput(initPoint)!;
        }

        private Task confirmDialogAsync()
        {
            var res_point = DataFormat.TryParseUserPoint(point);
            return ModalInstance.CloseAsync(ModalResult.Ok(
                new AnchorEdit(startTime: initStartTime == startTime ? null : startTime,
                breakTime: this.initBreakTime == breakTime ? null : breakTime,
                point: res_point == null || res_point == this.initPoint ? null : res_point.Value)));
        }

        private Task mergeDayToPreviousAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorAction.DayMerge));
        }

        private Task pinMarkerAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorAction.AnchorPin));
        }
        
        private Task removeSingleAnchorAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorAction.AnchorDelete));
        }
        
        private Task splitDayAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorAction.DaySplit));
        }
        
        private Task cancelDialogAsync()
        {
            return ModalInstance.CancelAsync();
        }
    }
}