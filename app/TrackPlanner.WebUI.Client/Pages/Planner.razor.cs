using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Blazor.DownloadFileFast.Interfaces;
using Blazored.Modal;
using Blazored.Modal.Services;
using BlazorLeaflet;
using BlazorLeaflet.MarkerCluster;
using BlazorLeaflet.Models;
using BlazorLeaflet.Models.Events;
using Force.DeepCloner;
using Geo;
using MathUnit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Visual;
using TrackPlanner.WebUI.Client.Shared;
using TrackPlanner.Shared;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Dialogs;
using TrackPlanner.WebUI.Client.Widgets;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Client.Pages
{
    public partial class Planner : IMapManager<TNodeId,TRoadId>, IVisualController
    {
        public Map Map { get; private set; } = default!;

        private Markers markers = default!;
        private VisualSchedule<TNodeId,TRoadId> visualSchedule = default!;
       // private ScheduleManager<UiState> visualSchedule = default!;
       private StateManager<ScheduleState<UiState,TNodeId,TRoadId>> grabber = default!;

        private bool autoBuild;

        public bool AutoBuild
        {
            get { return autoBuild; }
            set
            {
                if (autoBuild == value)
                    return;

                autoBuild = value;

                uiBuildPlanAsync();
            }
        }

        private IEnumerable<FragmentLine<TNodeId,TRoadId>> fragmentLines => this.Map.NewLayers
            .Select(it => (it as FragmentLine<TNodeId,TRoadId>)!)
            .Where(it => it != null);

        private IEnumerable<AnchorMarker> anchorMarkers => this.Map.NewLayers
            .Select(it => (it as AnchorMarker)!)
            .Where(it => it != null);


        private bool alreadyRendered;

        private readonly DraftHelper<long, long> draftHelper;
        private CommonDialog commonDialog = default!;
        private Microsoft.Extensions.Logging.ILogger logger = default!;
        private Marker? traceMarker;
        private readonly ApproximateCalculator calculator;
        public RestWorker<TNodeId,TRoadId> RestWorker { get; private set; } = default!;
        public MarkerClusterGroup MarkerCluster { get; private set; } = default!;
        public bool TrueCalculations { get; private set; }

        public UiState UiState { get; }
        
        [Inject] public IJSRuntime JsRuntime { get; set; } = default!;
        [Inject] public JsonRestClient Rest { get; set; } = default!;
        [Inject] public IBlazorDownloadFileService DownloadService { get; set; } = default!;
        [Inject] public IModalService Modal { get; set; } = default!;
        [Inject] public ILoggerFactory LoggerFactory { get; set; } = default!;
        public IReadOnlyList<(string label, Length distance, Length height)> ElevationPoints { get; set; } = Array.Empty<(string label, Length distance, Length height)>();
        public IReadOnlyList<(string? label,Length distance, GeoZPoint point)> ElevationLine { get; set; } = Array.Empty<(string? label,Length distance, GeoZPoint point)>();

        public Planner()
        {
            this.calculator = new ApproximateCalculator();
            this.UiState = new UiState();
            this.draftHelper = new DraftHelper<long, long>(this.calculator);
        }

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);

            if (!this.alreadyRendered)
            {
                this.alreadyRendered = true;
            }
        }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            this.logger = LoggerFactory.CreateLogger("Planner");

            logger.LogDebug($"Creating map with {Program.SystemConfiguration.TileServer}");

           // this.visualSchedule = new ScheduleManager<UiState>(this, Program.ScheduleSettings);
           this.visualSchedule = new VisualSchedule<TNodeId,TRoadId>( this, Program.ScheduleSettings);
           this.grabber = new StateManager<ScheduleState<UiState,TNodeId,TRoadId>>(this.createSnapshot,
               this.applySnapshotAsync,
               Program.GlobalSettings.UndoHistory);

           this.TrueCalculations = Program.SystemConfiguration.CalcReal;
            this.autoBuild = Program.GlobalSettings.AutoBuild;
            this.commonDialog = new CommonDialog(JsRuntime);
            this.RestWorker = new RestWorker<TNodeId,TRoadId>(Rest, Program.SystemConfiguration.PlannerServer);

            this.Map = new Map(JsRuntime)
            {
                Center = new LatLng {Lat = 53.16844f, Lng = 18.73222f},
                Zoom = 9.8f
            };

            this.MarkerCluster = await MarkerClusterGroup.CreateAsync(JsRuntime);

            this.Map.OnInitialized += async () =>
            {
                var tile_layer = new BlazorLeaflet.Models.TileLayer
                {
                    UrlTemplate = Program.SystemConfiguration.TileServer + "{z}/{x}/{y}.png",
                    Attribution = "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
                };
                this.Map.AddLayer(tile_layer);

                this.Map.OnClick += onMapClick;

                this.markers.Test();
                //TestAsync();

                await Map.AddNewLayerAsync(this.MarkerCluster);
            };

            // todo: remove leftovers
           // await JsRuntime.InvokeVoidAsync("foobar");
        }

        private void DownloadAsync()
        {
            /*  if (this.Plan == null)
              {
                  return;
              }
  
              byte[] bytes;
              using (var stream = new MemoryStream())
              {
                  TrackPlanner.DataExchange.TrackWriter.SaveAsKml(Program.Configuration, stream,"something meaningful", this.Plan);
                  bytes = stream.ToArray();
              }
  
              await DownloadService.DownloadFileAsync("export.kml", bytes, "application/vnd.google-earth.kml+xml");
  */
        }


      

        private async Task loadScheduleAsync(string schedulePath)
        {
            string? failure;
            ScheduleJourney<TNodeId,TRoadId>? schedule;
            long start = Stopwatch.GetTimestamp();
            using (Modal.ShowGuardDialog($"Loading {schedulePath} ..."))
            {
                this.logger.LogDebug("Sending load rquest");
                (failure, schedule) = await Rest.GetAsync<ScheduleJourney<TNodeId,TRoadId>>(Url.Combine(Program.SystemConfiguration.PlannerServer, Routes.Planner, Methods.Get_LoadSchedule),
                    new RestQuery().Add(Parameters.Path, schedulePath),
                    CancellationToken.None);


                if (failure != null)
                {
                    await this.commonDialog.AlertAsync(failure);
                }
                else
                {
                    this.logger.LogDebug($"Data loaded successfuly in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
                    failure = schedule!.Route.DEBUG_Validate();
                    if (failure != null)
                        await this.commonDialog.AlertAsync($"Loaded plan is invalid: {failure}");

                    using (this.grabber.OpenScope($"Load {schedulePath}"))
                    {
                        await this.visualSchedule.SetScheduleAsync(schedule,schedulePath,modified:false);
                    }
                }
            }
        }

        public async ValueTask OnScheduleSettingAsync()
        {
            await this.MarkerCluster.ClearAsync();
            await this.BeforeAnchorMarkersRemovedAsync();
        }

        public async ValueTask OnScheduleSetAsync()
        {
            this.markers.InitCollapsedDays();
            await this.markers.AddAttractionMarkersAsync();

            logger.LogDebug($"Recreated days {this.visualSchedule.Days.Count} with {this.visualSchedule.Days.SelectMany(it => it.Anchors).Count()} anchors");
            // daily statistics will be called from parent component
                    
            await recreateLegLayersAsync();
        }


     
        private async ValueTask<bool> uiBuildPlanAsync(bool calcReal)
        {
            using (calcReal ? Modal.ShowGuardDialog("Building...") : null)
            {
                if (this.visualSchedule.Route.IsEmpty())
                {
                    return await completeRebuildPlanAsync(calcReal);
                }

                if (!this.visualSchedule.HasEnoughPointsForBuild())
                {
                    logger.LogDebug("Not enough points for refresh build.");
                    return false;
                }

                    var anchors_count = markers.AnchorElements.Count();

                    //logger.LogDebug($"DEBUG building only needed legs, in total {this.route.Legs.Count}, needed {this.route.Legs.Count(it => it.IsDrafted)}");
                    var router_prefs = this.visualSchedule.RouterPreferences.DeepClone();
                    var turner_prefs = this.visualSchedule.TurnerPreferences.DeepClone();

                    //var replacements = new List<(int index, List<LegPlan<long,long>> legs)>();

                    {
                        var DEBUG_anchors = this.markers.AnchorElements.ToList();
                        if (this.visualSchedule.Settings.LoopRoute)
                            DEBUG_anchors.Add(DEBUG_anchors[0]);

                        string? first_problem = null;
                        int problem_count = 0;

                        int error_count = 0;

                        var leg_idx = this.visualSchedule.Route.Legs.Count;
                        foreach (var (next, prev) in this.visualSchedule
                                     .CreateJourneySchedule(Program.GlobalSettings.VisualPreferences,
                                         onlyPinned: false)
                                     .BuildPlanRequest<TNodeId>().GetPointsSequence()
                                     .Reverse()
                                     .Slide())
                        {
                            --leg_idx;
                            if (!this.visualSchedule.Route.Legs[leg_idx].NeedsRebuild(calcReal))
                                continue;

                            //logger.LogDebug($"DEBUG, BuildPlanAsync {leg_idx}/{this.route.Legs.Count}, count {anchors_count}, markers loop {this.visualSchedule.Settings.LoopRoute}");
                            if (!DEBUG_anchors[leg_idx].IsPinned || !DEBUG_anchors[leg_idx + 1].IsPinned)
                            {
                                var (DEBUG_day_idx, DEBUG_anchor_idx, _) = this.visualSchedule.LegIndexToStartingDayAnchor(leg_idx);
                                throw new InvalidOperationException($"Impossible scenario, partial rebuilding on auto-anchors leg:{leg_idx} = {DEBUG_day_idx}:{DEBUG_anchor_idx} with {DEBUG_anchors[leg_idx].IsPinned} and {DEBUG_anchors[leg_idx + 1].IsPinned} of {this.visualSchedule.DEBUG_PinsToString()}.");
                            }

                            var prev_req = prev with {AllowSmoothing = false};
                            var next_req = next with {AllowSmoothing = false};
                            //try to glue to adjacent legs without any gaps
                            if (calcReal)
                            {
                                var adj_leg_idx = leg_idx + 1;
                                if (leg_idx == this.visualSchedule.Route.Legs.Count && this.visualSchedule.IsLoopActivated())
                                    adj_leg_idx = 0;
                                if (adj_leg_idx < this.visualSchedule.Route.Legs.Count
                                    && adj_leg_idx!=leg_idx
                                    && !this.visualSchedule.Route.Legs[adj_leg_idx].NeedsRebuild(calcReal))
                                {
                                    // use computed point to avoid gaps and also enforce it, so the route finder
                                    // won't jump over the point
                                    next_req = next_req with {
                                        UserPoint = this.visualSchedule.Route.Legs[adj_leg_idx]
                                        .FirstStep().Point.Convert(),
                                        EnforcePoint = true,
                                    };
                                }
                            }
                            if (calcReal)
                            {
                                var adj_leg_idx = leg_idx - 1;
                                if (leg_idx == -1 && this.visualSchedule.IsLoopActivated())
                                    adj_leg_idx = this.visualSchedule.Route.Legs.Count-1;
                                if (adj_leg_idx >=0
                                && adj_leg_idx!=leg_idx
                                    && !this.visualSchedule.Route.Legs[adj_leg_idx].NeedsRebuild(calcReal))
                                {
                                    prev_req = prev_req with {
                                        UserPoint = this.visualSchedule.Route.Legs[adj_leg_idx]
                                        .LastStep().Point.Convert(),
                                        EnforcePoint = true,
                                        
                                    };
                                }
                            }
                            
                            var request = new PlanRequest<TNodeId>()
                            {
                                RouterPreferences = router_prefs,
                                TurnerPreferences = turner_prefs,
                                DailyPoints = new List<List<RequestPoint<TNodeId>>>()
                                {
                                    new List<RequestPoint<TNodeId>>() {prev_req, next_req,}
                                }
                            };

                            var (problem, partial_response) = await getPlanAsync(request, calcReal);
                            first_problem ??= problem;
                            if (problem != null)
                                ++problem_count;

                            if (partial_response == null)
                                continue;

                            error_count += await this.markers.RemoveLegAttractionMarkersAsync(leg_idx);
                            await this.visualSchedule.ReplaceLegAsync(legIndex: leg_idx,
                                replacementLegs: partial_response.Route.Legs, partial_response.Names);

                            await addMapLegsAsync(partial_response.Route.Legs);
                        }

                        if (first_problem != null)
                            await this.commonDialog.AlertAsync($"{problem_count} problems, first: {first_problem}");

                        if (error_count > 0)
                            await this.commonDialog.AlertAsync($"Couldn't find {error_count} marker(s) based on attraction.");
                    }

                    var current_legs = this.visualSchedule.Route.Legs.ToHashSet();
                    // remove all invalid now (pointing out to removed legs) layers
                    int remove_count = 0;
                    foreach (var layer in this.fragmentLines.Where(it => !current_legs.Contains(it.LegRef)).ToArray())
                    {
                        ++remove_count;
                        await removeLegLayerAsync(layer);
                    }

                    logger.LogDebug($"DEBUG build-needed removed {remove_count} leg fragment layers");

                    await this.RecreateMarkersAsync(0, 0);

                    if (calcReal)
                    {
                        var failure = this.visualSchedule.Route.DEBUG_Validate();
                        if (failure != null)
                            await this.commonDialog.AlertAsync(failure);
                    }

                    return true;
                }
        }

        private async ValueTask removeLegLayersAsync()
        {
            int count = 0;
            while (this.fragmentLines.Any())
            {
                await removeLegLayerAsync(fragmentLines.First());

                ++count;
            }

            logger.LogDebug($"Removed {count} fragment layers.");
        }

        private async ValueTask removeLegLayerAsync(InteractiveLayer layer)
        {
            await this.Map.RemoveNewLayerAsync(layer);
            await layer.DisposeAsync();
        }


        public async Task FindPeaksAsync()
        {
            var pt = this.visualSchedule.Days[0].Anchors.FirstOrDefault()?.UserPoint;
            if (pt == null)
                return;

            using (Modal.ShowGuardDialog("Finding peaks..."))
            {
                var (failure, response) = await RestWorker.GetPeaksAsync(new PeaksRequest()
                {
                    FocusPoint = pt.Value,
                    Count = this.visualSchedule.PlannerPreferences.PeaksCount,
                    SearchRange = this.visualSchedule.PlannerPreferences.PeaksSearchRange,
                    SeparationDistance = this.visualSchedule.PlannerPreferences.PeaksSeparationDistance,
                }, CancellationToken.None);

                if (failure != null)
                    await this.commonDialog.AlertAsync($"{failure}");
                else
                {
                    foreach (var peak in response!.Peaks)
                    {
                        await this.Map.AddNewLayerAsync(createPeakMarker(GeoHelper.GeoPointToLatLng(peak.Point.Convert()),
                            DataFormat.FormatHeight(peak.Point.Altitude, withUnit: true)));
                    }
                }
            }
        }

        public async Task PromptNewProjectAsync()
        {
            if (!await this.commonDialog.ConfirmAsync("Really start from scratch?"))
                return;

            await newProjectAsync();
        }


        private async Task testAsync()
        {
            using (Modal.ShowGuardDialog("Rebuilding..."))
            {
                // if (false)
                {
                    await addMarkerAsync(GeoPoint.FromDegrees(53.01777, 18.59697)); // Torun
                    await addMarkerAsync(GeoPoint.FromDegrees(53.1376, 18.19631)); // Ostromecko
                    await addMarkerAsync(GeoPoint.FromDegrees(53.34573, 18.42256)); // Chelmno
                    // await addMarkerAsync(GeoPoint.FromDegrees(53.47588, 18.75662)); // Grudziadz
                    // await addMarkerAsync(GeoPoint.FromDegrees(53.27762, 18.94476)); // Wabrzezno

                    //    await newProjectAsync();

                    await completeRebuildPlanAsync(this.TrueCalculations);

                    //await this.markers.GetAttractionsAsync(dayIndex:0);
                }

                //     await loadScheduleAsync("jesien.trproj");
                //   this.markers.InsertMarkerPlaceholder(0, 6);
            }
        }

        private async Task loadScheduleAsync()
        {
            if (this.visualSchedule.IsModified)
            {
                if (!await this.commonDialog.ConfirmAsync("Current plan is modified, load anyway?"))
                    return;
            }

            var schedule_path = await Modal.ShowFileDialogAsync("Load schedule", FileDialog.DialogKind.Open);
            if (schedule_path == null)
            {
                logger.LogInformation("Modal was cancelled");
                return;
            }

            await loadScheduleAsync(schedule_path);
        }
        private async Task saveScheduleAsync()
        {
            string? schedule_path = this.visualSchedule.Filename;
            if (schedule_path == null)
            {
                schedule_path = await Modal.ShowFileDialogAsync("Save schedule", FileDialog.DialogKind.Save);
                if (schedule_path == null)
                {
                    logger.LogInformation("Modal was cancelled");
                    return;
                }

                if (System.IO.Path.GetExtension(schedule_path).ToLowerInvariant() != SystemCommons.ProjectFileExtension)
                    schedule_path += SystemCommons.ProjectFileExtension;
            }

            var schedule = this.visualSchedule.CreateJourneySchedule(Program.GlobalSettings.VisualPreferences,
                onlyPinned: false);

            string? failure;
            using (Modal.ShowGuardDialog($"Saving {schedule_path} ..."))
            {
                (failure, _) = await Rest.PostAsync<ValueTuple>(Url.Combine(Program.SystemConfiguration.PlannerServer, Routes.Planner, Methods.Post_SaveSchedule),
                    new SaveRequest<TNodeId,TRoadId>() {Schedule = schedule, Path = schedule_path}, CancellationToken.None);
            }

            if (failure == null)
            {
                using (this.grabber.OpenScope($"Save {schedule_path}"))
                {
                    this.markers.VisualSchedule.DataSaved(schedule_path);
                }
            }
            else
            {
                await this.commonDialog.AlertAsync($"Saved failed: {failure}");
            }
        }

        private async ValueTask recreateLegLayersAsync()
        {
            await removeLegLayersAsync();

            await addMapLegsAsync(visualSchedule.Route.Legs);
        }


        private async ValueTask<bool> completeRebuildPlanAsync(bool calcReal)
        {
            if (!this.visualSchedule.HasEnoughPointsForBuild())
            {
                logger.LogDebug("Not enough points to build a plan.");
                return false;
            }

            var (problem, response) = await getPlanAsync(this.visualSchedule.CreateJourneySchedule(Program.GlobalSettings.VisualPreferences, onlyPinned: true)
                .BuildPlanRequest<TNodeId>(), calcReal);
            if (problem != null)
            {
                await this.commonDialog.AlertAsync(problem);
            }

            if (response != null)
            {
                int error_count = 0;
                for (int leg_idx = 0; leg_idx < this.visualSchedule.Route.Legs.Count; ++leg_idx)
                    error_count += await this.markers.RemoveLegAttractionMarkersAsync(leg_idx);
                if (error_count > 0)
                    await this.commonDialog.AlertAsync($"Couldn't find {error_count} marker(s) based on attraction.");

                await this.visualSchedule.SetRouteAsync(response.Route, response.Names);

                await recreateLegLayersAsync();
                //    this.visualSchedule.ResetSummary(); // this is triggered by rebuild too
            }

            logger.LogDebug($"Complete rebuild: done.");
            return true;
        }

        public async ValueTask RecreateMarkersAsync(int dayIndex, int anchorIndex)
        {
            for (int d_idx = dayIndex; d_idx < this.visualSchedule.Days.Count; ++d_idx)
            {
                var day = this.visualSchedule.Days[d_idx];

                for (int a_idx = (d_idx == dayIndex ? anchorIndex : 0); a_idx < day.Anchors.Count; ++a_idx)
                {
                    var anchor = day.Anchors[a_idx];
                    var marker = findMarker(anchor);

                    string title = ScheduleSummaryExtension.GetMarkerCode(d_idx, a_idx);

                    if (title == marker.Title && Markers.IconClassName(this.visualSchedule.IsAnchorImportant(d_idx, a_idx), anchor.IsPinned) == marker.Icon.ClassName)
                    {
                        continue;
                    }

                    await MarkerDeletedAsync(marker);
                    await this.markers.AttachAnchorMarkerAsync(anchor, d_idx, a_idx);
                }
            }
        }

        public ValueTask OnRawAnchorRemovingAsync(VisualAnchor anchor)
        {
            var marker = findMarker(anchor);
            return this.MarkerDeletedAsync(marker);
        }

        private AnchorMarker findMarker(IReadOnlyAnchor anchor)
        {
            return this.anchorMarkers.Single(it => it.Anchor == anchor);
        }

        public ValueTask OnAnchorChangedAsync(int dayIndex, int anchorIndex)
        {
            return this.RecreateMarkersAsync(dayIndex, anchorIndex);
        }

        public void OnDaySplit(int dayIndex)
        {
            this.UiState.CollapsedDays[dayIndex] = true;
            this.UiState.CollapsedDays.Insert(dayIndex+1,false);
        }

        public void OnDayMerge(int dayIndex)
        {
           this.UiState.CollapsedDays.RemoveAt(dayIndex-1); 
        }

        public async ValueTask OnAttractionsRemovingAsync(int firstLegIndex, int lastLegIndex)
        {
            int error_count = 0;

            for (int leg_idx = firstLegIndex; leg_idx < lastLegIndex; ++leg_idx)
                error_count += await this.markers.RemoveLegAttractionMarkersAsync(leg_idx);

            if (error_count > 0)
                await this.commonDialog.AlertAsync($"Couldn't find {error_count} marker(s) based on attraction.");
        }

        public ValueTask OnAttractionsSetAsync()
        {
            return this.markers.AddAttractionMarkersAsync();
        }

        private async ValueTask<(string? problem, RouteResponse<TNodeId, TRoadId>? plan)> getPlanAsync(PlanRequest<TNodeId> request, bool calcReal)
        {
            try
            {
                var (failure, response) = await this.RestWorker.GetPlanAsync(request, calcReal, CancellationToken.None);

                if (failure != null)
                {
                    logger.LogError(failure);
                    return (failure, null);
                }
                else if (response?.Route.Legs == null)
                    return ("Computing route failed", null);
                else
                {
                    failure = response.Route.DEBUG_Validate();
                    if (failure != null)
                        logger.LogError($"Plan received with {failure}.");

                    return (failure, response);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return ($"Error {ex.Message}", null);
            }
        }


        public async ValueTask MarkerDeletedAsync(Marker marker)
        {
            if (!await this.Map.RemoveNewLayerAsync(marker))
                logger.LogError("We don't have given marker on the map.");
            await marker.DisposeAsync();
        }

        public async ValueTask BeforeAnchorMarkersRemovedAsync()
        {
            await removeLegLayersAsync();
            while (this.anchorMarkers.Any())
                await MarkerDeletedAsync(anchorMarkers.First());
        }

        public ValueTask MarkerAddedAsync(Marker marker)
        {
            return this.Map.AddNewLayerAsync(marker);
        }

        private static Color getColor(LineDecoration lineDecoration)
        {
            return Color.FromArgb(lineDecoration.GetArgbColor());
        }

        private async void LineOnOnMouseOverAsync(InteractiveLayer sender, MouseEvent e)
        {
            if (!e.OriginalEvent.ShiftKey)
                return;

            var line = sender as FragmentLine<TNodeId,TRoadId>;
            if (line == null)
            {
                logger.LogError($"Unknown segment");
                return;
            }

            this.visualSchedule.ComputeProgress(this.calculator, GeoPoint.FromDegrees(e.LatLng.Lat, e.LatLng.Lng),
                line.LegRef, line.FragmentRef, out Length running_distance,
                out TimeSpan day_time,
                out Length remaining_distance,
                out TimeSpan remaining_time);

            var p = new Popup
            {
                Position = e.LatLng,
                Content = $"{DataFormat.FormatDistance(running_distance, withUnit: true)} @({DataFormat.Format(day_time)})"
                          + "<br/>"
                          + $"{DataFormat.FormatDistance(remaining_distance, withUnit: true)} ({DataFormat.Format(remaining_time)})"
                          + "<br/>"
                          + $"{line.FragmentRef.Mode}"
            };

            await p.OpenOnAsync(this.Map);
            await Task.Delay(Program.GlobalSettings.PopupTimeout);
            await p.CloseAsync(this.Map);
        }

        public ValueTask<bool> RebuildNeededAsync()
        {
            return uiBuildPlanAsync(this.TrueCalculations && this.AutoBuild);
        }

        private async Task uiBuildPlanAsync() // for UI sake
        {
            await uiBuildPlanAsync(this.TrueCalculations);
        }

        private async Task uiCompleteRebuildPlanAsync() // for UI sake
        {
            using (Modal.ShowGuardDialog("Rebuilding..."))
            {
                await completeRebuildPlanAsync(this.TrueCalculations);
            }
        }

        private async Task UndoAsync() 
        {
            using (Modal.ShowGuardDialog("Undo..."))
            {
                await this.grabber.TryUndoAsync();
            }
        }

        private async Task RedoAsync() 
        {
            using (Modal.ShowGuardDialog("Redo..."))
            {
                await this.grabber.TryRedoAsync();
            }
        }

        private Marker createPeakMarker(LatLng coords, string title)
        {
            DivIcon icon;
            icon = new DivIcon()
            {
                Size = new Size(36, 46),
                Anchor = new Point(18, 43),
                PopupAnchor = new Point(3, -40),
                ClassName = "mountain-icon",
                Html = $"<div class='attraction-icon-text'>{title}</div>",
            };

            var marker = new Marker(coords)
            {
                Draggable = false,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                Icon = icon,
            };

            marker.OnClick += (sender, ev) => { Console.WriteLine(GeoHelper.GeoPointFromLatLng(coords)); };

            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            return marker;
        }

        private async void onMapClick(object sender, MouseEvent e)
        {
            await addMarkerAsync(GeoHelper.GeoPointFromLatLng(e.LatLng));
        }

        private async ValueTask addMarkerAsync(GeoPoint position)
        {
            using (this.grabber.OpenScope("Add anchor"))
                await this.visualSchedule.InsertAnchorAtPlaceholderAsync(position, "");
        }

        private async ValueTask addMapLegsAsync(IReadOnlyList<LegPlan<TNodeId, TRoadId>> legs)
        {
            foreach (var leg in legs)
            {
                foreach (var fragment in leg.Fragments)
                {
                    var deco = fragment.IsForbidden ? Program.GlobalSettings.VisualPreferences.ForbiddenStyle : Program.GlobalSettings.VisualPreferences.SpeedStyles[fragment.Mode];

                    var line = new FragmentLine<TNodeId,TRoadId>(leg, fragment);
                    line.StrokeColor = getColor(deco);
                    line.StrokeWidth = deco.Width;
                    line.Fill = false;

                    var shape = new PointF[1][];
                    int size = fragment.Steps.Count;
                    shape[0] = new PointF[size];
                    for (int i = 0; i < size; ++i)
                    {
                        double lat = fragment.Steps[i].Point.Latitude.Degrees;
                        double lon = fragment.Steps[i].Point.Longitude.Degrees;
                        shape[0][i] = new PointF((float) lat, (float) lon);
                    }

                    line.Shape = shape;
                    line.OnMouseOver += LineOnOnMouseOverAsync;

                    await this.Map.AddNewLayerAsync(line);
                }
            }

            //logger.LogDebug($"We have {legs.Count} legs, added as {this.legLayers.Count} fragments to the map.");
        }

        private async ValueTask newProjectAsync()
        {
            await this.BeforeAnchorMarkersRemovedAsync();

            using (this.grabber.OpenScope("New project"))
            {
                await this.visualSchedule.StartNewAsync();
            }

            this.markers.InitCollapsedDays();
            this.markers.RefreshState();
        }

        public void OnSummarySet()
        {
            var day_idx = this.UiState.CollapsedDays.IndexOf(it => !it);
            if (day_idx == -1 || !this.visualSchedule.HasEnoughPointsForBuild())
            {
                this.ElevationLine = Array.Empty<(string? label,Length distance, GeoZPoint point)>();
                this.ElevationPoints = Array.Empty<(string label, Length distance, Length height)>();
            }
            else
            {
                {
                    var data = new List<(string? label,Length distance, GeoZPoint point)>();
                    var step_dist = Length.Zero;
                    var chk_dist = Length.Zero;
                    for (int idx=0;idx<this.visualSchedule.Summary.Days[day_idx].Checkpoints.Count;++idx)
                    {
                        var leg_idx = this.visualSchedule.GetIncomingLegIndexByCheckpoint(day_idx, idx);
                        if (leg_idx != null)
                        {
                            foreach (var step in this.visualSchedule.Route.Legs[leg_idx.Value].AllSteps().Skip(1).SkipLast(1))
                            {
                                step_dist += step.IncomingFlatDistance;
                                data.Add((null,step_dist, step.Point));
                            }
                        }

                        var pt = this.visualSchedule.Summary.Days[day_idx].Checkpoints[idx];
                        chk_dist += pt.IncomingDistance;
                        step_dist = chk_dist;
                        data.Add(($"{pt.Code} {pt.Label}", step_dist, pt.UserPoint));
                        

                    }

                    /*foreach (var step in this.visualSchedule.GetDayLegs(day_idx)
                                 .SelectMany(it => it.AllSteps()))
                    {
                        step_dist += step.IncomingFlatDistance;
                        data.Add((step_dist, step.Point));
                    }*/

                    this.ElevationLine = data;
                }

                {
                    var data = new List<(string label, Length distance, Length height)>();
                    var dist = Length.Zero;
                    foreach (var pt in this.visualSchedule.Summary.Days[day_idx].Checkpoints)
                    {
                        dist += pt.IncomingDistance;
                        data.Add(($"{pt.Code} {pt.Label}", dist, pt.UserPoint.Altitude));
                    }

                    this.ElevationPoints = data;
                }
            }

            StateHasChanged();
        }

        private async Task editPreferencesAsync()
        {
            var prefs = await Modal.ShowPreferencesDialogAsync("Schedule preferences",
                this.visualSchedule.Settings);
            if (prefs == null)
                return;

            using (this.grabber.OpenScope("Parameters change"))
                await this.visualSchedule.SetSettingsAsync(prefs);
        }

        public async ValueTask OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex)
        {
            await markers.AttachAnchorMarkerAsync(anchor, dayIndex, anchorIndex).ConfigureAwait(false);
        }

     

        public  void OnPlaceholderChanged()
        {
            this.markers.RefreshState();
        }

        public async ValueTask OnAnchorInsertedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex)
        {
            await this.markers.AttachAnchorMarkerAsync(anchor, dayIndex, anchorIndex);
        }

        private async ValueTask onChartMouseOutAsync()
        {
            if (this.traceMarker == null)
                return;

            await Map.RemoveNewLayerAsync(this.traceMarker);
            await this.traceMarker.DisposeAsync();
            this.traceMarker = null;
        }

        private ValueTask onChartHoverAsync(GeoZPoint point)
        {
            if (this.traceMarker == null)
                return setTraceMarkerAsync(point);
            else
            {
                return this.traceMarker.SetLatLngAsync(JsRuntime, GeoHelper.GeoPointToLatLng(point.Convert()));
            }
        }

        public ValueTask setTraceMarkerAsync(GeoZPoint point)
        {
            // https://dotscrapbook.wordpress.com/2014/11/28/simple-numbered-markers-with-leaflet-js/
            DivIcon icon;

            icon = new DivIcon()
            {
                Size = new Size(48, 48),
                Anchor = new Point(24, 24),
            };

            icon.ClassName = "dot-icon";

            var marker = new Marker(GeoHelper.GeoPointToLatLng(point.Convert()))
            {
                Draggable = false,
                Icon = icon,
            };

            this.traceMarker = marker;
            return this.Map.AddNewLayerAsync(marker);
        }

        public async ValueTask OnRemovingLegAttractionsAsync(int legIndex)
        {
            await markers.RemoveLegAttractionMarkersAsync(legIndex);
        }

        public async ValueTask OnPlanChangedAsync()
        {
            await RebuildNeededAsync();
        }

        public async ValueTask OnAnchorMovedAsync(VisualAnchor anchor)
        {
            var marker = this.anchorMarkers.SingleOrDefault(it => it.Anchor == anchor);
            if (marker != null)
            {
                await marker.SetLatLngAsync(JsRuntime, GeoHelper.GeoPointToLatLng(anchor.UserPoint));
            }
            
            await RebuildNeededAsync();
        }

        public async ValueTask OnLegRemovingAsync(int legIndex)
        {
            int count = 0;
            var leg = this.visualSchedule.Route.Legs[legIndex];
            foreach (var layer in this.fragmentLines.Where(it => it.LegRef == leg).ToArray())
            {
                await removeLegLayerAsync(layer);

                ++count;
            }
        }

        private ScheduleState<UiState,TNodeId,TRoadId> createSnapshot(string label)
        {
            var state = new ScheduleState<UiState,TNodeId,TRoadId>(Label:label, 
                this.UiState.DeepClone(),
                this.visualSchedule.Filename, 
                this.visualSchedule.CreateJourneySchedule(null!, onlyPinned: false),
                isModified: this.visualSchedule.IsModified);
            return state;
        }

        private async ValueTask applySnapshotAsync(ScheduleState<UiState,TNodeId,TRoadId> snapshot)
        {
            await this.visualSchedule.SetScheduleAsync(snapshot.schedule.DeepClone(), snapshot.filename, snapshot.isModified);
            this.UiState.Assign( snapshot.uiState);
            this.markers.RefreshState();
        }
        
        public async ValueTask OnLoopChangedAsync()
        {
            if (this.visualSchedule.Days[0].Anchors.Count > 0)
                await RecreateMarkersAsync(this.visualSchedule.Days.Count - 1,
                    this.visualSchedule.Days[^1].Anchors.Count - 1);

            if (!await RebuildNeededAsync()) // if nothing was rebuilt at least update the state
                StateHasChanged();
        }
        
        public void NotifyOnChanged(object sender, string propertyName)
        {
           
        }

        public IDisposable NotifyOnChanging(object sender, string propertyName)
        {
            return this.grabber.OpenScope(propertyName);
        }

    }
}

// https://stackoverflow.com/questions/7060009/css-max-height-remaining-space
// https://stackoverflow.com/questions/90178/make-a-div-fill-the-height-of-the-remaining-screen-space
// https://stackoverflow.com/questions/68516283/flexbox-layout-take-remaining-space-but-do-not-expand-more-than-100-of-remaini

// https://css-tricks.com/snippets/css/complete-guide-grid/
// https://cssgrid-generator.netlify.app/