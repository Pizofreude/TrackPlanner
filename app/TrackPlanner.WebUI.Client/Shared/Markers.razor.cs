using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Blazored.Modal.Services;
using BlazorLeaflet;
using BlazorLeaflet.Models;
using BlazorLeaflet.Models.Events;
using Geo;
using MathUnit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.WebUI.Client.Pages;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Dialogs;
using TrackPlanner.WebUI.Client.Widgets;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Client.Shared
{
    public partial class Markers : IDisposable
    {
        private LatLng onDragMarkerLatLng = new LatLng {Lat = 47.5574007f, Lng = 16.3918687f};

        public IEnumerable<IReadOnlyAnchor> AnchorElements => this.Days.SelectMany(it => it.Anchors);

        private readonly EditContext editContext;

        [Parameter, EditorRequired] public VisualSchedule<TNodeId,TRoadId> VisualSchedule { get; set; } = default!;
        public IReadOnlyList<IVisualDay> Days => this.VisualSchedule.Days;
        [Inject] public IModalService Modal { get; set; } = default!;
        [Inject] public ILoggerFactory LoggerFactory { get; set; } = default!;
        [Parameter, EditorRequired] public StateManager<ScheduleState<UiState,TNodeId,TRoadId>> Grabber { get; set; } = default!;

        private CommonDialog commonDialog = default!;
        private Microsoft.Extensions.Logging.ILogger logger = default!;

        [Parameter] public IMapManager<TNodeId,TRoadId> MapManager { get; set; } = default!;
        [Inject] public IJSRuntime JsRuntime { get; set; } = default!;

        [Parameter] [EditorRequired] public UiState UiState { get; set; } = default!;

        public Markers()
        {
            this.editContext = new EditContext(this);
        }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            this.commonDialog = new CommonDialog(JsRuntime);
            this.logger = LoggerFactory.CreateLogger("Markers");
        }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            InitCollapsedDays();
        }

        public void Dispose()
        {
        }

        public void Test()
        {
            //   attachAttractionMarker(new LatLng(53.182995f, 18.484497f ), "foobar","whatever");
        }

        private void onDayToggled(AccordionToggle toggle) // this one was already toggled
        {
            if (!toggle.Collapsed) // if it is open now
            {
                // collapse all others
                for (int i = 0; i < UiState.CollapsedDays.Count; ++i)
                {
                    // if (toggle != day.Accordion)
                    if (!i.Equals(toggle.Tag))
                    {
                        logger.LogDebug($"Collapsing day accordion {i}");
                        UiState.CollapsedDays[i] = true;
                    }
                    else
                    {
                        logger.LogDebug($"At {i} there is active toggle, internal {toggle.Collapsed}, bound value {UiState.CollapsedDays[i]}");
                    }
                }
            }

            logger.LogDebug("Triggering toggle render for markers");
            // StateHasChanged();
        }

        public void InitCollapsedDays()
        {
            this.UiState.Assign(this.VisualSchedule.Days.Select(_ => true));
            this.UiState.CollapsedDays[^1] = false;
        }


        private (int dayIndex, int anchorIndex, IReadOnlyAnchor anchor) indexOfMarker(AnchorMarker marker)
        {
            for (int day_idx = 0; day_idx < this.Days.Count; ++day_idx)
            {
                for (int anchor_idx = 0; anchor_idx < this.Days[day_idx].Anchors.Count; ++anchor_idx)
                {
                    var anchor = this.Days[day_idx].Anchors[anchor_idx];
                    if (anchor == marker.Anchor)
                        return (day_idx, anchor_idx, anchor);
                }
            }

            throw new ArgumentException("Marker not found.");
        }

        private void onMarkerDrag(Marker marker, DragEvent evt)
        {
            this.onDragMarkerLatLng = evt.LatLng;

            StateHasChanged();
        }


        public static string IconClassName(bool isImportant, bool isPinned)
        {
            if (isPinned)
                return isImportant ? "orange-number-icon" : "navy-number-icon";
            else
                return "auto-anchor-icon";
        }


        public async ValueTask<AnchorMarker> AttachAnchorMarkerAsync(IAnchor anchor,
            int dayIndex, int anchorIndex)
        {
            string title = ScheduleSummaryExtension.GetMarkerCode(dayIndex, anchorIndex);
            bool is_important = this.VisualSchedule.IsAnchorImportant(dayIndex, anchorIndex);
            // https://dotscrapbook.wordpress.com/2014/11/28/simple-numbered-markers-with-leaflet-js/
            DivIcon icon;
            if (anchor.IsPinned)
                icon = new DivIcon()
                {
                    Size = new Size(36, 46),
                    Anchor = new Point(18, 43),
                    PopupAnchor = new Point(3, -40),
                };
            else
                /*  icon= new DivIcon()
                  {
                      Size = new Size(36, 46),
                      Anchor = new Point(18, 43),
                      PopupAnchor= new Point(3, -40),
                  };*/
                icon = new DivIcon()
                {
                    Size = new Size(48, 48),
                    Anchor = new Point(15, 38),
                    PopupAnchor = new Point(3, -40),
                };

            icon.ClassName = IconClassName(is_important, anchor.IsPinned);
            icon.Html = $"<div class='{(anchor.IsPinned ? "pinned" : "auto")}-anchor-icon-text'>{title}</div>";

            var marker = new AnchorMarker(anchor, GeoHelper.GeoPointToLatLng(anchor.UserPoint))
            {
                Draggable = anchor.IsPinned,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                //Tooltip = new Tooltip {Content = "Click and drag to move me"}
                Icon = icon,
            };

            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            if (anchor.IsPinned)
            {
                marker.OnMove += onMarkerDrag;
                marker.OnMoveEnd += onAnchorMarkerDragEndAsync;
                marker.OnClick += onAnchorMarkerClickAsync;
            }

            await this.MapManager.MarkerAddedAsync(marker);

            return marker;
        }


        public AttrMarker CreateAttractionMarker(LatLng coords, string title, string? tooltip)
        {
            DivIcon icon;
            icon = new DivIcon()
            {
                Size = new Size(36, 46),
                Anchor = new Point(18, 43),
                PopupAnchor = new Point(3, -40),
                ClassName = "camera-icon",
                Html = $"<div class='attraction-icon-text'>{title}</div>",
            };

            var marker = new AttrMarker(coords)
            {
                Draggable = false,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                Icon = icon,
            };
            if (tooltip != null)
                marker.Tooltip = new Tooltip {Content = tooltip};

            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            marker.OnClick += onAttractionMarkerClickAsync;
            marker.OnDblClick += AttractionMarkerOnDblClick;

            return marker;
        }

        private async void onAttractionMarkerClickAsync(InteractiveLayer sender, MouseEvent e)
        {
            var marker = (sender as Marker)!;
            logger.LogInformation(marker.Tooltip?.Content);
        }




        private async void addMarkerByPositionAsync()
        {
            var coords = await askForPositionAsync(position: null);
            if (coords != null)
                using (this.Grabber.OpenScope("Add anchor"))
                {
                    await VisualSchedule.InsertAnchorAtPlaceholderAsync(coords.Value, "");
                }
        }

        private async ValueTask<GeoPoint?> askForPositionAsync(GeoPoint? position)
        {
            var initial = DataFormat.FormatUserInput(position);
            string? input = await this.commonDialog.PromptAsync("Anchor position", initial);
            return DataFormat.TryParseUserPoint(input);
        }


        private async void onAnchorMarkerDragEndAsync(Marker marker, Event e)
        {
            await setMarkerAnchorPositionAsync((AnchorMarker) marker, GeoHelper.GeoPointFromLatLng(this.onDragMarkerLatLng));
        }

        private async void AttractionMarkerOnDblClick(InteractiveLayer sender, MouseEvent e)
        {
            var attr_marker = (AttrMarker) sender;
            if (await VisualSchedule.TryConvertAttractionToAnchor(attr_marker.Attraction!))
                await MapManager.MarkerCluster.RemoveLayerAsync(attr_marker);
            else
                await this.commonDialog.AlertAsync("Error: cannot find appropriate reference");
        }


        public async Task GetAttractionsAsync(int dayIndex)
        {
            if (this.VisualSchedule.Days[dayIndex].Anchors.Count == 0)
                return;

            string? failure;
            AttractionsResponse? atrr_response;
            using (Modal.ShowGuardDialog("Looking for attractions..."))
            {
                (failure, atrr_response) = await this.MapManager.RestWorker.GetAttractionsAsync(new AttractionsRequest()
                {
                    CheckPoints = this.VisualSchedule.GetSummary().Days[dayIndex].Checkpoints.Select(it => it.UserPoint.Convert())
                        //.GetDayCheckpoints(dayIndex)
                        .ToList(),
                    Range = VisualSchedule.PlannerPreferences.AttractionsRange,
                    ExcludeFeatures = VisualSchedule.PlannerPreferences.ExcludeAttractions,
                }, CancellationToken.None);
            }

            if (failure != null)
                await this.commonDialog.AlertAsync($"{failure}");
            else

            {
                await VisualSchedule.ClearAttractionsAsync(dayIndex);
                await this.VisualSchedule.AssignAttractionsAsync(dayIndex, atrr_response!.Attractions);
            }
        }

        public async ValueTask AddAttractionMarkersAsync()
        {
            foreach (var placed_attr in VisualSchedule.Route.Legs.SelectMany(it => it.Attractions))
            {
                string? description = placed_attr.Attraction.Name;
                if (placed_attr.Place.NodeId is { } node_id && node_id != -1)
                    description = $"#{node_id} {description}".Trim();
                var attr_marker = CreateAttractionMarker(GeoHelper.GeoPointToLatLng(placed_attr.Place.Point.Convert()),
                    String.Join(", ", placed_attr.Attraction.GetFeatures()), description);
                attr_marker.Attraction = placed_attr;
                await this.MapManager.MarkerCluster.AddLayerAsync(attr_marker);
            }
        }

        public async ValueTask<int> RemoveLegAttractionMarkersAsync(int legIndex)
        {
            int error_count = 0;
            int removed = 0;
            foreach (var attr in this.VisualSchedule.Route.Legs[legIndex].Attractions)
            {
                AttrMarker? marker = this.MapManager.MarkerCluster
                    .FindMarkerOrDefault<AttrMarker>(it => it.Attraction == attr);
                if (marker == null)
                    ++error_count;
                else
                {
                    await this.MapManager.MarkerCluster.RemoveLayerAsync(marker);
                    await marker.DisposeAsync();
                    ++removed;
                }
            }

//            logger.LogDebug($"removed {removed} attractions for leg {legIndex}");

            return error_count;
        }

        private async ValueTask rebuildNeededAsync([CallerMemberName] string memberName = "")
        {
            // Console.WriteLine($"redraw trigger from {memberName}");
            await this.MapManager.RebuildNeededAsync();
        }

        public void RefreshState()
        {
            StateHasChanged();
        }

        private async Task clearAttractionsAsync(int dayIdx)
        {
            await this.VisualSchedule.ClearAttractionsAsync(dayIdx);
        }

        private async Task mergeDayToPreviousAsync(int dayIndex)
        {
            using (Grabber.OpenScope("Merge days"))
                await this.VisualSchedule.MergeDayToPreviousAsync(dayIndex);
        }

        private void deletePlaceholder()
        {
            this.VisualSchedule.DeletePlaceholder();
        }

        private async Task pinMarkerAsync(int dayIndex, int anchorIndex)
        {
            using (Grabber.OpenScope("Pin anchor"))
                await this.VisualSchedule.PinMarkerAsync(dayIndex, anchorIndex);
        }

        private async Task removeSingleAnchorAsync(int dayIndex, int anchorIndex)
        {
            using (Grabber.OpenScope("Remove anchor"))
                await this.VisualSchedule.RemoveSingleAnchorAsync(dayIndex, anchorIndex);
        }

        private async Task splitDayAsync(int dayIndex, int anchorIndex)
        {
            using (Grabber.OpenScope("Split day"))
                await this.VisualSchedule.SplitDayAsync(dayIndex, anchorIndex);
        }

        private async ValueTask setMarkerAnchorPositionAsync(AnchorMarker marker, GeoPoint position)
        {
            marker.Position = GeoHelper.GeoPointToLatLng(position);
            (int day_idx, int anchor_idx, var anchor) = indexOfMarker(marker);
            using (Grabber.OpenScope("Move anchor"))
                await VisualSchedule.SetAnchorPositionAsync(position, day_idx, anchor_idx);
        }

        private async void onAnchorMarkerClickAsync(InteractiveLayer sender, MouseEvent e)
        {
            var marker = (sender as AnchorMarker)!;
            (int day_idx, int anchor_idx, var anchor) = indexOfMarker(marker);
            await editAnchorDetailsAsync(day_idx, this.VisualSchedule.AnchorIndexToCheckpoint(day_idx, anchor_idx));
            /*
            var coords = await askForPositionAsync(GeoHelper.GeoPointFromLatLng(marker.Position));
            if (coords != null)
            {
                await setMarkerAnchorPositionAsync(marker, coords.Value);
            }*/
        }
        
        private async ValueTask editAnchorDetailsAsync(int dayIndex, int checkpointIndex)
        {
            object? result = await this.Modal.ShowAnchorDialogAsync("Anchor", this.VisualSchedule, dayIndex, 
                checkpointIndex);
          (dayIndex,int anchor_idx,_)=  this.VisualSchedule.CheckpointIndexToAnchor(dayIndex, checkpointIndex);
            if (result is AnchorEdit edit)
            {
                using (Grabber.OpenScope("Anchor edit"))
                {
                    if (edit.Point is { } point)
                        await this.VisualSchedule.SetAnchorPositionAsync(point, dayIndex, anchor_idx);
                    if (edit.StartTime is { } start_time)
                        this.VisualSchedule.Days[dayIndex].Start = start_time;
                    if (edit.BreakTime is { } break_time)
                        this.VisualSchedule.Days[dayIndex].Anchors[anchor_idx].UserBreak = break_time;
                }
            }
            else if (result is AnchorAction action)
            {
                switch (action)
                {
                    case AnchorAction.AnchorDelete:
                        await removeSingleAnchorAsync(dayIndex, anchor_idx);
                        break;
                    case AnchorAction.DayMerge:
                        await mergeDayToPreviousAsync(dayIndex);
                        break;
                    case AnchorAction.AnchorPin:
                        await this.pinMarkerAsync(dayIndex, anchor_idx);
                        break;
                    case AnchorAction.DaySplit:
                        await this.splitDayAsync(dayIndex, anchor_idx);
                        break;
                    default: throw new NotImplementedException();
                }
            }
        }
    }
}