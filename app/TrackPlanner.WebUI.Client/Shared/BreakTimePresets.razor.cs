using System;
using Microsoft.AspNetCore.Components;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.WebUI.Client.Shared
{
    public partial class BreakTimePresets
    {
        [Parameter] [EditorRequired] public UserPlannerPreferences PlannerPreferences { get; set; } = default!;

        private TimeSpan value;

        [Parameter] [EditorRequired] public TimeSpan Value
        {
            get { return this.value; }
            set
            {
                if (this.value == value)
                    return;
                this.value = value;
                _ = ValueChanged.InvokeAsync(Value);
            }
        }

        [Parameter] public EventCallback<TimeSpan> ValueChanged { get; set; }

        public BreakTimePresets()
        {
        }
    }
}