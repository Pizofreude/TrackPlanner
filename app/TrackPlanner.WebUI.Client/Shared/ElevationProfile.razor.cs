using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MathUnit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using PSC.Blazor.Components.Chartjs;
using PSC.Blazor.Components.Chartjs.Models.Common;
using PSC.Blazor.Components.Chartjs.Models.Line;
using PSC.Blazor.Components.Chartjs.Models.Scatter;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.WebUI.Client.Shared
{
    public partial class ElevationProfile
    {
        private ScatterChartConfig config = default!;
        private Chart chart = default!;

        [Parameter] [EditorRequired] public IReadOnlyList<(string? label, Length distance, GeoZPoint point)> Line { get; set; } = default!;
        [Parameter] [EditorRequired] public IReadOnlyList<(string label, Length distance, Length height)> Points { get; set; } = default!;
        [Parameter] [EditorRequired] public Func<GeoZPoint, ValueTask> OnHoverAsync { get; set; } = default!;
        [Parameter] [EditorRequired] public Func<ValueTask> OnMouseOutAsync { get; set; } = default!;

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            Line.Select(it => it.point.Altitude.Meters).TryMinMax(out double y_min, out double y_max);
            y_min = Math.Floor(y_min / 5) * 5;
            y_max = Math.Ceiling(y_max / 5) * 5;

            this.config = new ScatterChartConfig()
            {
                Options = new Options()
                {
                    OnHoverAsync = onHoverAsync,
                    OnMouseOutAsync = onMouseOutAsync,
                    MaintainAspectRatio = false,
                    Plugins = new Plugins()
                    {
                        Crosshair = new Crosshair()
                        {
                            Cursor = "crosshair",
                            Vertical = new CrosshairLine()
                            {
                                Color = "rgba(0, 119, 290, 0.6)", //"#ff0000",
                                Width = 2,
                            },
/*                            Horizontal = new CrosshairLine()
                            {
                                Color = "#00ff00",
                                Width = 5,
                                Dash = new []{3, 3, 5, 7, 5, 2, 2},
                            },*/
                        },
                        Legend = new PSC.Blazor.Components.Chartjs.Models.Common.Legend()
                        {
                            Display = false,
                        },
                        Tooltip = new Tooltip()
                        {
                            Callbacks = new Callbacks()
                            {
                                Label = (ctx) =>
                                {
                                    if (ctx.DatasetIndex == 1)
                                    {
                                        var entry = this.Points[ctx.DataIndex];
                                        return new[] {$"{entry.label} {DataFormat.DecoFormatHeight(entry.height)}"};
                                    }
                                    else if (ctx.DatasetIndex == 0)
                                    {
                                        var entry = this.Line[ctx.DataIndex];
                                        string tooltip = DataFormat.DecoFormatHeight(entry.point.Altitude);
                                        if (!string.IsNullOrEmpty(entry.label))
                                            tooltip = $"{entry.label} {tooltip}";
                                        return new[] { tooltip };
                                    }
                                    else
                                    {
                                        return new[] {$"xx"};
                                    }
                                },
                            }
                        }
                    },
                    Scales = new Dictionary<string, Axis>()
                    {
                        {
                            Scales.XAxisId, new Axis()
                            {
                                Max = this.Line.LastOrDefault().distance.Kilometers,
                            }
                        },
                        {
                            "mirrorY", new Axis()
                            {
                                Min = y_min,
                                Max = y_max,
                                Position = "right",
                                Grid = new Grid()
                                {
                                    DrawOnChartArea = false,
                                }
                            }
                        },
                        {
                            Scales.YAxisId, new Axis()
                            {
                                Min = y_min,
                                Max = y_max,
                                Position = "left"
                            }
                        }
                    },
                    Elements = new Elements()
                    {
                        Line = new Line()
                        {
                            BorderColor = "rgba(0,0,0,1)",
                            BorderWidth = 1,
                        }
                        /*  Point = new Point()
                          {
                              Radius = 10,
                              BorderWidth = 0,
                              BackgroundColor = "rgba(1,0.3,0.2,0.7)",
                          },*/
                    }
                },
            };
            this.config.Data.Datasets.Add(new ScatterDataset()
            {
                YAxisId = Scales.YAxisId,
                BackgroundColor = "rgb(0, 99, 255)",
                Label = null,
                Data = Line.Select(it => new ScatterXYValue()
                {
                    X = (decimal) it.distance.Kilometers,
                    Y = (decimal) it.point.Altitude.Meters
                }).ToList(),
                PointHitRadius = 0,
                ShowLine = true,
                Tension = 0.2M,
                PointRadius = 0,
            });
            this.config.Data.Datasets.Add(new ScatterDataset()
            {
                YAxisId = "mirrorY",
                BackgroundColor = "rgb(42, 156, 16)",
                PointRadius = 5,
                PointHitRadius = 15,
                Label = null,
                Data = Points.Select(it => new ScatterXYValue()
                {
                    X = (decimal) it.distance.Kilometers,
                    Y = (decimal) it.height.Meters
                }).ToList(),
            });
        }

        private ValueTask onMouseOutAsync(MouseEventArgs args)
        {
            return OnMouseOutAsync();
        }

        private ValueTask onHoverAsync(HoverContext ctx)
        {
            if (Line.Count == 0)
                return ValueTask.CompletedTask;
            //   Console.WriteLine($"Looking for {ctx.DataX:0.0} in {(String.Join(", ",Line.Select(it => it.distance.Kilometers)))}");
            var index = this.Line.ApproxLinearIndexOf(it => it.distance.Kilometers, ctx.DataX);

            return OnHoverAsync(Line[index].point);
        }
    }
}

// https://www.youtube.com/watch?v=rLUwF1UQcbI
// https://stackoverflow.com/questions/45800521/chartjs-draw-vertical-line-at-data-point-on-chart-on-mouseover
// https://devsheet.com/code-snippet/show-vertical-line-on-data-point-hover-chartjs/
// https://stackoverflow.com/questions/68058199/chartjs-need-help-on-drawing-a-vertical-line-when-hovering-cursor