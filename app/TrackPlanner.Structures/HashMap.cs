﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace TrackPlanner.Structures
{
    public static class HashMap
    {
        public static HashMap<TKey, TValue> ToHashMap<T,TKey,TValue>(this IEnumerable<T> source,
            Func<T,TKey>keySelector,Func<T,TValue> valueSelector)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(source.ToDictionary(keySelector,valueSelector));
        }
        public static HashMap<TKey, TValue> Create<TKey, TValue>()
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(0);
        }

        public static HashMap<TKey, TValue> Create<TKey, TValue>(int capacity)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(capacity);
        }

        public static HashMap<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> sequence)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(new Dictionary<TKey, TValue>( sequence));
        }
    }
    
    public sealed class HashMap<TKey, TValue> : IMap<TKey,TValue>
        where TKey : notnull
    {
        private  Dictionary<TKey, TValue> elements;

        public IEnumerable<TKey> Keys => this.elements.Keys;

        public int Count => this.elements.Count;

        public IEnumerable<TValue> Values => this.elements.Values;

        public TValue this[TKey index]
        {
            get { return this.elements[index]; }
            set { this.elements[index] = value; }
        }

        internal HashMap(int capacity) 
        {
            this.elements = new Dictionary<TKey, TValue>(capacity);
        }

        internal HashMap(Dictionary<TKey, TValue> source)
        {
            this.elements = source;
        }

        public bool TryAdd(TKey key, TValue value, [MaybeNullWhen(true)] out TValue existing)
        {
            if (this.elements.TryGetValue(key, out existing))
                return false;
            else
            {
                this.elements.Add(key, value);
                return true;
            }
        }

        public bool TryAdd(TKey key, TValue value)
        {
            return TryAdd(key, value, out _);
        }

        public bool TryGetValue(TKey index, [MaybeNullWhen(false)] out TValue value)
        {
            return this.elements.TryGetValue(index, out value);
        }

        public void Add(TKey index, TValue value)
        {
            this.elements.Add(index, value);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.elements.GetEnumerator();
        }

        public bool ContainsKey(TKey idx)
        {
            return this.elements.ContainsKey(idx);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void ExceptWith(IEnumerable<TKey> keys)
        {
            foreach (TKey idx in keys)
                this.elements.Remove(idx);
        }
        
        public void IntersectWith(IEnumerable<TKey> keys)
        {
            var intersection = new Dictionary<TKey, TValue>(capacity:this.elements.Count, this.elements.Comparer);
            foreach (var k in keys)
                if (this.elements.TryGetValue(k,out var value))
                    intersection.TryAdd(k,value);

            this.elements = intersection;
        }

        public void TrimExcess()
        {
            this.elements.TrimExcess();
        }

        public void Clear()
        {
            this.elements.Clear();
        }
    }
}