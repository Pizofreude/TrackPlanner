﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace TrackPlanner.Structures
{
    /*public sealed class ArrayDictionary<TValue> : ICompactDictionary<int, TValue>
    {
        private const double growthRatio = 1.6;

        public int Count { get; private set; }
        public int Capacity => this.values.Length;

        private TValue[] values = default!;
        private readonly TValue notAValue;

        public TValue this[int key]
        {
            get
            {
                if (!TryGetValue(key, out TValue? value))
                    throw new ArgumentException($"Key {key} does not exist.");

                return value;
            }
            set
            {
                if (!tryAdd(key, key.GetHashCode(), value, overwrite: true, out _))
                    throw new NotSupportedException();
            }
        }

        public IEnumerable<int> Keys => this.iterate().Select(it => it.Key);
        public IEnumerable<TValue> Values => this.iterate().Select(it => it.Value);

        public ArrayDictionary(TValue notAValue, int capacity)
        {
            this.notAValue = notAValue;
            initData(capacity);
        }

        private void initData(int capacity)
        {
            if (capacity < 0)
                throw new NotSupportedException($"{nameof(capacity)} {capacity}");

            this.values = new TValue[capacity];

            this.Count = 0;
            Array.Fill(this.values, this.notAValue);
        }

        public void Clear()
        {
            this.Count = 0;
            Array.Fill(this.values, this.notAValue);
        }

        public void Add(int key, TValue value)
        {
            if (!tryAdd(key, key.GetHashCode(), value, overwrite: false, out _))
                throw new ArgumentException($"Key {key} already exists.");
        }

        public bool TryAdd(int key, TValue value, [MaybeNullWhen(true)]out TValue existing)
        {
            return tryAdd(key, key.GetHashCode(), value, overwrite: false, out existing);
        }

        public void TrimExcess()
        {
            resize(this.Count);
        }

        public void Expand()
        {
            resize((int) Math.Round((this.values.Length + 1) * growthRatio));
        }

        // https://stackoverflow.com/a/51018529/210342
        protected int mod(int k, int n) // always returns positive value
        {
            return ((k %= n) < 0) ? k + n : k;
        }

        private void resize(int capacity)
        {
            int DEBUG = Count;

            var source_values = values;

            initData(capacity);

            for (int i = source_values.Length - 1; i >= 0; --i)
            {
                if (Object.Equals( source_values[i],this.notAValue))
                    continue;

                if (!tryAdd(i,  source_values[i], overwrite: false, out _))
                    throw new InvalidOperationException();
                --DEBUG;

            }

            if (DEBUG != 0)
            {
                throw new InvalidOperationException($"Invalid resize {DEBUG} not copied");
            }
        }

       
        private bool tryAdd(int key,  TValue value, bool overwrite, out TValue? existing)
        {
            if (this.values.Length == 0)
                initData(2);

            if (overwrite)
            {
                this.values[key] = value;
                return true;
            }
            else 

            int hash_index = mod(pureHash, this.keys.Length);

            // this slot is taken and it is taken with out-of-sync hash, thus we need to move
            // this entry somewhere else
            if (this.targets[hash_index] < 0)
            {
                if (Count == this.keys.Length)
                {
                    Expand();
                    return tryAdd(key, pureHash, value, overwrite, out existing);
                }

                int index = findReferer(hash_index);

                // looking for some free entry
                while (this.targets[this.occupied] != notUsed)
                {
                    ++this.occupied;
                }

                // we move this entry to free one
                this.keys[occupied] = this.keys[hash_index];
                this.values[this.occupied] = this.values[hash_index];
                // and reorganize the indices
                this.targets[this.occupied] = this.targets[hash_index]; // redirect bit was already set
                this.targets[index] = indexToTarget(this.occupied) | (this.targets[index] & redirectedBit); // we have to preserve redirect bit, not blindly set it
                this.targets[hash_index] = notUsed;
            }

            if (this.targets[hash_index] == notUsed)
            {
                if (Count == this.keys.Length)
                {
                    // some time later -- if we have unused slot, this condition is then false, correct?
                    throw new Exception("REMOVE ME");
                    Expand();
                    return tryAdd(key, pureHash, value, overwrite, out existing);
                }

                this.keys[hash_index] = key;
                this.values[hash_index] = value;
                this.targets[hash_index] = indexToTarget(hash_index); // this is valid (in-sync) slot so do not set redirection bit 
            }
            else
            {
                {
                    int index = hash_index;
                    do
                    {
                        if (this.comparer.Equals(keys[index], key))
                        {
                            existing = values[index];
                            if (overwrite)
                            {
                                this.values[index] = value;
                                return true;
                            }
                            else
                                return false;
                        }

                        index = targetToIndex(this.targets[index]);

                    } while (index != hash_index);
                }

                if (Count == this.keys.Length)
                {
                    Expand();
                    return tryAdd(key, pureHash, value, overwrite, out existing);
                }

                while (this.targets[this.occupied] != notUsed)
                {
                    ++this.occupied;
                }

                var data_index = this.occupied;
                this.keys[data_index] = key;
                this.values[data_index] = value;
                this.targets[data_index] = this.targets[hash_index] | redirectedBit;
                this.targets[hash_index] = indexToTarget(data_index); // this is valid (in-sync) slot so do not set redirection bit 
            }


            ++Count;

            existing = default;
            return true;
        }

        protected abstract int targetToIndex(int target);
        protected abstract int indexToTarget(int index);


        private int findReferer(int index)
        {
            int curr_index = index;
            // searching the slot which directs to this entry
            while (true)
            {
                var dest = targetToIndex(this.targets[curr_index]);
                if (dest == index)
                    break;
                curr_index = dest;
            }

            return curr_index;
        }

        private bool tryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value, out int index, out int hashIndex)
        {
            if (Count == 0)
            {
                value = default;
                index = default;
                hashIndex = default;
                return false;
            }

            hashIndex = mod(key.GetHashCode(), this.keys.Length);

            // checking if slot is occupied and whether at least hash matches
            if (this.targets[hashIndex] != notUsed && this.targets[hashIndex] >= 0)
            {
                index = hashIndex;
                do
                {
                    if (this.comparer.Equals(keys[index], key))
                    {
                        value = this.values[index];
                        return true;
                    }

                    index = targetToIndex(this.targets[index]);
                } while (index != hashIndex);
            }

            value = default;
            index = default;
            hashIndex = default;
            return false;
        }

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            return tryGetValue(key, out value, out _, out _);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return iterate().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool ContainsKey(TKey key)
        {
            return TryGetValue(key, out _);
        }

        protected IEnumerable<KeyValuePair<TKey, TValue>> iterate()
        {
            int idx = -1;
            foreach (var target in this.targets)
            {
                ++idx;
                if (target == notUsed)
                    continue;
                yield return KeyValuePair.Create(this.keys[idx], this.values[idx]);
            }
        }

        public void ExceptWith(IEnumerable<TKey> keys)
        {
            foreach (TKey k in keys)
            {
                Remove(k, out _);
            }
        }
        
        public bool Remove(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            if (!tryGetValue(key, out value, out int del_index, out int hash_index))
                return false;

            if (del_index != targetToIndex(this.targets[del_index])) // we have multiple entries for given hash
            {
                int referer_index;

                // we are removing out-of-sync entry, so all it takes
                // is to find who is linking to this entry
                if (hash_index != del_index)
                {
                    referer_index = findReferer(del_index);
                }
                // if we are about to remove entry from its true slot (in sync)
                // we have to move something to this place,
                // because we cannot leave hole in true slot
                else
                {
                    // copy stuff from slot we are pointing to
                    var curr_dest_index = targetToIndex(this.targets[del_index]);
                    keys[del_index] = keys[curr_dest_index];
                    this.values[del_index] = this.values[curr_dest_index];
                    // and then set indices for removal to the other slot
                    referer_index = del_index;
                    del_index = curr_dest_index;
                }

                // closing the same-hash-loop/ring
                {
                    var curr_target = this.targets[del_index];
                    if (referer_index == hash_index) // if we are at in-sync position we have to clear redirect flag
                        curr_target &= indexMask;
                    else
                        curr_target |= redirectedBit;
                    this.targets[referer_index] = curr_target;
                }
            }

            this.targets[del_index] = notUsed;
            // set key and value to default so GC could reclaim their memory
            this.keys[del_index] = default(TKey)!;
            this.values[del_index] = default(TValue)!;

            --Count;
            this.occupied = Math.Min(this.occupied, del_index);

            return true;
        }
    }*/
}