﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.IO;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
  public  class RoadGridCell<TNodeId,TRoadId> : IReadOnlyGridCell<TNodeId,TRoadId>
      where TNodeId:struct
  where TRoadId:notnull
    {
        // todo: we could keep only those segments starters which are not in nodes-roads map
        // but watch out -- the last node of the road does not create a segment, yet this road-node is
        // present nodes-roads map
        
        // segment from given road index to (implicit) next road index
        private readonly List<RoadIndexLong<TRoadId>> roadRoadSegments; // when working, we don't need hashset, so let's keep it list for lower memory

        public IReadOnlyList<RoadIndexLong<TRoadId>> RoadSegments => this.roadRoadSegments;

        public RoadGridCell()
        {
            this.roadRoadSegments = new List<RoadIndexLong<TRoadId>>();
        }

        public RoadGridCell( List<RoadIndexLong<TRoadId>> segmets )
        {
            this.roadRoadSegments = segmets ;
        }

        internal void AddSegment(RoadIndexLong<TRoadId> roadIdx)
        {
            this.roadRoadSegments.Add(roadIdx);
        }

        public virtual (int, int) GetStats()
        {
            return (0, 0);
        }
        public IEnumerable<TNodeId> GetSegmentNodes(IWorldMap<TNodeId,TRoadId> map)
        {
            foreach (var idx in this.roadRoadSegments)
            {
                yield return map.GetNode(idx);
                yield return map.GetNode(idx.Next());
            }
        }

        public virtual void Write(BinaryWriter writer)
        {
            DiskHelper.WriteMap(writer,this.roadRoadSegments,(w,v) => v.Write(w));
        }
        
        public IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> GetSnaps(IWorldMap<TNodeId,TRoadId> map, IGeoCalculator calc,
            GeoZPoint point, Length snapLimit,
            Func<RoadInfo<TNodeId>,bool>? predicate)
        {
            foreach (var idx in this.roadRoadSegments)
            {
                if (predicate != null && !predicate(map.GetRoad(idx.RoadMapIndex)))
                    continue;

                // because we basically look for points on mapped ways, we expect the difference to be so small that we can use plane/euclidian distance
                var start = map.GetPoint(idx);
                var end = map.GetPoint(idx.Next());
                (var snap_distance, var cx, Length distance_along_segment) = calc.GetDistanceToArcSegment(point, start, end);
                if (snap_distance <= snapLimit)
                {
                    yield return new RoadSnapInfo<TNodeId,TRoadId>(map.GetNode(idx), idx, snap_distance, cx, 
                        distance_along_segment, shortestNextDistance: Length.Zero);
                    yield return new RoadSnapInfo<TNodeId,TRoadId>(map.GetNode(idx.Next()), idx.Next(), snap_distance, 
                        cx, calc.GetDistance(start, end) - distance_along_segment, shortestNextDistance: Length.Zero);
                }
            }
        }
    }
}
