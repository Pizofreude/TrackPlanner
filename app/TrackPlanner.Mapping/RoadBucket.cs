﻿using MathUnit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Geo;
using TrackPlanner.Mapping;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class RoadBucket
    {
        public static List<RoadBucket<TNodeId,TRoadId>> GetRoadBuckets<TNodeId,TRoadId>(IReadOnlyList<TNodeId> mapNodes,
            IWorldMap<TNodeId,TRoadId> map, IGeoCalculator calc,
            bool allowSmoothing)
            where TNodeId:struct
            where TRoadId:notnull
        {
            return mapNodes.ZipIndex().Select(it =>
            {
                return CreateBucket(it.index,it.item,map,calc, isFinal:it.index == 0 || it.index == mapNodes.Count - 1,
                    allowSmoothing:allowSmoothing);
            }).ToList();
        }

        public static RoadBucket<TNodeId,TRoadId> CreateBucket<TNodeId,TRoadId>(int index,TNodeId nodeId,IWorldMap<TNodeId,TRoadId> map,
            IGeoCalculator calc,bool isFinal,
            bool allowSmoothing)
        where TNodeId:struct
            where TRoadId:notnull
        {
            var node_point = map.GetPoint(nodeId);
            Dictionary<RoadIndexLong<TRoadId>, RoadSnapInfo<TNodeId,TRoadId>> snaps = map.GetRoadsAtNode(nodeId)
                .ToDictionary(idx => idx,
                    idx => new RoadSnapInfo<TNodeId,TRoadId>(nodeId,idx, trackSnapDistance: Length.Zero, 
                        trackCrosspoint: node_point, 
                        distanceAlongRoad: Length.Zero, shortestNextDistance: Length.Zero));
            return new RoadBucket<TNodeId,TRoadId>(DEBUG_trackIndex: index, map, nodeId:nodeId, node_point, calc, snaps, 
                reachableNodes: new HashSet<TNodeId>(), Length.Zero,  isFinal: isFinal,allowSmoothing);
        }
    }
    public sealed class RoadBucket<TNodeId,TRoadId> : IEnumerable<RoadSnapInfo<TNodeId,TRoadId>>
    where TNodeId:struct
    where TRoadId:notnull
    {

        private readonly IWorldMap<TNodeId,TRoadId> map;
        private readonly TNodeId? nodeId;
        private readonly IGeoCalculator calc;
        public Length UsedProximityLimit { get; }

        // IMPORTANT: for given OSM node this type does not keep all roads (you have to fetch them from the map)
        // this is because we get the nodes by hitting segments in nearby, if some segment is too far
        // we won't register it

        // key: road id
        private readonly IReadOnlyDictionary<TRoadId, IReadOnlyList<RoadSnapInfo<TNodeId,TRoadId>>> roads;

        public IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> RoadSnaps => this.roads.Values.SelectMany(x => x);
        public int Count => this.roads.Count;

        public int DEBUG_TrackIndex { get; }
        public GeoZPoint UserPoint { get; }
        public IReadOnlySet<TNodeId> ReachableNodes { get; }
        public bool IsFinal { get; }
        public bool AllowSmoothing { get; }

        //public IEnumerable<long> Nodes => this.Values.Select(it => map.GetNode(it.Idx)).Distinct();

        public RoadBucket(int DEBUG_trackIndex, IWorldMap<TNodeId,TRoadId> map, TNodeId? nodeId, GeoZPoint userPoint,
            IGeoCalculator calculator,
            IReadOnlyDictionary<RoadIndexLong<TRoadId> , RoadSnapInfo<TNodeId,TRoadId>> snaps,
            IReadOnlySet<TNodeId> reachableNodes,
            Length usedProximityLimit, bool isFinal,bool allowSmoothing)
        {
            DEBUG_TrackIndex = DEBUG_trackIndex;
            this.map = map;
            this.nodeId = nodeId;
            this.UserPoint = userPoint;
            ReachableNodes = reachableNodes;
            this.calc = calculator;
            this.UsedProximityLimit = usedProximityLimit;
            IsFinal = isFinal;
            AllowSmoothing = allowSmoothing;
            this.roads = snaps
                // here we remove "lone islands", i.e. nodes which does not have connections (within given assigment)
                //.GroupBy(it => map.GetNode(it.Key))
                //.Where(it => it.Count()>1)                
                //.SelectMany(x => x)

                .GroupBy(it => it.Key.RoadMapIndex)
                .ToDictionary(it => it.Key, it => it.OrderBy(it => it.Key.IndexAlongRoad)
                .Select(it => it.Value).ToList().ReadOnlyList());
        }

        public RoadBucket<TNodeId,TRoadId> RebuildWithSingleSnap(GeoPoint snapCrosspoint, TNodeId snapNodeId)
        {
            var snap = RoadSnaps
                .Where(it => EqualityComparer<TNodeId>.Default
                    .Equals( this.map.GetNode(it.RoadIdx) , snapNodeId) && it.TrackCrosspoint.Convert() == snapCrosspoint)
                .SingleOrNone();

            if (!snap.HasValue)
            {
                throw new Exception($"Cannot find single entry {snapNodeId}, cx {snapCrosspoint} in the bucket:"
                                    + Environment.NewLine
                                    + String.Join(Environment.NewLine, RoadSnaps.Select(it => $"node {this.map.GetNode(it.RoadIdx)}, cx {it.TrackCrosspoint}")));
            }

            return new RoadBucket<TNodeId,TRoadId>(DEBUG_TrackIndex,this.map,this.nodeId,UserPoint,calc,
                new Dictionary<RoadIndexLong<TRoadId>, RoadSnapInfo<TNodeId,TRoadId>>()
            {
                [snap.Value.RoadIdx] = snap.Value
            },ReachableNodes,UsedProximityLimit,IsFinal,AllowSmoothing);
        }


        public IEnumerator<RoadSnapInfo<TNodeId,TRoadId>> GetEnumerator()
        {
            return this.RoadSnaps.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }


        public IEnumerable<(RoadIndexLong<TRoadId> index, Length snapDistance, GeoZPoint crosspoint)> TryGetSameRoad(TRoadId roadId)
        {
            if (this.roads.TryGetValue(roadId, out var list))
                return list.Select(it => (Idx: it.RoadIdx, it.TrackSnapDistance, it.TrackCrosspoint));
            else
                return Enumerable.Empty<(RoadIndexLong<TRoadId> index, Length snapDistance, GeoZPoint crosspoint)>();
        }

        public IEnumerable<(RoadIndexLong<TRoadId> index, Length snapDistance)> GetRoadNeighbourhood(RoadIndexLong<TRoadId> idx)
        {
            if (this.roads.TryGetValue(idx.RoadMapIndex, out var list))
            {
                var left = list.Where(it => it.RoadIdx.IndexAlongRoad < idx.IndexAlongRoad).Select(it => (ass: it, exist: true))
                    .LastOrDefault();
                var curr = list.Where(it => it.RoadIdx.IndexAlongRoad == idx.IndexAlongRoad).Select(it => (ass: it, exist: true))
                    .SingleOrDefault();
                var right = list.Where(it => it.RoadIdx.IndexAlongRoad > idx.IndexAlongRoad).Select(it => (ass: it, exist: true))
                    .FirstOrDefault();


                if (left.exist)
                    yield return (left.ass.RoadIdx, left.ass.TrackSnapDistance);
                if (curr.exist)
                    yield return (curr.ass.RoadIdx, curr.ass.TrackSnapDistance);
                if (right.exist)
                    yield return (right.ass.RoadIdx, right.ass.TrackSnapDistance);
            }
        }

        public IEnumerable<(RoadIndexLong<TRoadId> index, Length snapDistance)> GetAtNode(in RoadIndexLong<TRoadId> idx)
        {
            return GetAtNode(map.GetNode(idx));
        }

        public IEnumerable<(RoadIndexLong<TRoadId> index, Length snapDistance)> GetAtNode(TNodeId nodeId)
        {
            IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> get_entries()
            {
                foreach (var entry in this.RoadSnaps)
                    if (EqualityComparer<TNodeId>.Default.Equals( map.GetNode(entry.RoadIdx) , nodeId))
                        yield return entry;
            }

            return get_entries().OrderBy(it => it.LEGACY_ShortestNextDistance)
                .Select(it => (Idx: it.RoadIdx, it.TrackSnapDistance));
        }

        public bool TryGetEntry(RoadIndexLong<TRoadId> idx, out RoadSnapInfo<TNodeId,TRoadId> info)
        {
            if (!this.roads.TryGetValue(idx.RoadMapIndex, out var list))
            {
                info = default;
                return false;
            }

            int index_of = list.IndexOf(it => it.RoadIdx.IndexAlongRoad == idx.IndexAlongRoad);
            if (index_of == -1)
            {
                info = default;
                return false;
            }

            info = list[index_of];
            return true;
        }

        public bool Contains(RoadIndexLong<TRoadId> idx)
        {
            return TryGetEntry(idx, out _);
        }

        public override string ToString()
        {
            if (this.nodeId == null)
                return this.UserPoint.ToString();
            else
                return $"n#{this.nodeId}@{UserPoint}";
        }

    }
}
