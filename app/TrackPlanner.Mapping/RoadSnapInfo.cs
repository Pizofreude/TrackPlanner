﻿using System.Runtime.InteropServices;
using Geo;
using MathUnit;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct RoadSnapInfo<TNodeId, TRoadId>
    {
        public TNodeId NodeId { get; } // for performance gain
        public RoadIndexLong<TRoadId> RoadIdx { get; }
        public Length TrackSnapDistance { get; }
        public GeoZPoint TrackCrosspoint { get; }
        // distance from crosspoint to the given node
        public Length DistanceAlongRoad { get; }
        
        public Length LEGACY_ShortestNextDistance { get; }

        public RoadSnapInfo(TNodeId nodeId, RoadIndexLong<TRoadId> roadIdx, Length trackSnapDistance, 
            in GeoZPoint trackCrosspoint,
            Length distanceAlongRoad, Length shortestNextDistance)
        {
            NodeId = nodeId;
            this.RoadIdx = roadIdx;
            this.TrackSnapDistance = trackSnapDistance;
            TrackCrosspoint = trackCrosspoint;
            DistanceAlongRoad = distanceAlongRoad;
            this.LEGACY_ShortestNextDistance = shortestNextDistance;
        }

        public void Deconstruct(out RoadIndexLong<TRoadId> idx, out Length trackSnapDistance,
            out GeoZPoint trackCrosspoint, out Length distanceAlongRoad, out Length shortestNextDistance)
        {
            idx = this.RoadIdx;
            trackSnapDistance = this.TrackSnapDistance;
            shortestNextDistance = this.LEGACY_ShortestNextDistance;
            distanceAlongRoad = this.DistanceAlongRoad;
            trackCrosspoint = this.TrackCrosspoint;
        }

    }
  }
