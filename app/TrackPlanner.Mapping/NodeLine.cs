﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Storage;
using TrackPlanner.Structures;

namespace TrackPlanner.Mapping
{
    public readonly record struct NodeLine<TNodeId>
    {
        public IReadOnlyList<TNodeId> Nodes { get; }

        public NodeLine(IReadOnlyList<TNodeId> nodes)
        {
            Nodes = nodes;
        }
    }
}