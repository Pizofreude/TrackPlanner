﻿namespace TrackPlanner.Mapping.Stored
{
    public sealed class MemorySettings
    {
        public int WorldCacheCellsLimit { get; set; }
        public int ElevationCacheCellsLimit { get; set; }
        public int GridCellSize { get; set; }
        private MapMode mapMode;
        private bool enableOsmId;

        public MemorySettings()
        {
            this.mapMode = MapMode.TrueDisk;

            ElevationCacheCellsLimit = 20;
            GridCellSize = 182; // 182 is the max we can use 

            WorldCacheCellsLimit = 20_000;
        }

        // avoding serialization
        public MapMode GetMapMode() => this.mapMode;

        public void SetMapMode(MapMode mapMode)
        {
            this.mapMode = mapMode;
        }

        public bool GetEnableOsmId() => this.enableOsmId;

        public void SetEnableOsmId(bool enable)
        {
            this.enableOsmId = enable;
        }

        public override string ToString()
        {
            return $"{this.mapMode} mode, {GridCellSize} cell size, {WorldCacheCellsLimit} cache limit, OSM Id {(this.enableOsmId?"on":"off")}";
        }
    }

 
}