﻿using MathUnit;
using System.Collections.Generic;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public interface IGrid
    {
        int CellSize { get; }
        IGeoCalculator Calc { get; } 
        
        string GetStats();
    }
    
    public interface IGrid<TNodeId,TRoadId> : IGrid
        where TNodeId:struct
        where TRoadId : notnull
    {
        IEnumerable<KeyValuePair<CellIndex,IReadOnlyGridCell<TNodeId,TRoadId>>> Cells { get; }

        List<RoadBucket<TNodeId, TRoadId>> GetRoadBuckets(IReadOnlyList<RequestPoint<TNodeId>> userPoints, 
            Length proximityLimit,
            Length upperProximityLimit, bool requireAllHits, bool singleMiddleSnaps);
        RoadBucket<TNodeId,TRoadId>? GetRoadBucket(int index, GeoZPoint userPoint, Length initProximityLimit,
            Length finalProximityLimit, bool requireAllHits, bool singleSnap, bool isFinal, 
            bool allowSmoothing);
    }

}