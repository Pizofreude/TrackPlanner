﻿using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    // 8GB robocza, 10B peak, 19 sekund wczytanie
    public sealed class NodeRoadsDictionary<TNodeId, TRoadId> 
        where TNodeId : notnull
        where TRoadId : notnull
    {
        private readonly IReadOnlyDictionary<TNodeId, IReadOnlySet<RoadIndexLong<TRoadId>>> roadReferences;

        public IReadOnlySet<RoadIndexLong<TRoadId>> this[TNodeId nodeId] => this.roadReferences[nodeId];


        public NodeRoadsDictionary(IReadOnlyMap<TNodeId, GeoZPoint> nodes,
            IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads)
        {
            // roads can form strange loops like "q" shape (example: https://www.openstreetmap.org/way/23005989 )
            // or can have knots, example: https://www.openstreetmap.org/way/88373084

            var back_refs = nodes.ToDictionary(it => it.Key, _ => new HashSet<RoadIndexLong<TRoadId>>());

            foreach (var (road_id, road_info) in roads)
            {
                for (int i = 0; i < road_info.Nodes.Count; ++i)
                {
                    var node_set = back_refs[road_info.Nodes[i]];
                    node_set.Add(new RoadIndexLong<TRoadId>(road_id, i));
                }
            }

            this.roadReferences = back_refs.ToDictionary(it => it.Key,
                it => it.Value.Me<IReadOnlySet<RoadIndexLong<TRoadId>>>());
        }
    }
}