﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;


#nullable enable

namespace TrackPlanner.Mapping
{
   // translations between OSM identifiers and grid-based indices
   internal sealed class IdTranslationTable
   {
      private sealed class SubListComparer : IEqualityComparer<List<long>>
      {
         public static SubListComparer Instance { get; } = new SubListComparer();

         private SubListComparer()
         {
            
         }
         public bool Equals(List< long>? x, List<long>? y)
         {
            if (x == y)
               return true;
            if ((x == null) != (y == null))
               return false;

            return Enumerable.SequenceEqual(x!, y!);
         }

         public int GetHashCode(List<long> obj)
         {
            return obj.Aggregate(0, (acc, val) => acc ^ val.GetHashCode());
         }
      }
      
      private readonly IMap<long,WorldIdentifier> osmToGrid;
      private readonly IMap<CellIndex,List<long>> gridToOsm;

      public int Count => this.osmToGrid.Count; 

      public IdTranslationTable(int capacity)
      {
         this.osmToGrid = HashMap.Create<long, WorldIdentifier>(capacity);
         this.gridToOsm = HashMap.Create< CellIndex,List<long>>();
      }

      private IdTranslationTable(IMap<long, WorldIdentifier> osmToGrid, 
         IMap<CellIndex,List<long>> gridToOsm)
      {
         this.osmToGrid = osmToGrid;
         this.gridToOsm = gridToOsm;
      }

      public WorldIdentifier AddOrGet(long osmId,CellIndex cellIndex)
      {
         if (this.osmToGrid.TryGetValue(osmId, out var world_id))
            return world_id;

         var sub_map = new List<long>();
         if (!this.gridToOsm.TryAdd(cellIndex, sub_map, out var existing))
            sub_map = existing;
         world_id = new WorldIdentifier(cellIndex, WorldIdentifier.CastIndex(sub_map.Count));
         sub_map.Add(osmId);
         this.osmToGrid.Add(osmId,world_id);
         return world_id;
      }

      public WorldIdentifier Get(long osmId)
      {
         return this.osmToGrid[osmId];
      }

      public long GetOsm(WorldIdentifier worldId)
      {
         return this.gridToOsm[worldId.CellIndex][worldId.EntityIndex];
      }


      public void Write(BinaryWriter writer)
      {
         DiskHelper.WriteMap(writer,this.gridToOsm,
            writeKey:(k) => k.Write(writer),
            writeValue:(v) => DiskHelper.WriteList(writer,v, writer.Write));
      }

      public static IdTranslationTable Read(BinaryReader reader)
      {
         IMap<CellIndex, List< long>> grid_to_osm =  
            DiskHelper.ReadMap(reader,HashMap.Create<CellIndex, List< long>>,
            CellIndexExtension.Read, 
            r => DiskHelper.ReadList(r ,subr => subr.ReadInt64()),
            SubListComparer.Instance);

         IMap<long, WorldIdentifier> osm_to_grid = HashMap.Create<long, WorldIdentifier>(grid_to_osm
            .Sum(it => it.Value.Count));

         foreach (var (cell_idx, sub_list) in grid_to_osm)
            for (int entity_idx=0;entity_idx<sub_list.Count;++entity_idx)
         {
            osm_to_grid.Add(sub_list[entity_idx], 
               new WorldIdentifier(cell_idx,WorldIdentifier.CastIndex( entity_idx)));
         }

         return new IdTranslationTable(osm_to_grid,grid_to_osm);
      }
   }
}