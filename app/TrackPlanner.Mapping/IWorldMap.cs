﻿using MathUnit;
using System.Collections.Generic;
using Geo;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public interface IWorldMap<TNodeId,TRoadId>
        where TNodeId:struct
    where TRoadId:notnull
    {
        IGrid<TNodeId,TRoadId> Grid { get; }
        
        GeoZPoint GetPoint(TNodeId nodeId);
        IEnumerable<KeyValuePair<TNodeId, GeoZPoint>> GetAllNodes();

        Angle Eastmost { get; }
        Angle Northmost { get; }
        Angle Southmost { get; }
        Angle Westmost { get; }

        IReadOnlySet<RoadIndexLong<TRoadId>> GetRoadsAtNode(TNodeId nodeId);
        bool IsBikeFootRoadDangerousNearby(TNodeId nodeId);

        string GetStats();
        RoadInfo<TNodeId> GetRoad(TRoadId roadId);
        IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads();
        long GetOsmRoadId(TRoadId roadId);
        long GetOsmNodeId(TNodeId nodeId);
        IEnumerable<TouristAttraction> GetAttractions(TNodeId nodeId);
        IEnumerable<(TNodeId,GeoPoint,TouristAttraction)> GetAttractionsWithin(Region region,
            TouristAttraction.Feature excludeFeatures);
        IEnumerable<(TNodeId,GeoPoint,CityInfo)> GetCitiesWithin(Region region);
        IEnumerable<(TNodeId nodeId,NodeInfo<TRoadId> nodeInfo)> GetNodesWithin(Region region);
        TNodeId FromOsmNodeId(long osmNodeId);
    }
    
}