﻿#nullable enable

namespace TrackPlanner.Mapping
{
    public enum MapMode
    {
        MemoryOnly = 0,
        TrueDisk = 1,
    }
}