﻿#nullable enable

namespace TrackPlanner.Mapping.Data
{
    public enum RoadSmoothness : byte
    {
        Excellent = 0,
        Good = 1,
        Intermediate = 2,
        Bad = 3,
        VeryBad = 4,
        Horrible = 5,
        VeryHorrible = 6,
        Impassable = 7,
    }
    
    public static class RoadSmoothnessExtension
    {
        public static bool EqualOrWorse(this RoadSmoothness smoothness, RoadSmoothness other)
        {
            return (int) smoothness >= (int) other;
        }
    }

}