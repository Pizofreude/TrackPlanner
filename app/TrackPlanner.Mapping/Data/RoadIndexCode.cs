﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace TrackPlanner.Mapping.Data
{
    /*public readonly struct RoadIndexCode : IEquatable<RoadIndexCode>
    {
        private static readonly NumberFormatInfo nfi;

        static RoadIndexCode()
        {
            nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = "_";
        }

        public static RoadIndexCode InvalidIndex(WorldIdentifier roadId)
        {
            return new RoadIndexCode(roadId, ushort.MaxValue);
        }

        public WorldIdentifier RoadMapIndex { get; }
        public ushort IndexAlongRoad { get; }

        public RoadIndexCode(in WorldIdentifier roadMapIndex, int indexAlongRoad)
        {
            RoadMapIndex = roadMapIndex;
            IndexAlongRoad = (ushort)indexAlongRoad;
        }

        public RoadIndexCode(KeyValuePair< WorldIdentifier, ushort> pair) : this(pair.Key,pair.Value)
        {
        }

        public void Write(BinaryWriter writer)
        {
            RoadMapIndex.Write(writer);
            writer.Write(IndexAlongRoad);
        }
        
        public static RoadIndexCode Read(BinaryReader reader)
        {
            var id = WorldIdentifier.Read(reader);
            var along = reader.ReadUInt16();

            return new RoadIndexCode(id, along);
        }

        public void Deconstruct(out WorldIdentifier roadId,out ushort indexAlongRoad)
        {
            roadId = this.RoadMapIndex;
            indexAlongRoad = this.IndexAlongRoad;
        }

        public override string ToString()
        {
            return $"{RoadMapIndex} [{IndexAlongRoad}]";
        }

        public override bool Equals(object? obj)
        {
            return obj is RoadIndexCode idx && this.Equals(idx);
        }

        public  bool Equals(RoadIndexCode obj)
        {
            return this.RoadMapIndex == obj.RoadMapIndex && this.IndexAlongRoad == obj.IndexAlongRoad;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.RoadMapIndex, this.IndexAlongRoad);
        }

        public RoadIndexCode Next()
        {
            return new RoadIndexCode(RoadMapIndex, IndexAlongRoad + 1);
        }

        public static bool operator==(in RoadIndexCode a,in RoadIndexCode b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(in RoadIndexCode a, in RoadIndexCode b)
        {
            return !a.Equals(b);
        }
    }*/

}
