﻿using System.Collections.Generic;

namespace TrackPlanner.Mapping.Data
{
    public enum WayKind : byte
    {
        Highway = 0, // AKA motorway
        HighwayLink = 1,
        Trunk = 2,
        TrunkLink = 3,
        Primary = 4,
        PrimaryLink = 5,
        Secondary = 6,
        SecondaryLink = 7,
        Tertiary = 8,
        TertiaryLink = 9,
        Cycleway = 10,

        /// <summary>
        /// if surface is not given assume both paved and unpaved
        /// </summary>
        Unclassified = 11,
        Footway = 12,
        Steps = 13,
        Ferry = 14,

        /// <summary>
        /// if surface is not given assume unpaved
        /// </summary>
        Path = 15,
        Crossing = 16,
    }

    public static class WayKindExtension
    {
        public static int IndexOf(this WayKind kind)
        {
            return (int) kind;
        }

        public static bool IsStable(this WayKind kind)
        {
            return kind != WayKind.Path;
        }
    }
}