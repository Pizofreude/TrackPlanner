﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using TrackPlanner.Mapping.Disk;

namespace TrackPlanner.Mapping.Data
{
    // https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.structlayoutattribute.pack?view=net-6.0
    // https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.layoutkind?view=net-6.0
    
    [StructLayout(LayoutKind.Sequential, Pack=2)]
    public readonly struct RoadIndexLong<TRoadId> : IEquatable<RoadIndexLong<TRoadId>>
    {
        private static readonly NumberFormatInfo nfi;

        static RoadIndexLong()
        {
            nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = "_";
        }

        public static RoadIndexLong<TRoadId> InvalidIndex(TRoadId roadId)
        {
            return new RoadIndexLong<TRoadId>(roadId, ushort.MaxValue);
        }

        public TRoadId RoadMapIndex { get; }
        public ushort IndexAlongRoad { get; }

        public RoadIndexLong(TRoadId roadMapIndex, int indexAlongRoad)
        {
            RoadMapIndex = roadMapIndex;
            IndexAlongRoad = (ushort)indexAlongRoad;
        }

        public RoadIndexLong(KeyValuePair< TRoadId, ushort> pair) : this(pair.Key,pair.Value)
        {
        }

        public void Write(BinaryWriter writer)
        {
            DiskHelper.WriteId(writer,this.RoadMapIndex);
            writer.Write(IndexAlongRoad);
        }

        public static RoadIndexLong<TRoadId> Read(BinaryReader reader)
        {
            TRoadId id = DiskHelper.ReadId<TRoadId>(reader);
            var along = reader.ReadUInt16();

            return new RoadIndexLong<TRoadId>(id, along);
        }

        public void Deconstruct(out TRoadId roadId,out ushort indexAlongRoad)
        {
            roadId = this.RoadMapIndex;
            indexAlongRoad = this.IndexAlongRoad;
        }

        public override string ToString()
        {
//            return $"{RoadId.ToString("#,0",nfi)} [{IndexAlongRoad}]";
            return $"{RoadMapIndex} [{IndexAlongRoad}]";
        }

        public override bool Equals(object? obj)
        {
            return obj is RoadIndexLong<TRoadId> idx &&  this.Equals(idx);
        }

        public  bool Equals(RoadIndexLong<TRoadId> obj)
        {
            return EqualityComparer<TRoadId>.Default.Equals(this.RoadMapIndex,obj.RoadMapIndex) && this.IndexAlongRoad == obj.IndexAlongRoad;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.RoadMapIndex, this.IndexAlongRoad);
        }

        public RoadIndexLong<TRoadId> Next()
        {
            return new RoadIndexLong<TRoadId>(RoadMapIndex, IndexAlongRoad + 1);
        }

        public static bool operator==(in RoadIndexLong<TRoadId> a,in RoadIndexLong<TRoadId> b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(in RoadIndexLong<TRoadId> a, in RoadIndexLong<TRoadId> b)
        {
            return !a.Equals(b);
        }
    }

}
