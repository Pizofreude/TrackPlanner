using System;
using System.Collections.Generic;
using MathUnit;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public interface IReadOnlyGridCell<TNodeId,TRoadId>
        where TNodeId:struct
    where TRoadId:notnull
    {
        IReadOnlyList<RoadIndexLong<TRoadId>> RoadSegments { get; }
        IEnumerable<TNodeId> GetSegmentNodes(IWorldMap<TNodeId,TRoadId> map);
        IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> GetSnaps(IWorldMap<TNodeId,TRoadId> map, 
            IGeoCalculator calc,GeoZPoint point, Length snapLimit,Func<RoadInfo<TNodeId>,bool>? predicate);
    }
}