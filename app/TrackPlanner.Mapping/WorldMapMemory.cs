﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Geo;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Storage;
using TrackPlanner.Structures;
using TNodeId = System.Int64;
using TRoadId = System.Int64;

namespace TrackPlanner.Mapping
{
    public sealed class WorldMapMemory : IWorldMap<TNodeId, TRoadId>
    {
        public static WorldMapMemory CreateOnlyRoads(ILogger logger,
            IReadOnlyMap<TNodeId, GeoZPoint> nodes,
            IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads,
            NodeRoadsDictionary<TNodeId, TRoadId> backReferences,
            List<(TNodeId, TouristAttraction)> attractions,
            List<(TNodeId, CityInfo)> cities,
            int gridCellSize, string? debugDirectory)
        {
            return new WorldMapMemory(logger, nodes, roads,
                backReferences,attractions,
                forests: null, rivers: null, cities, waters: null, protectedArea: null,
                noZone: null, railways: null, gridCellSize, debugDirectory, onlyRoads: true);
        }

        private readonly IReadOnlyList<NodeLine<TNodeId>>? _protected;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? forests;

        private readonly NodeRoadsDictionary<TNodeId, TRoadId> nodeRoadReferences;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? noZone;
        private readonly ILogger logger;
        private readonly bool onlyRoads;

        private readonly IReadOnlyList<NodeLine<TNodeId>>? railways;
        private readonly IReadOnlyList<(RiverKind kind, NodeLine<TNodeId> indices)>? rivers;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? waters;

        private IReadOnlySet<TNodeId>? bikeFootDangerousNearbyNodes;
        private RoadGridMemory grid;
        public IGrid<TNodeId, TRoadId> Grid => this.grid;
        public IReadOnlyList<(TNodeId nodeId, TouristAttraction attraction)> Attractions { get; set; }

        private IReadOnlyMap<TNodeId, GeoZPoint> nodes { get; }
        private IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads { get; }
        private IReadOnlyList<NodeLine<TNodeId>> Railways => this.railways ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<NodeLine<TNodeId>> Forests => this.forests ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<(RiverKind kind, NodeLine<TNodeId> indices)> Rivers => this.rivers ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        public IReadOnlyList<(TNodeId nodeId,CityInfo info)> Cities { get; }
        private IReadOnlyList<NodeLine<TNodeId>> Waters => this.waters ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<NodeLine<TNodeId>> Protected => this._protected ?? throw new InvalidOperationException("Map was loaded only with roads info.");

        private IReadOnlyList<NodeLine<TNodeId>> NoZone => this.noZone ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        public Angle Southmost { get; }
        public Angle Northmost { get; }
        public Angle Eastmost { get; }
        public Angle Westmost { get; }

        public WorldMapMemory(ILogger logger,
            IReadOnlyMap<TNodeId, GeoZPoint> nodes,
            IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads,
            NodeRoadsDictionary<TNodeId, TRoadId> backReferences,
            IReadOnlyList<(TNodeId nodeId, TouristAttraction attraction)> attractions,
            List<NodeLine<TNodeId>>? forests,
            List<(RiverKind kind, NodeLine<TNodeId> indices)>? rivers,
            List<(TNodeId,CityInfo)> cities,
            List<NodeLine<TNodeId>>? waters,
            List<NodeLine<TNodeId>>? protectedArea,
            List<NodeLine<TNodeId>>? noZone,
            List<NodeLine<TNodeId>>? railways,
            int gridCellSize, string? debugDirectory,
            bool onlyRoads)
        {
            this.nodes = nodes;
            this.logger = logger;
            this.onlyRoads = onlyRoads;

            logger.Verbose($"Creating map with {roads.Count} roads");

            this.roads = roads;
            Attractions = attractions;
            this.Cities = cities;
            if (!onlyRoads)
            {
                this.forests = forests;
                this.rivers = rivers;
                this.waters = waters;
                this._protected = protectedArea;
                this.noZone = noZone;
                this.railways = railways;
            }

            validate(this.roads.SelectMany(it => it.Value.Nodes));
            validate(cities.Select(it => it.Item1));
            if (!onlyRoads)
            {
                validate(forests!.SelectMany(it => it.Nodes));
                validate(rivers!.SelectMany(it => it.indices.Nodes));
                validate(waters!.SelectMany(it => it.Nodes));
                validate(railways!.SelectMany(it => it.Nodes));
                validate(protectedArea!.SelectMany(it => it.Nodes));
                validate(noZone!.SelectMany(it => it.Nodes));
            }

            Southmost = this.nodes.Min(n => n.Value.Latitude);
            Northmost = this.nodes.Max(n => n.Value.Latitude);
            Eastmost = this.nodes.Max(n => n.Value.Longitude);
            Westmost = this.nodes.Min(n => n.Value.Longitude);

            this.nodeRoadReferences = backReferences;

            {
                var calc = new ApproximateCalculator();
                this.grid = new RoadGridMemory(logger,
                    new RoadGridMemoryBuilder(logger, this, calc, gridCellSize, debugDirectory).BuildCells(),
                    this, calc, gridCellSize, debugDirectory, legacyGetNodeAllRoads: false);
            }
        }


        // note we can get even for the same road multiple indices, example case: roundabouts -- "start" and "end" are at the same point
        public IReadOnlySet<RoadIndexLong<TRoadId>> GetRoadsAtNode(TNodeId nodeId)
        {
            return this.nodeRoadReferences[nodeId];
        }

        public GeoZPoint GetPoint(TNodeId nodeId)
        {
            return this.nodes[nodeId];
        }

        public IEnumerable<KeyValuePair<TNodeId, GeoZPoint>> GetAllNodes()
        {
            return nodes;
        }

        public string GetStats()
        {
            return this.Grid.GetStats();
        }

        public void SetDangerous(IReadOnlySet<TNodeId> dangerousNearbyNodes)
        {
            this.bikeFootDangerousNearbyNodes = dangerousNearbyNodes;
        }

        public bool IsBikeFootRoadDangerousNearby(TNodeId nodeId)
        {
            //return this.bikeFootDangerousNearbyNodes.TryGetValue(roadId, out var indices) && indices.Contains(nodeId);
            return this.bikeFootDangerousNearbyNodes!.Contains(nodeId);
        }

        public RoadInfo<TNodeId> GetRoad(TRoadId roadId)
        {
            return roads[roadId];
        }

        public IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads()
        {
            return roads;
        }

        private void validate(IEnumerable<TNodeId> nodeReferences)
        {
            foreach (var node_id in nodeReferences)
                if (!nodes.ContainsKey(node_id))
                    throw new KeyNotFoundException($"Cannot find reference node {node_id}");
        }

        internal void Write(long timestamp, Stream stream)
        {
            throw new NotSupportedException();
            if (!onlyRoads)
                throw new Exception("Can write only roads map");

            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);
                writer.Write(timestamp);
                writer.Write(Grid.CellSize);

                GeoZPoint.WriteFloatAngle(writer, this.Northmost);
                GeoZPoint.WriteFloatAngle(writer, this.Eastmost);
                GeoZPoint.WriteFloatAngle(writer, this.Southmost);
                GeoZPoint.WriteFloatAngle(writer, this.Westmost);

                Dictionary<TNodeId, long> node_offsets;

                using (new OffsetKeeper(writer)) // grid offset
                {
                    using (new OffsetKeeper(writer)) // roads offset
                    {
                        // ---- writing nodes -----------------------------

                        // we create array to make sure we have the same order while iterating in two loops (C# framework does not guarantee this) 
                        var map_node_ids = nodes.Keys.OrderBy(x => x).ToArray();

                        var writer_offsets = new WriterOffsets<TNodeId>(writer);

                        writer.Write(nodes.Count);
                        foreach (var node_id in map_node_ids)
                        {
                            writer.Write(node_id);
                            writer_offsets.Register(node_id);
                        }

                        foreach (var map_idx in map_node_ids)
                        {
                            var debug_pos = writer_offsets.AddOffset(map_idx);
                            writer.Write(this.bikeFootDangerousNearbyNodes!.Contains(map_idx));
                            nodes[map_idx].Write(writer);
                        }

                        node_offsets = writer_offsets.GetOffsets();

                        writer_offsets.WriteBackOffsets();
                    }

                    // ---- writing roads -----------------------------------
                    {
                        var map_road_ids = roads.Keys.OrderBy(x => x).ToArray();

                        var offsets = new WriterOffsets<TRoadId>(writer);

                        writer.Write(roads.Count);
                        foreach (var road_id in map_road_ids)
                        {
                            writer.Write(road_id);
                            offsets.Register(road_id);
                        }

                        foreach (var road_id in map_road_ids)
                        {
                            offsets.AddOffset(road_id);
                            writer.Write(road_id);
                            roads[road_id].Write(writer);
                        }

                        offsets.WriteBackOffsets();
                    }
                }
                // ---- writing grid ---------------------------

                grid.Write(writer, this, node_offsets);
            }
        }


        internal static WorldMapMemory ReadRawArray(ILogger logger, IEnumerable<string> fileNames,
            int gridCellSize, string? debugDirectory,
            out List<string> invalidFiles)
        {
            throw new NotSupportedException();
            // Loaded MEM in 131.860504244 s

// this way of reading, i.e. with mapping sparse identifiers into array indices
// is not flexible but it allowed to use arrays instead of dictionaries and 
// it saved around 3GB for node to roads references

            long? timestamp = null;

            //Console.WriteLine("PRESS KEY BEFORE STREAMS");
            //Console.ReadLine();

            invalidFiles = new List<string>();

            foreach (var fn in fileNames)
            {
                using (var reader = new BinaryReader(new FileStream(fn, FileMode.Open, FileAccess.Read), Encoding.UTF8, leaveOpen: false))
                {
                    var curr_version = reader.ReadInt32();
                    if (curr_version != StorageInfo.DataFormatVersion)
                    {
                        invalidFiles.Add(fn);
                        logger.Warning($"File {fn} uses format {curr_version}, supported {StorageInfo.DataFormatVersion}");
                        continue;
                    }

                    var ts = reader.ReadInt64();
                    if (!timestamp.HasValue)
                        timestamp = ts;
                    else if (timestamp != ts)
                        throw new ArgumentException($"Maps are not sync, road names use different identifiers {fn}.");

                    reader.ReadInt32(); // cell size

                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);


//                    logger.Info($"{fn} stats: {nameof(nodes_count)} {nodes_count}, {nameof(roads_count)} {roads_count}.");
                }
            }

            // here: 3 GB taken 

            //Console.WriteLine("PRESS KEY FOR REAL READ");
            //Console.ReadLine();

            var nodes = HashMap.Create<TNodeId, GeoZPoint>(); //capacity: total_nodes_count);
            var roads = HashMap.Create<TRoadId, RoadInfo<TNodeId>>(); //total_roads_count);

            var dangerous_nearby_nodes = new HashSet<TNodeId>();

            foreach (var fn in fileNames)
            {
                logger.Verbose($"Loading raw map from {fn}");

                using (var reader = new BinaryReader(new FileStream(fn, FileMode.Open, FileAccess.Read), Encoding.UTF8, leaveOpen: false))
                {
                    var curr_version = reader.ReadInt32();
                    if (curr_version != StorageInfo.DataFormatVersion)
                    {
                        continue;
                    }

                    reader.ReadInt64(); // timestamp
                    reader.ReadInt32(); // cell size

                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);
                    GeoZPoint.ReadFloatAngle(reader);

                    reader.ReadInt64(); // grid offset
                    var roads_offset = reader.ReadInt64();

                    {
                        var node_offsets = ReaderOffsets.Read(reader, r => r.ReadInt64());

                        foreach (var (id, offset) in node_offsets.Offsets.OrderBy(it => it.Value)) // sort in order to get sequential read from disk
                        {
                            reader.BaseStream.Seek(offset, SeekOrigin.Begin);

                            bool is_dangerous_nearby = reader.ReadBoolean();
                            var pt = GeoZPoint.Read(reader);
                            if (!nodes.TryGetValue(id, out var existing))
                            {
                                nodes.Add(id, pt);
                            }
                            else
                            {
                                if (pt != existing)
                                    throw new Exception($"Node {id} already exists at {existing}, while trying to add at {pt}");
                            }

                            if (is_dangerous_nearby)
                                dangerous_nearby_nodes.Add(id);
                        }
                    }

                    reader.BaseStream.Seek(roads_offset, SeekOrigin.Begin);

                    {
                        var road_offsets = ReaderOffsets.Read(reader, r => r.ReadInt64());

                        for (int i = 0; i < road_offsets.Count; ++i)
                        {
                            var road_id = reader.ReadInt64();
                            var road = RoadInfo<TNodeId>.Read(reader);
                            if (!roads.TryGetValue(road_id, out var existing))
                            {
                                roads.Add(road_id, road);
                            }
                            else
                            {
                                if (road != existing)
                                {
                                    if (road.TryMergeWith(existing, out RoadInfo<TNodeId> merged))
                                        roads[road_id] = merged;
                                    else
                                        throw new ArgumentException($"Road {road_id} already exists with {road}, while we have {existing}");
                                }
                            }
                        }
                    }
                }
            }

            nodes.TrimExcess();
            roads.TrimExcess();

            // here: 4.7 GB (+1.7GB) taken 
            // 27_309_382 nodes (exp: 28_137_923), 4_109_763 roads (exp: 4_185_103), 33_486_629 road points
//            logger.Verbose($"All maps loaded {nodes.Count} nodes (exp: {total_nodes_count}), {roads.Count} roads (exp: {total_roads_count}), {roads.Sum(it => it.Value.Nodes.Count)} road points");
            logger.Verbose($"All maps loaded {nodes.Count} nodes, {roads.Count} roads, {roads.Sum(it => it.Value.Nodes.Count)} road points");
            //Console.WriteLine("PRESS KEY FOR BACK REFS");
            //Console.ReadLine();
            var start = Stopwatch.GetTimestamp();
            var nodes_to_roads = new NodeRoadsDictionary<TNodeId, TRoadId>(nodes, roads);

            // here: 5.7 GB (+1GB) taken 

            logger.Verbose($"Nodes to roads references created in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            //Console.WriteLine("PRESS KEY STOP");
            //Console.ReadLine();

            var map = CreateOnlyRoads(logger, nodes, roads, nodes_to_roads,new List<(long, TouristAttraction)>(),
                new List<(long, CityInfo)>(),
                gridCellSize, debugDirectory);
            map.bikeFootDangerousNearbyNodes = dangerous_nearby_nodes;

            return map;
        }

        public void AttachDangerInNonMotorNodes(RoadGridMemory grid, Length highTrafficProximity)
        {
            this.grid = grid;
            long start = Stopwatch.GetTimestamp();

            // cycle/foot-way id -> node id (it is strange mapping, but retrieving indices during route searching is indirect, currently)
            var dangerous_nearby = new Dictionary<TRoadId, HashSet<TNodeId>>();
            // we no longer use this layout, but lets keep it here for the sake of history (maybe we will get back to this)

            foreach (var road in this.roads.Values)
            {
                if (!road.IsDangerous)
                    continue;

                foreach (var node_id in road.Nodes)
                {
                    foreach (var snap in grid.GetSnaps(this.nodes[node_id], highTrafficProximity,
                                 info => info.Layer == road.Layer && (info.Kind == WayKind.Cycleway || info.Kind == WayKind.Footway)))
                    {
                        var snapped_road = this.roads[snap.RoadIdx.RoadMapIndex];

                        if (!dangerous_nearby.TryGetValue(snap.RoadIdx.RoadMapIndex, out HashSet<TNodeId>? node_indices))
                        {
                            node_indices = new HashSet<TNodeId>();
                            dangerous_nearby.Add(snap.RoadIdx.RoadMapIndex, node_indices);
                        }

                        node_indices.Add(snapped_road.Nodes[snap.RoadIdx.IndexAlongRoad]);
                    }
                }
            }

            logger.Info($"High traffic nodes {dangerous_nearby.Sum(it => it.Value.Count)} computed in {((Stopwatch.GetTimestamp() - start + 0.0) / Stopwatch.Frequency)}s");

            this.SetDangerous(dangerous_nearby.Values.SelectMany(x => x).ToHashSet());
        }

        public long GetOsmRoadId(TRoadId roadId)
        {
            return roadId;
        }

        public long GetOsmNodeId(TNodeId nodeId)
        {
            return nodeId;
        }

        public IEnumerable<TouristAttraction> GetAttractions(long nodeId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(long, GeoPoint, TouristAttraction)> GetAttractionsWithin(Region region, TouristAttraction.Feature excludeFeatures)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(long, GeoPoint, CityInfo)> GetCitiesWithin(Region region)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(long, NodeInfo<long>)> GetNodesWithin(Region region)
        {
            throw new NotImplementedException();
        }

        public long FromOsmNodeId(long osmNodeId)
        {
            throw new NotImplementedException();
        }
    }
}