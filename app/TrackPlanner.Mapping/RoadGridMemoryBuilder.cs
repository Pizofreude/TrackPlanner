﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

using TNodeId = System.Int64;
using TRoadId = System.Int64;

namespace TrackPlanner.Mapping
{
    public sealed class RoadGridMemoryBuilder
    {
        public int CellSize { get; }

        private readonly WorldMapMemory mapMemory;
        private readonly IGeoCalculator calc;
        private readonly ILogger logger;
        private readonly string? debugDirectory;

        public RoadGridMemoryBuilder(ILogger logger, WorldMapMemory mapMemory, IGeoCalculator calc, 
            int gridCellSize, string? debugDirectory)
        {
            this.CellSize = gridCellSize;
            this.logger = logger;
            this.mapMemory = mapMemory;
            this.calc = calc;
            this.debugDirectory = debugDirectory;
        }
        
        public HashMap<CellIndex, RoadGridCell<TNodeId,TRoadId>> BuildCells()
        {
            var cells = HashMap.Create<CellIndex, RoadGridCell<TNodeId,TRoadId>>();

            RoadGridCell<TNodeId,TRoadId> select_cell(in GeoZPoint current, out CellIndex cellIndex)
            {
                cellIndex = CellIndex.Create(current.Latitude, current.Longitude,this.CellSize);

                if (!cells.TryGetValue(cellIndex, out var cell))
                {
                    cell = new RoadGridCell<TNodeId,TRoadId>();
                    cells.Add(cellIndex, cell);
                }

                return cell;
            }

            foreach (var (road_map_index, road_info) in this.mapMemory.GetAllRoads())
            {
                for (int i = 0; i < road_info.Nodes.Count - 1; ++i)
                {
                    var curr_idx = new RoadIndexLong<TRoadId>(road_map_index, i);
                    GeoZPoint curr_point = this.mapMemory.GetPoint(curr_idx);
                    GeoZPoint next_point = this.mapMemory.GetPoint(curr_idx.Next());

                    var curr_cell = select_cell(curr_point, out var curr_cell_idx);
                    curr_cell.AddSegment(curr_idx);
                    var next_cell = select_cell(next_point, out var next_cell_idx);
                    if (curr_cell != next_cell)
                    {
                        next_cell.AddSegment(curr_idx);
                        if (!isAdjacentOrSame(curr_cell_idx, next_cell_idx))
                        {
                            // fill_mid_cells(curr_idx, dist, curr_cell_idx, next_cell_idx, curr_point, next_point);

                            var occupied = new HashSet<RoadGridCell<TNodeId,TRoadId>>();
                            occupied.Add(curr_cell);
                            occupied.Add(next_cell);

                            // far from optimal -- from each corner of the cells calculate projection on the segment, compute in which
                            // cell crosspoint falls and register segment there
                            var corners = new HashSet<GeoZPoint>();

                            for (int lati=Math.Min( curr_cell_idx.LatitudeGridIndex, next_cell_idx.LatitudeGridIndex);lati<=Math.Max( curr_cell_idx.LatitudeGridIndex, next_cell_idx.LatitudeGridIndex);++lati)
                            for (int loni = Math.Min(curr_cell_idx.LongitudeGridIndex, next_cell_idx.LongitudeGridIndex); lati <= Math.Max(curr_cell_idx.LongitudeGridIndex, next_cell_idx.LongitudeGridIndex); ++loni)
                            {
                                foreach ((Angle lat, Angle lon) in getCellCorners(lati, loni))
                                {
                                    if (lat<curr_point.Latitude.Min(next_point.Latitude) 
                                        || lat>curr_point.Latitude.Max(next_point.Latitude)
                                        || lon<curr_point.Longitude.Min(next_point.Longitude) 
                                        || lat>curr_point.Longitude.Max(next_point.Longitude))
                                        continue;

                                    corners.Add(GeoZPoint.Create(lat, lon, null));
                                }
                            }

                            foreach (var pt in corners)
                            {
                                (_, var cx, _) = calc.GetDistanceToArcSegment( pt, curr_point, next_point );
                                var cx_cell = select_cell(cx, out _);
                                if (occupied.Add(cx_cell))
                                {
                                    cx_cell.AddSegment(curr_idx);
                                }
                            }
                            
                        }
                    }
                }
            }

            {
                var debug_stats = cells.Values.Select(it => it.RoadSegments.Count).OrderBy(x => x).ToArray();
                logger.Info($"Grid cells fill stats: count = {debug_stats.Length}, min = {debug_stats.First()}, median = {debug_stats[debug_stats.Length/2]}, max: {debug_stats.Last()}");
            }
            
            return cells;
        }

        private static bool isAdjacentOrSame(CellIndex indexA, CellIndex indexB)
        {
            return Math.Abs(indexA.LatitudeGridIndex - indexB.LatitudeGridIndex) + Math.Abs(indexA.LongitudeGridIndex- indexB.LongitudeGridIndex) <= 1;
        }

        private IEnumerable< (Angle lat, Angle lon)> getCellCorners(int latIndex, int lonIndex)
        {
            int lat_dir = Math.Sign(latIndex);
            int lon_dir = Math.Sign(lonIndex);
            
            // todo: use CellIndex methods
            yield return (Angle.FromDegrees(latIndex * 1.0 / this.CellSize), Angle.FromDegrees(lonIndex * 1.0 / this.CellSize));
            yield return (Angle.FromDegrees((latIndex+lat_dir) * 1.0 / this.CellSize), Angle.FromDegrees((lonIndex+lon_dir) * 1.0 / this.CellSize));
            yield return (Angle.FromDegrees((latIndex+lat_dir) * 1.0 / this.CellSize), Angle.FromDegrees(lonIndex * 1.0 / this.CellSize));
            yield return (Angle.FromDegrees(latIndex * 1.0 / this.CellSize), Angle.FromDegrees((lonIndex+lon_dir) * 1.0 / this.CellSize));
        }

    


    }
}