using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class MapTranslator
    {
        public static IEnumerable<TurnInfo<long,long>> TurnsToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,IEnumerable<TurnInfo<TNodeId,TRoadId>> turns)
            where TNodeId:struct
            where TRoadId:struct
        {
            return turns.Select(ti => new TurnInfo<long, long>(ti.Entity, ti.AtNode() ? map.GetOsmNodeId(ti.NodeId!.Value) : null,
                ti.AtRoundabout() ? map.GetOsmRoadId(ti.RoadId!.Value) : null,
                ti.Point, ti.TrackIndex, ti.RoundaboutCounter, ti.Forward, ti.Backward, ti.Reason));
        }

        public static RoadInfo<TTargetNodeId> ConvertRoadInfo<TSourceNodeId, TTargetNodeId>(RoadInfo<TSourceNodeId> roadInfo,
            Func<TSourceNodeId,TTargetNodeId> translateNodeId)
        {
            return new RoadInfo<TTargetNodeId>(roadInfo.Kind,
                roadInfo.HasName ? roadInfo.NameIdentifier : null,
                roadInfo.OneWay, roadInfo.IsRoundabout, roadInfo.Surface,
                roadInfo.Smoothness, roadInfo.Grade,
                roadInfo.HasAccess, roadInfo.HasSpeedLimit50, 
                roadInfo.BikeLane, roadInfo.IsSingletrack,
                roadInfo.UrbanSidewalk, roadInfo.Dismount, roadInfo.Layer,
                roadInfo.Nodes.Select(translateNodeId).ToList());
        }

        public static MapPoint<TNodeId> ConvertFromOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            MapPoint<long> place)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new MapPoint<TNodeId>(place.Point, place.NodeId == null ? null : map.FromOsmNodeId(place.NodeId.Value));
        }
        public static MapPoint<long> ConvertToOsm<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            MapPoint<TNodeId> place)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new MapPoint<long>(place.Point, place.NodeId == null ? null : map.GetOsmNodeId(place.NodeId.Value));
        }
        public static FragmentStep<long> ConvertToOsm<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            FragmentStep<TNodeId> step)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new FragmentStep<long>(step.Point, step.NodeId == null ? null : map.GetOsmNodeId(step.NodeId.Value),
                step.IncomingFlatDistance);
        }

        public static IEnumerable<MapPoint<long>> ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
          IEnumerable<MapPoint<TNodeId>> mapPoints)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return mapPoints.Select(p => ConvertToOsm(map,p));
        }

        public static IEnumerable<FragmentStep<long>> ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            IEnumerable<FragmentStep<TNodeId>> steps)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return steps.Select(p => ConvertToOsm(map,p));
        }

        public static RoutePlan<long, long> ConvertToOsm<TNodeId,TRoadId>(IWorldMap<TNodeId,TRoadId> map,
            RoutePlan<TNodeId, TRoadId> plan)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            RoutePlan<long, long> osm_plan = new RoutePlan<long, long>()
            {
                Legs = plan.Legs.Select(l => new LegPlan<long, long>()
                    {
                        Fragments = l.Fragments.Select(f => new LegFragment<long, long>()
                        {
                            Risk = f.Risk,
                            Mode = f.Mode,
                            IsForbidden = f.IsForbidden,
                            Steps = ConvertToOsm(map,f.Steps).ToList(),
                            UnsimplifiedFlatDistance = f.UnsimplifiedFlatDistance,
                            RawTime = f.RawTime,
                            RoadIds = f.RoadIds.Select(map.GetOsmRoadId).ToHashSet(),
                        }).ToList(),
                        UnsimplifiedDistance = l.UnsimplifiedDistance,
                        RawTime = l.RawTime,
                        IsDrafted = l.IsDrafted,
                        AutoAnchored = l.AutoAnchored
                    }
                ).ToList(),
                DailyTurns = plan.DailyTurns.Select(dt => map.TurnsToOsm(dt).ToList()).ToList(),
                ProblemMessage = plan.ProblemMessage,
            };

            return osm_plan;
        } 
        
        
    }
}