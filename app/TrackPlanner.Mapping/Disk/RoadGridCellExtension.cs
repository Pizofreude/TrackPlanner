﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping.Disk
{
    public static class RoadGridCellExtension
    {
        public static void WriteMemory(this RoadGridCell<long,long> cell, BinaryWriter writer,
            IWorldMap<long,long> map,
            IReadOnlyDictionary<long, long> nodeOffsets)
        {
            using (new OffsetKeeper(writer))
            {
                writer.Write(cell.RoadSegments.Count);
                foreach (var elem in cell.RoadSegments)
                    elem.Write(writer);
            }

            foreach (var node_id in cell.GetSegmentNodes(map).Distinct())
            {
                writer.Write(node_id);
                writer.Write(nodeOffsets[node_id]);
            }
        }
        
        public static RoadGridCell<TNodeId,TRoadId> Read<TNodeId,TRoadId>(CellIndex cellIndex, BinaryReader reader)
            where TNodeId:struct
        where TRoadId:notnull
        {
            reader.ReadInt64(); // nodes offset
            
            var count = reader.ReadInt32();
            var segments = new HashSet<RoadIndexLong<TRoadId>>(capacity: count);
            for (int i = 0; i < count; ++i)
                segments.Add(RoadIndexLong<TRoadId>.Read(reader));

            return new RoadGridCell<TNodeId,TRoadId>(segments.ToList());
        }
        
        public static unsafe RoadGridCell<TNodeId,TRoadId> Load<TNodeId,TRoadId>(CellIndex cellIndex, IReadOnlyList<BinaryReader> readers)
            where TNodeId:struct
        where TRoadId:notnull
        {
            var counts = stackalloc int[readers.Count];
            int total_count = 0;
            for (int r=0;r<readers.Count;++r)
            {
                readers[r].ReadInt64(); // nodes offset
                var c = readers[r].ReadInt32();
                counts[r] = c;
                total_count += c;
            }

            var segments = new HashSet<RoadIndexLong<TRoadId>>(capacity: total_count);

            for (int r = 0; r < readers.Count; ++r)
            {
                for (int i = 0; i < counts[r]; ++i)
                    segments.Add(RoadIndexLong<TRoadId>.Read(readers[r]));
            }

            return new RoadGridCell<TNodeId,TRoadId>( segments.ToList());
        }
    }

   
}
