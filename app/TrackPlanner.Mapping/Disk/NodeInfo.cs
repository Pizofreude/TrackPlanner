﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping.Disk
{

    public readonly struct NodeInfo<TRoadId> : IEquatable<NodeInfo<TRoadId>>
    {
        public GeoZPoint Point { get; }
        public IReadOnlySet<RoadIndexLong<TRoadId>> Roads { get; }

        public NodeInfo( GeoZPoint point, IReadOnlySet<RoadIndexLong<TRoadId>> roads)
        {
            Point = point;
            Roads = roads;
        }

        public override bool Equals(object? other)
        {
            return other is NodeInfo<TRoadId> info && this.Equals(info);
        }
        public bool Equals(NodeInfo<TRoadId> other)
        {
            return Point.Equals(other.Point) && Roads.SetEquals(other.Roads);
        }

        public override int GetHashCode()
        {
            var hash_code = new HashCode();
            hash_code.Add(Point);
            hash_code.InvokeAddHashCode(Roads.GetHashCodeSet());
            return hash_code.ToHashCode();
        }

        public void Write(BinaryWriter writer)
        {
            Point.Write(writer);
            writer.Write(Roads.Count);
            foreach (var road_idx in Roads)
            {
                road_idx.Write(writer);
            }
        }
        public static NodeInfo<TRoadId> Read(BinaryReader reader)
        {
            var point = GeoZPoint.Read(reader);
            var count = reader.ReadInt32();
            var roads = new HashSet<RoadIndexLong<TRoadId>>(count);
            for (int i=0;i<count;++i)
                roads.Add(RoadIndexLong<TRoadId>.Read(reader));

            return new NodeInfo<TRoadId>(point, roads);

        }

        public override string ToString()
        {
            return $"NodeInfo @{Point.Convert()} with {Roads.Count} roads.";
        }

        public static NodeInfo<TRoadId> Merge(NodeInfo<TRoadId> info1, NodeInfo<TRoadId> info2) 
        {
            if (info1.Point!=info2.Point)
                throw new ArgumentException($"Value collision, we have point {info1.Point} and {info2.Point}");

            return new NodeInfo<TRoadId>(info1.Point, info1.Roads.Concat(info2.Roads).ToHashSet());
        }
    }
  
}