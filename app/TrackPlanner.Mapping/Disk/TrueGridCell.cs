﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using TrackPlanner.Elevation;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.Mapping.Disk
{
    internal sealed class TrueGridCell<TNodeId,TRoadId> : RoadGridCell<TNodeId,TRoadId>
        where TNodeId:struct
    where TRoadId:notnull
    {
        private readonly IMap<ushort,NodeInfo<TRoadId>> nodes;
        private readonly IMap<ushort,RoadInfo<TNodeId>> roads;
        private readonly HashSet<ushort> bikeFootDangerousNearbyNodes;
        private readonly List<(ushort nodeIndex, TouristAttraction attraction)> nodeAttractions;
        private readonly List<(ushort nodeIndex, CityInfo info)> cities;

        public IEnumerable<(ushort nodeIndex, TouristAttraction attraction)> NodeAttractions => this.nodeAttractions;
        public IEnumerable<(ushort nodeIndex, CityInfo info)> Cities => cities;
        public IEnumerable<(ushort nodeIndex, NodeInfo<TRoadId> info)> Nodes => nodes.Select(it => (it.Key,it.Value));
        
        public TrueGridCell() : base()
        {
            this.nodes = new CompactDictionaryFill<ushort, NodeInfo<TRoadId>>();
            this.roads = new CompactDictionaryFill<ushort, RoadInfo<TNodeId>>();
            this.bikeFootDangerousNearbyNodes = new HashSet<ushort>();
            this.nodeAttractions = new List<(ushort nodeIndex, TouristAttraction attraction)>();
            this.cities = new List<(ushort nodeIndex, CityInfo info)>();
        }

        public TrueGridCell(IMap<ushort,NodeInfo<TRoadId>> nodes,
         IMap<ushort,RoadInfo<TNodeId>> roads,
         List<(ushort nodeIndex, TouristAttraction attraction)> attractions,
         List<(ushort nodeIndex, CityInfo info)> cities,
            HashSet<ushort> bikeFootDangerousNearbyNodes,
            List<RoadIndexLong<TRoadId>> segmets) : base( segmets)
        {
            this.nodes = nodes;
            this.roads = roads;
            this.nodeAttractions = attractions;
            this.cities = cities;
            this.bikeFootDangerousNearbyNodes = bikeFootDangerousNearbyNodes;
        }


        public GeoZPoint GetPoint(ushort nodeIndex)
        {
            return nodes[nodeIndex].Point;
        }
        
        public IReadOnlySet<RoadIndexLong<TRoadId>> GetRoads(ushort nodeIndex)
        {
            return nodes[nodeIndex].Roads;
        }

        public RoadInfo<TNodeId> GetRoad(ushort roadIndex)
        {
            return roads[roadIndex];
        }

        public bool IsBikeFootRoadDangerousNearby(ushort nodeIndex)
        {
            return this.bikeFootDangerousNearbyNodes.Contains(nodeIndex);
        }

        public void AddNode(ushort nodeIndex, NodeInfo<TRoadId> nodeInfo,bool bikeFootDangerous)
        {
            this.nodes.Add(nodeIndex,nodeInfo);
            if (bikeFootDangerous)
                this.bikeFootDangerousNearbyNodes.Add(nodeIndex);
        }

        public void UpdateNodes(IElevationMap elevation, HashSet<GeoPoint> missingCoordinates)
        {
            foreach (var (idx,info) in this.nodes.ToList())
            {
                if (!elevation.TryGetHeight(info.Point.Convert(), out var height))
                    missingCoordinates.Add(ElevationMap.SourceCoordinate(info.Point.Convert()));

                this.nodes[idx] = new NodeInfo<TRoadId>(info.Point.WithAltitude(height), info.Roads);
            }
        }

        public void AddRoad(ushort roadIndex, RoadInfo<TNodeId> roadInfo)
        {
            this.roads.Add(roadIndex,roadInfo);
        }

        public override void Write(BinaryWriter writer)
        {
            base.Write(writer);
            
            DiskHelper.WriteMap(writer,this.nodes,writer.Write, (info) => info.Write(writer));
            DiskHelper.WriteMap(writer,this.roads,writer.Write, (info) => info.Write(writer));
            
            DiskHelper.WriteList(writer,this.nodeAttractions, ( a) =>
            {
                writer.Write(a.nodeIndex);
                a.attraction.WriteAttraction(writer);
            });
            
            DiskHelper.WriteList(writer,this.cities, ( c) =>
            {
                writer.Write(c.nodeIndex);
                c.info.WriteCity(writer);
            });

            writer.Write(this.bikeFootDangerousNearbyNodes.Count);
            foreach (var index in this.bikeFootDangerousNearbyNodes)
                writer.Write(index);
        }
        
        public static TrueGridCell<TNodeId,TRoadId> Load(CellIndex _, IReadOnlyList<BinaryReader> readers)
        {
            // all those data can have overlaps between readers (i.e. between map files)
            IMap<ushort, NodeInfo<TRoadId>>?  nodes = null;
            IMap<ushort, RoadInfo<TNodeId>>? roads = null;
            List<(ushort nodeIndex, TouristAttraction attraction)>? attractions = null;
            List<(ushort nodeIndex, CityInfo info)>? cities = null;
             HashSet<ushort>? bike_foot_dangerous = null;
             HashSet<RoadIndexLong<TRoadId>>? road_segments = null;
             foreach (var reader in  readers)
             {
                 DiskHelper.ReadMap(reader,ref road_segments,RoadIndexLong<TRoadId>.Read);

                 DiskHelper.MergeRead(reader, ref nodes, capacity => new CompactDictionaryFill<ushort, NodeInfo<TRoadId>>(capacity),
                     r => r.ReadUInt16(), NodeInfo<TRoadId>.Read,NodeInfo<TRoadId>.Merge);

                 DiskHelper.ReadMap(reader, ref roads, 
                     capacity => new CompactDictionaryFill<ushort, RoadInfo<TNodeId>>(capacity),
                     r => r.ReadUInt16(), RoadInfo<TNodeId>.Read);
                 
                 DiskHelper.ReadList(reader,ref attractions, r =>
                 {
                     var idx = r.ReadUInt16();
                     var attr = DataDiskExtension.ReadAttraction(r);
                     return (idx,attr);
                 });

                 DiskHelper.ReadList(reader,ref cities, r =>
                 {
                     var idx = r.ReadUInt16();
                     var city = DataDiskExtension.ReadCity(r);
                     return (idx, city);
                 });

                 DiskHelper.ReadMap(reader,ref bike_foot_dangerous,r => r.ReadUInt16());
             }

             return new TrueGridCell<TNodeId,TRoadId>(nodes!,roads!,attractions!,cities!, bike_foot_dangerous!,
                 road_segments!.ToList());
        }
        
        
        public override (int, int) GetStats()
        {
            return (nodes.Count, this.roads.Count);
        }

        public void AddAttraction(WorldIdentifier nodeId, TouristAttraction attraction)
        {
           this.nodeAttractions.Add((nodeId.EntityIndex,attraction));
        }

        public void AddCity(WorldIdentifier nodeId, CityInfo info)
        {
            this.cities.Add((nodeId.EntityIndex,info));
        }
    }
}