﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Geo;
using MathUnit;
using TrackPlanner.Elevation;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Stored;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Storage;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Mapping.Disk
{
    internal sealed class TrueGridWorldMap : IWorldMap<TNodeId, TRoadId>
    {
        public static TrueGridWorldMap Create(ILogger logger,
            WorldMapMemory source, IElevationMap elevation,
            IdTranslationTable nodesTranslation,
            IdTranslationTable roadsTranslation,
            string? debugDirectory)
        {
            // ---- building translation tables
            foreach (var (src_node_id, pt) in source.GetAllNodes())
            {
                var cell_idx = source.Grid.GetCellIndex(pt.Latitude, pt.Longitude);
                nodesTranslation.AddOrGet(src_node_id, cell_idx);
            }

            foreach (var (src_road_id, road_info) in source.GetAllRoads())
            {
                var pt = source.GetPoint(road_info.Nodes.First());
                var cell_idx = source.Grid.GetCellIndex(pt.Latitude, pt.Longitude);
                roadsTranslation.AddOrGet(src_road_id, cell_idx);
            }

            // --- adding info

            var cells = HashMap.Create<CellIndex, TrueGridCell<TNodeId, TRoadId>>();

            foreach (var (src_node_id, pt) in source.GetAllNodes())
            {
                var cell_idx = source.Grid.GetCellIndex(pt.Latitude, pt.Longitude);
                if (!cells.TryGetValue(cell_idx, out var cell))
                {
                    cell = new TrueGridCell<TNodeId, TRoadId>();
                    cells.Add(cell_idx, cell);
                }

                var world_id = nodesTranslation.AddOrGet(src_node_id, cell_idx);
                var node_info = new NodeInfo<TRoadId>(pt,
                    convertRoadIndices(source.GetRoadsAtNode(src_node_id), roadsTranslation)
                        .ToHashSet());
                cell.AddNode(world_id.EntityIndex, node_info,
                    source.IsBikeFootRoadDangerousNearby(src_node_id));
            }

            {
                var missing_coords = new HashSet<GeoPoint>();
                foreach (var cell in cells.Values)
                {
                    cell.UpdateNodes(elevation,missing_coords);
                }

                if (missing_coords.Any())
                    throw new ArgumentException($"Cannot get elevation for {(String.Join("; ",missing_coords))}");
            }

            foreach (var (src_road_id, road_info) in source.GetAllRoads())
            {
                var pt = source.GetPoint(road_info.Nodes.First());
                var cell_idx = source.Grid.GetCellIndex(pt.Latitude, pt.Longitude);
                if (!cells.TryGetValue(cell_idx, out var cell))
                {
                    cell = new TrueGridCell<TNodeId, TRoadId>();
                    cells.Add(cell_idx, cell);
                }

                var world_id = roadsTranslation.AddOrGet(src_road_id, cell_idx);
                cell.AddRoad(world_id.EntityIndex, MapTranslator.ConvertRoadInfo(road_info, nodesTranslation.Get));
            }

            foreach (var (cell_idx, source_cell) in source.Grid.Cells)
            {
                if (!cells.TryGetValue(cell_idx, out var cell))
                {
                    cell = new TrueGridCell<TNodeId, TRoadId>();
                    cells.Add(cell_idx, cell);
                }

                foreach (var road_seg in convertRoadIndices(source_cell.RoadSegments, roadsTranslation))
                {
                    cell.AddSegment(road_seg);
                }
            }

            foreach (var (node_id, attraction) in source.Attractions)
            {
                var world_id = nodesTranslation.Get(node_id);
                var cell = cells[world_id.CellIndex];
                cell.AddAttraction(world_id, attraction);
            }

            foreach (var (node_id, info) in source.Cities)
            {
                var world_id = nodesTranslation.Get(node_id);
                var cell = cells[world_id.CellIndex];
                cell.AddCity(world_id, info);
            }

            return new TrueGridWorldMap(logger,
                source.Northmost,
                source.Eastmost,
                source.Southmost,
                source.Westmost,
                nodesTranslation,
                roadsTranslation,
                cells,
                source.Grid.CellSize, debugDirectory);
        }

        private static IEnumerable<RoadIndexLong<TRoadId>> convertRoadIndices(IEnumerable<RoadIndexLong<long>> osmRoadIndices,
            IdTranslationTable roadsTable)
        {
            return osmRoadIndices.Select(it => new RoadIndexLong<WorldIdentifier>(roadsTable.Get(it.RoadMapIndex), it.IndexAlongRoad));
        }

        private readonly ILogger logger;
        private readonly IdTranslationTable? nodesTranslation;
        private readonly IdTranslationTable? roadsTranslation;
        private readonly IReadOnlyBasicMap<CellIndex, TrueGridCell<TNodeId, TRoadId>> cells;

        private readonly RoadGridDisk<TNodeId, TRoadId, TrueGridCell<TNodeId, TRoadId>> grid;

        public Angle Eastmost { get; }
        public Angle Northmost { get; }
        public Angle Southmost { get; }
        public Angle Westmost { get; }
        public IGrid<TNodeId, TRoadId> Grid => grid;

        public TrueGridWorldMap(ILogger logger,
            Angle northmost,
            Angle eastmost,
            Angle southmost,
            Angle westmost,
            IdTranslationTable? nodesTranslation,
            IdTranslationTable? roadsTranslation,
            IReadOnlyEnumerableDictionary<CellIndex, TrueGridCell<TNodeId, TRoadId>> cells,
            int gridCellSize, string? debugDirectory)
        {
            this.logger = logger;
            this.nodesTranslation = nodesTranslation;
            this.roadsTranslation = roadsTranslation;
            this.cells = cells;
            Southmost = southmost;
            Northmost = northmost;
            Eastmost = eastmost;
            Westmost = westmost;


            var calc = new ApproximateCalculator();

            this.grid = new RoadGridDisk<TNodeId, TRoadId, TrueGridCell<TNodeId, TRoadId>>(logger, cells, this, calc,
                gridCellSize, debugDirectory, legacyGetNodeAllRoads: false);
        }

        internal static IDisposable Read(ILogger logger, IReadOnlyList<string> fileNames, MemorySettings memSettings,
            IdTranslationTable? nodes_table,
            IdTranslationTable? roads_table,
            string? debugDirectory,
            out TrueGridWorldMap map, out List<string> invalidFiles)
        {
            var result = DiskHelper.FilenamesToStreams(fileNames, out var files);

            result.Stack(ReadMap(logger, files, memSettings,
                nodes_table, roads_table,
                debugDirectory, out map, out invalidFiles));
            return result;
        }

        internal static IDisposable ReadMap(ILogger logger,
            IReadOnlyList<(Stream stream, string name)> files,
            MemorySettings memSettings,
            IdTranslationTable? nodesTranslationTable, IdTranslationTable? roadsTranslationTable,
            string? debugDirectory,
            out TrueGridWorldMap map,
            out List<string> invalidFiles)
        {
            invalidFiles = new List<string>();

            Angle northmost = -Angle.PI;
            Angle eastmost = -Angle.PI;
            Angle southmost = Angle.PI;
            Angle westmost = Angle.PI;

            int? cell_size = null;

            var offsets = new List<ReaderOffsets<CellIndex>>();
            var disposables = new List<IDisposable>();

            foreach (var (stream, fn) in files)
            {
                stream.Position = 0;
                logger.Verbose($"Loading raw map from {fn}");

                var reader = new BinaryReader(stream, Encoding.UTF8,
                    leaveOpen: true);
                var format = reader.ReadInt32();
                if (format != StorageInfo.DataFormatVersion)
                {
                    invalidFiles.Add(fn);
                    logger.Warning($"Format {format} not supported, expected {StorageInfo.DataFormatVersion}");
                    reader.Dispose();
                    continue;
                }

                int cs = reader.ReadInt32();
                if (cell_size == null)
                    cell_size = cs;
                else if (cell_size != cs)
                {
                    invalidFiles.Add(fn);
                    logger.Warning($"Invalid cell size {cs}, expected {cell_size}");
                    reader.Dispose();
                    continue;
                }

                northmost = northmost.Max(GeoZPoint.ReadFloatAngle(reader));
                eastmost = eastmost.Max(GeoZPoint.ReadFloatAngle(reader));
                southmost = southmost.Min(GeoZPoint.ReadFloatAngle(reader));
                westmost = westmost.Min(GeoZPoint.ReadFloatAngle(reader));

                ReaderOffsets<CellIndex> reader_offsets = ReaderOffsets.Read(reader, CellIndexExtension.Read);
                logger.Info($"Cells count {reader_offsets.Count}");
                offsets.Add(reader_offsets);
                disposables.Add(reader);
            }

            var cells = new DiskDictionary<CellIndex, TrueGridCell<TNodeId, TRoadId>>(offsets, extraOffset: 0,
                TrueGridCell<TNodeId, TRoadId>.Load, _ => { }, memSettings.WorldCacheCellsLimit);
            map = new TrueGridWorldMap(logger, northmost, eastmost, southmost, westmost,
                nodesTranslationTable, roadsTranslationTable, cells, cell_size!.Value, debugDirectory);

            return CompositeDisposable.Create(disposables);
        }

        internal void WriteMap(Stream stream)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);
                writer.Write(Grid.CellSize);

                GeoZPoint.WriteFloatAngle(writer, this.Northmost);
                GeoZPoint.WriteFloatAngle(writer, this.Eastmost);
                GeoZPoint.WriteFloatAngle(writer, this.Southmost);
                GeoZPoint.WriteFloatAngle(writer, this.Westmost);

                grid.Write(writer);
                this.logger.Info($"Cells stats: {this.grid.GetStats()}");
            }
        }

        internal static void WriteTranslationTable(Stream stream, IdTranslationTable translation)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);

                translation.Write(writer);
            }
        }

        internal static IdTranslationTable ReadIdTranslationTable( Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.UTF8, leaveOpen: true))
            {
                var format = reader.ReadInt32();
                if (format != StorageInfo.DataFormatVersion)
                    throw new NotSupportedException($"Format {format} not supported, expected {StorageInfo.DataFormatVersion}");

                return IdTranslationTable.Read(reader);
            }
        }

        public GeoZPoint GetPoint(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.GetPoint(nodeId.EntityIndex);
        }

        public RoadInfo<TNodeId> GetRoad(TRoadId roadId)
        {
            var cell = this.cells[roadId.CellIndex];
            return cell.GetRoad(roadId.EntityIndex);
        }

        public IEnumerable<KeyValuePair<TNodeId, GeoZPoint>> GetAllNodes()
        {
            throw new System.NotImplementedException();
        }

        public IReadOnlySet<RoadIndexLong<TRoadId>> GetRoadsAtNode(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.GetRoads(nodeId.EntityIndex);
        }


        public long GetOsmRoadId(TRoadId roadId)
        {
            return this.roadsTranslation?.GetOsm(roadId) ?? -1;
        }

        public long GetOsmNodeId(TNodeId nodeId)
        {
            return this.nodesTranslation?.GetOsm(nodeId) ?? -1;
        }

        public IEnumerable<TouristAttraction> GetAttractions(WorldIdentifier nodeId)
        {
            return this.cells[nodeId.CellIndex].NodeAttractions.Where(it => it.nodeIndex == nodeId.EntityIndex).Select(it => it.attraction);
        }

        public IEnumerable<(WorldIdentifier, GeoPoint, TouristAttraction)> GetAttractionsWithin(Region region,
            TouristAttraction.Feature excludeFeatures)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, attr) in cell.NodeAttractions)
                {
                    if ((attr.Features & excludeFeatures) != 0)
                        continue;

                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert();
                    if (region.IsWithin(pt))
                        yield return (world_id, pt, attr);
                }
            }
        }

        public IEnumerable<(WorldIdentifier, GeoPoint, CityInfo)> GetCitiesWithin(Region region)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, city) in cell.Cities)
                {
                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert();
                    if (region.IsWithin(pt))
                        yield return (world_id, pt, city);
                }
            }
        }

        public IEnumerable<(WorldIdentifier, NodeInfo<WorldIdentifier>)> GetNodesWithin(Region region)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, node_info) in cell.Nodes)
                {
                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert();
                    if (region.IsWithin(pt))
                        yield return (world_id, node_info);
                }
            }
        }

        public WorldIdentifier FromOsmNodeId(long osmNodeId)
        {
            return this.nodesTranslation?.Get(osmNodeId) ?? default;
        }

        public bool IsBikeFootRoadDangerousNearby(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.IsBikeFootRoadDangerousNearby(nodeId.EntityIndex);
        }

        public string GetStats()
        {
            return this.cells.ToString() ?? "";
        }


        public IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads()
        {
            throw new System.NotImplementedException();
        }
    }
}