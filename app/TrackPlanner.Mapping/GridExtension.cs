﻿using System.Collections.Generic;
using MathUnit;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class GridExtension
    {
        public static CellIndex GetCellIndex(this IGrid grid,Angle latitude, Angle longitude)
        {
            return CellIndex.Create(latitude, longitude, grid.CellSize);
        }
        
        public static IEnumerable<CellIndex> EnumerateCellIndices(this IGrid grid,Region region)
        {
            var ( min_lat_grid,  min_lon_grid) = grid.GetCellIndex(region.South, region.West);
            var ( max_lat_grid,  max_lon_grid) = grid.GetCellIndex(region.North,region.East);

            for (int lat_idx = min_lat_grid; lat_idx <= max_lat_grid; ++lat_idx)
            for (int lon_idx = min_lon_grid; lon_idx <= max_lon_grid; ++lon_idx)
            {
                yield return new CellIndex(latitudeGridIndex: lat_idx, longitudeGridIndex:  lon_idx); 
            }
        }
    }
}