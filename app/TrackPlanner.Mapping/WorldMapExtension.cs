﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using MathUnit;
using SharpKml.Dom;

using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Mapping
{
    public static class WorldMapExtension
    {
        public static string KmlDangerousTag => "dangerous";

        public static MapMode GetMapMode<TNodeId,TRoadId>()
        {
            if (typeof(TNodeId)==typeof(long) && typeof(TRoadId)==typeof(long))
                return MapMode.MemoryOnly;
            else if (typeof(TNodeId) == typeof(WorldIdentifier) && typeof(TRoadId) == typeof(WorldIdentifier))
                return MapMode.TrueDisk;
            else
                throw new NotSupportedException();
        }

        public static bool IsPeak<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId, Length altitude)
            where TNodeId:struct
            where TRoadId:notnull
        {
            int higher = 0;
            int equal = 0;
            foreach (var alt in map.GetAdjacentRoads(nodeId).Select(it => map.GetPoint(it).Altitude))
            {
                if (altitude < alt)
                    return false;
                else if (altitude > alt)
                    ++higher;
                else
                    ++equal;
            }

            return higher > 0 // we have at least one adjacent node below us
                   || equal == 0; // or there is no equals nodes, meaning it is lone node
        }

        public static int LEGACY_RoadSegmentsDistanceCount<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map,
            TRoadId roadId, int sourceIndex, int destIndex)
            where TNodeId:struct
        where TRoadId:notnull
        {
            int min_idx = Math.Min(sourceIndex, destIndex);
            int max_idx = Math.Max(sourceIndex, destIndex);

            int count = max_idx - min_idx;

            {
                // when end is looped  (EXCLUDING start-end)
                int conn_idx = map.GetRoad(roadId).Nodes.IndexOf(map.GetRoad(roadId).Nodes.Last());
                if (conn_idx != 0 && conn_idx != map.GetRoad(roadId).Nodes.Count - 1)
                {
                    count = Math.Min(count, map.GetRoad(roadId).Nodes.Count - 1 - max_idx + Math.Abs(conn_idx - min_idx));
                }
            }
            {
                // when start is looped (including start-end)
                int conn_idx = map.GetRoad(roadId).Nodes.Skip(1).IndexOf(map.GetRoad(roadId).Nodes.First());
                if (conn_idx != -1)
                {
                    ++conn_idx;

                    count = Math.Min(count, min_idx + Math.Abs(conn_idx - max_idx));
                }
            }

            // todo: add handling other forms of loops, like knots

            return count;
        }

        public static GeoZPoint GetRoundaboutCenter<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            IGeoCalculator calc, TRoadId roundaboutId)
            where TNodeId:struct
        where TRoadId:notnull
        {
            GeoZPoint min =  GeoZPoint.Create(Angle.PI, Angle.Zero, null);
            GeoZPoint max =  GeoZPoint.Create(-Angle.PI, Angle.Zero, null);
            foreach (var node in map.GetRoad(roundaboutId).Nodes)
            {
                GeoZPoint pt = map.GetPoint(node);
                if (min.Latitude >= pt.Latitude)
                    min = pt;
                if (max.Latitude <= pt.Latitude)
                    max = pt;
            }

            return calc.GetMidPoint(min, max);
        }

        public static void SaveAsKml<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            UserVisualPreferences visualPrefs, string path,bool flatRoads)
            where TNodeId : struct
            where TRoadId : notnull
        {
            var title = System.IO.Path.GetFileNameWithoutExtension(path);

            using (var stream = new FileStream(path, FileMode.CreateNew, FileAccess.Write))
            {
                SaveAsKml(map, visualPrefs, title, stream,flatRoads);
            }
        }

        public static void SaveAsKml<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            UserVisualPreferences visualPrefs, string title,Stream stream, bool flatRoads)
            where TNodeId:struct
            where TRoadId:notnull
        {
            var speed_lines = TrackWriter.GetKmlSpeedLines(visualPrefs);

            var input=  new TrackWriterInput() { Title = title };
            input.Lines = map.GetAllRoads().Select(it =>
            {
                var road = it.Value;
                GeoZPoint[] road_points = road.Nodes.Select(it =>
                {
                    var pt = map.GetPoint(it);
                    if (flatRoads)
                        pt = pt.WithAltitude(Length.Zero);
                    return pt;
                }).ToArray();
                return new LineDefinition(road_points, name: it.Key.ToString(),
                    description:road.DetailsToString(), speed_lines[ it.Value.GetRoadSpeedMode()]);
            }).ToList();
            
            input.Waypoints = map.GetAllNodes().Select(it => new WaypointDefinition (it.Value,$"{it.Key}",
                    description:(map.IsBikeFootRoadDangerousNearby(it.Key)?KmlDangerousTag:""), PointIcon.DotIcon))
                .ToList();

            var kml = input.BuildDecoratedKml();

            if ((kml.Root as Document)!.Features.Any())
            {
                kml.Save(stream);
            }
        }

        public static IWorldMap<long, long> ExtractMiniMap<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> sourceMap,
            ILogger logger, IGeoCalculator calc,
            Length range, int gridCellSize, string? debugDirectory,
            bool onlyRoads,
            params GeoPoint[] focusPoints)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : notnull, IEquatable<TRoadId>
        {
            var nodes_extract = HashMap.Create<TNodeId, GeoZPoint>();
            var roads_extract = HashMap.Create<TRoadId, RoadInfo<TNodeId>>();

            foreach (var (prev_point, next_point) in focusPoints.Slide())
            {
                var boundary = calc.GetBoundary(range, prev_point, next_point);
                foreach (var (node_id, node_info) in sourceMap.GetNodesWithin(boundary))
                {
                    if (nodes_extract.ContainsKey(node_id))
                        continue;
                    var (dist, _, _) = calc.GetDistanceToArcSegment(node_info.Point,
                        prev_point.Convert(), next_point.Convert());

                    if (dist > range)
                        continue;

                    nodes_extract.TryAdd(node_id, node_info.Point);

                    foreach (var road_id in node_info.Roads.Select(it => it.RoadMapIndex))
                        if (!roads_extract.ContainsKey(road_id))
                        {
                            roads_extract.TryAdd(road_id, sourceMap.GetRoad(road_id));
                        }
                }
            }

            if (onlyRoads)
                nodes_extract.Clear();

            foreach (var node_id in roads_extract.Values.SelectMany(it => it.Nodes))
            {
                nodes_extract.TryAdd(node_id, sourceMap.GetPoint(node_id));
            }

            var dangerous = new HashSet<long>();
            foreach (var node_id in nodes_extract.Keys)
                if (sourceMap.IsBikeFootRoadDangerousNearby(node_id))
                    dangerous.Add(sourceMap.GetOsmNodeId(node_id));

            var osm_nodes = nodes_extract.ToHashMap(it => sourceMap.GetOsmNodeId(it.Key),
                it => it.Value);
            var osm_roads = roads_extract.ToHashMap(it => sourceMap.GetOsmRoadId(it.Key),
                it => MapTranslator.ConvertRoadInfo(it.Value, sourceMap.GetOsmNodeId));
            var map_extract = WorldMapMemory.CreateOnlyRoads(logger, osm_nodes, osm_roads,
                new NodeRoadsDictionary<long, long>(osm_nodes, osm_roads),
                new List<(long, TouristAttraction)>(),
                new List<(long, CityInfo)>(),
                gridCellSize, debugDirectory);
            map_extract.SetDangerous(dangerous);
            return map_extract;
        }

        public static IEnumerable<RoadIndexLong<TRoadId>> GetAdjacentRoads<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map,
            TNodeId nodeId)
            where TNodeId:struct
        where TRoadId:notnull
        {
            foreach (var road_idx in map.GetRoadsAtNode(nodeId))
            {
                if (map.TryGetSibling(road_idx, -1, out var prev_road_idx))
                    yield return prev_road_idx;
                if (map.TryGetSibling(road_idx, +1, out var next_road_idx))
                    yield return next_road_idx;
            }
        }

        public static bool TryGetSibling<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            RoadIndexLong<TRoadId> current, int direction, out RoadIndexLong<TRoadId> sibling)
            where TNodeId:struct
        where TRoadId:notnull
        {
            if (direction != 1 && direction != -1)
                throw new ArgumentOutOfRangeException($"{nameof(direction)} = {direction}");

            var target_index = current.IndexAlongRoad + direction;

            if (target_index >= 0 && target_index < map.GetRoad(current.RoadMapIndex).Nodes.Count)
            {
                sibling = new RoadIndexLong<TRoadId>(current.RoadMapIndex, target_index);
                return true;
            }

            sibling = default;
            return false;
        }

        public static bool TryGetPrevious<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            RoadIndexLong<TRoadId> current, out RoadIndexLong<TRoadId> previous)
            where TNodeId:struct
        where TRoadId:notnull
        {
            return TryGetSibling(map, current, -1, out previous);
        }
        
        public static bool TryGetNext<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            RoadIndexLong<TRoadId> current, out RoadIndexLong<TRoadId> next)
            where TNodeId:struct
            where TRoadId:notnull
        {
            return TryGetSibling(map, current, +1, out next);
        }

        public static TNodeId GetNode<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            in RoadIndexLong<TRoadId> idx)
            where TNodeId:struct
            where TRoadId:notnull
        {
            var road_info = map.GetRoad(idx.RoadMapIndex);
            return road_info.Nodes[idx.IndexAlongRoad];
        }

        public static GeoZPoint GetPoint<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, in RoadIndexLong<TRoadId> idx)
            where TNodeId:struct
            where TRoadId:notnull
        {
            var node_id = map.GetNode(idx);
            return map.GetPoint(node_id);
        }

        public static bool IsCycleWay<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, TRoadId roadId) 
            where TNodeId:struct
            where TRoadId:notnull
            => map.GetRoad(roadId).Kind == WayKind.Cycleway;

        public static bool IsMotorRoad<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, TRoadId roadId)
            where TNodeId:struct
            where TRoadId:notnull
        {
            var road_kind = map.GetRoad(roadId).Kind;
            return road_kind != WayKind.Cycleway && road_kind != WayKind.Footway && road_kind != WayKind.Steps;
        }

        public static bool IsSignificantMotorRoad<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, TRoadId roadId) 
            where TNodeId:struct
            where TRoadId:notnull
            => map.IsMotorRoad(roadId) && map.GetRoad(roadId).Kind <= WayKind.Unclassified;

        internal static int CycleWeight<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, in RoadIndexLong<TRoadId> idx) 
            where TNodeId:struct
            where TRoadId:notnull
            => map.IsCycleWay(idx.RoadMapIndex) ? 1 : 0;

        public static bool IsRoadContinuation<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            TRoadId currentRoadId, TRoadId nextRoadId)
            where TNodeId:struct
            where TRoadId:notnull
        {
            return EqualityComparer<TRoadId>.Default.Equals(currentRoadId, nextRoadId)
                || (map.IsCycleWay(currentRoadId) && map.IsCycleWay(nextRoadId))
                || (map.GetRoad(currentRoadId).HasName && map.GetRoad(nextRoadId).HasName 
                                                     && map.GetRoad(currentRoadId).NameIdentifier == map.GetRoad(nextRoadId).NameIdentifier);
        }

        internal static bool IsRoadLooped<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, TRoadId roadId)
            where TNodeId:struct
            where TRoadId:notnull
        {
            return map.GetRoad(roadId).Nodes.Count != map.GetRoad(roadId).Nodes.Distinct().Count();
        }

        public static bool IsDirectionAllowed<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, 
            in RoadIndexLong<TRoadId> from, in RoadIndexLong<TRoadId> dest)
            where TNodeId:struct
            where TRoadId:notnull
        {
            if (!EqualityComparer<TRoadId>.Default.Equals( from.RoadMapIndex , dest.RoadMapIndex))
                throw new ArgumentException($"Cannot compute direction for two different roads {from.RoadMapIndex} {dest.RoadMapIndex}");
            if (EqualityComparer<TNodeId>.Default.Equals(map.GetNode(from) , map.GetNode(dest)))
                throw new ArgumentException($"Cannot compute direction for the same spot {from.IndexAlongRoad}");

            if (!map.GetRoad(from.RoadMapIndex).OneWay)
                return true;

            if (map.IsRoadLooped(from.RoadMapIndex))
                throw new ArgumentException($"Cannot tell direction of looped road {from.RoadMapIndex}");

            return dest.IndexAlongRoad > from.IndexAlongRoad;
        }

        public static Length GetRoadDistance<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map, IGeoCalculator calc, 
            RoadIndexLong<TRoadId> start, RoadIndexLong<TRoadId> dest)
            where TNodeId:struct
            where TRoadId:notnull
        {
            if (EqualityComparer<TNodeId>.Default.Equals( map.GetNode(start) ,map.GetNode(dest)))
                return Length.Zero;

            if (!EqualityComparer<TRoadId>.Default.Equals( start.RoadMapIndex ,dest.RoadMapIndex))
                throw new ArgumentException();

            if (start.IndexAlongRoad > dest.IndexAlongRoad)
                return GetRoadDistance(map, calc, dest, start);

            var length = Length.Zero;

            var curr = start;
            foreach (var next in GetRoadIndices(map, start, dest).Skip(1))
            {
                length += calc.GetDistance(map.GetPoint(curr), map.GetPoint(next));
                curr = next;
            }

            return length;
        }

        public static IEnumerable<RoadIndexLong<TRoadId>> GetRoadIndices<TNodeId,TRoadId>(this IWorldMap<TNodeId,TRoadId> map,
            RoadIndexLong <TRoadId>start, RoadIndexLong <TRoadId>dest)
            where TNodeId:struct
            where TRoadId:notnull
        {
            if (!EqualityComparer<TRoadId>.Default.Equals(start.RoadMapIndex, dest.RoadMapIndex))
                throw new ArgumentException();

            // todo: add road loop handling

            yield return start;

            int dir = dest.IndexAlongRoad.CompareTo(start.IndexAlongRoad);
            for (var curr = start; curr != dest;)
            {
                var next = new RoadIndexLong<TRoadId>(curr.RoadMapIndex, curr.IndexAlongRoad + dir);
                yield return next;
                curr = next;
            }
        }


    }

}
