﻿using System.IO;
using System.Runtime.CompilerServices;
using TrackPlanner.Shared.Data;

[assembly: InternalsVisibleTo("TrackPlanner.Tests")]

namespace TrackPlanner.Mapping
{
    public static class CellIndexExtension 
    {
        public static void Write(this CellIndex index, BinaryWriter writer)
        {
            writer.Write(index.LatitudeGridIndex);
            writer.Write(index.LongitudeGridIndex);
        }
        public static CellIndex Read(BinaryReader reader)
        {
            var latitudeGrid = reader.ReadInt16();
            var longitudeGrid = reader.ReadInt16();

            return new CellIndex(latitudeGridIndex: latitudeGrid, longitudeGridIndex:longitudeGrid);
        }


    }
    
}