﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.WebUI.Client;
using TrackPlanner.WebUI.Server.Controllers;
using TrackPlanner.WebUI.Server.Stored;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHost host = CreateHost(args);
            var ctrl = host.Services.GetRequiredService<PlannerController>();
            //  RunTest(ctrl);
            host.Run();
        }

        private static void RunTest(PlannerController ctrl)
        {
            if (false)
            {
                var schedule = new ScheduleJourney<TNodeId,TRoadId>();
                schedule.Days.Add(new ScheduleDay());
                var summary = schedule.GetSummary();
                ;
            }
            // if (false)
            {
                if (ctrl.TryLoadSchedule("jesien.trproj", out var schedule))
                {
                    //schedule.SplitDay(1, 5);
                    var summary = schedule.GetSummary();
                    Console.WriteLine(summary.Days[1].Checkpoints.Count);
                }
            }
            if (false)
            {
                
                if (ctrl.TryLoadSchedule("sztum_test.trproj", out var schedule))
                {
                    ctrl.SaveFullSchedule(new SaveRequest<TNodeId,TRoadId>(){ Path = "xxx.trproj", Schedule = schedule});
                }
            }

        }

        public static IHost CreateHost(string[] args)
        {
            var rest_custom_config_path =  ConfigHelper.InitializeConfigFile("restservice_settings.json",
                (RestServiceConfig.SectionName, RestServiceConfig.Defaults()));
            var web_custom_config_path = ConfigHelper.InitializeConfigFile("webui_settings.json", 
                (ScheduleSettings.SectionName, ScheduleSettings.Defaults()),
                (GlobalSettings.SectionName, GlobalSettings.Defaults()),
                (SystemConfiguration.SectionName,  SystemConfiguration.Defaults()));


            /*return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });*/

            // https://stackoverflow.com/a/37365382/6734314
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-5.0
            return Host.CreateDefaultBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;

                    config.Sources.Clear();

                    config
                        .SetBasePath(env.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                        .AddJsonFile(rest_custom_config_path, optional: false)
                        .AddEnvironmentVariables()
                        .AddCommandLine(args);

                    System.IO.File.Copy(web_custom_config_path,
                        destFileName:Path.Combine(env.ContentRootPath, "wwwroot", Constants.ConfigFilename), 
                        overwrite:true);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateOnBuild = false; // 2/2 step for controllers as services
                })
                .Build();
        }




    }
}
