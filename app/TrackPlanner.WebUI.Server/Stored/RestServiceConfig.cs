using TrackPlanner.PathFinder.Stored;

namespace TrackPlanner.WebUI.Server.Stored
{
    public sealed class RestServiceConfig
    {
        public static string SectionName { get; } = "Rest";
        public static string CorsPolicyName { get; } = "CorsPolicy";

        public string[] CorsOrigins { get; set; } = default!;

        public bool DummyRouting { get; set; }
        public string OsmSubdirectory { get; set; } = default!;
        public bool UseSummaryLightTheme { get; set; }
        public SummaryTheme SummaryLightTheme { get; set; } = default!;
        public SummaryTheme SummaryDarkTheme { get; set; } = default!;

        public SystemConfiguration SystemConfiguration { get; set; } = default!;

        public SummaryTheme GetSummaryActiveTheme() => UseSummaryLightTheme ? SummaryLightTheme : SummaryDarkTheme;

        public RestServiceConfig()
        {
            // do NOT put any defaults here, ConfigurationBinder is buggy
        }

        public static RestServiceConfig Defaults()
        {
            return new RestServiceConfig()
            {
                SystemConfiguration = new TrackPlanner.PathFinder.Stored.SystemConfiguration(),

                CorsOrigins = new[]
                {
                    "http://localhost:5200",
                },
                OsmSubdirectory = "poland",
                SummaryLightTheme = new SummaryTheme()
                {
                    BackgroundColor = "white",
                    TextColor = "black",
                    WarningTextColor = "red"
                },
                SummaryDarkTheme = new SummaryTheme()
                {
                    BackgroundColor = "black",
                    TextColor = "white",
                    WarningTextColor = "yellow"
                },
            };
        }
    }
}