using System.IO;
using System.Linq;
using System.Reflection;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.WebUI.Server
{
    public static class ConfigHelper
    {
        public static string InitializeConfigFile(string configFilename,
            params (string sectionName, object defaultConfig)[] configs)
        {
            string bin_directory = Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!;
            var path = System.IO.Path.Combine(bin_directory, "..", "config", configFilename);

            if (!System.IO.File.Exists(path))
            {
                var config_dir = System.IO.Path.GetDirectoryName(path);
                System.IO.Directory.CreateDirectory(config_dir!);
                System.IO.File.WriteAllText(path, new ProxySerializer().Serialize(
                    configs.ToDictionary(it => it.sectionName,it => it.defaultConfig)));
            }

            return path;
        }
    }
}