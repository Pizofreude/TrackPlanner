﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;
using TrackPlanner.Shared.Stored;
using TrackPlanner.WebUI.Server.Stored;
using TrackPlanner.WebUI.Server.Workers;
using TimeSpan = System.TimeSpan;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Server.Controllers
{
    [ApiController]
    [Route(Routes.Planner)]
    public sealed class PlannerController : ControllerBase
    {
        private readonly RestServiceConfig serviceConfig;
        private readonly Navigator navigator;
        private readonly ILogger logger;
        private readonly IWorker<TNodeId,TRoadId> worker;
        private readonly ProxySerializer serializer;

        internal PlannerController(ILogger? logger, IWorker<TNodeId,TRoadId>? worker, RestServiceConfig serviceConfig, 
            Navigator navigator)
        {
            this.worker = worker ?? throw new ArgumentNullException(nameof(worker));
            this.serviceConfig = serviceConfig;
            this.navigator = navigator;
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.serializer = new ProxySerializer();
            
            navigator.CreateSchedules();
        }


        [HttpGet(Methods.Get_About)]
        public string About()
        {
            this.logger.Info("Received about request");
            return "Hello world";
        }

        [HttpPost(Methods.Post_SaveSchedule)]
        public ActionResult SaveFullSchedule([FromBody] SaveRequest<TNodeId,TRoadId> request)
        {
            string full_path = saveSchedule(request.Schedule, request.Path);

            ConvertToKml(full_path, request.Schedule,turnsMode:false);
            if (request.Schedule.Route.DailyTurns.Any()) // checking whether turns were computed
                ConvertToKml(full_path, request.Schedule,turnsMode:true);

            SaveSummary(full_path, request.Schedule);

            return Ok();
        }

        public string saveSchedule(ScheduleJourney<TNodeId,TRoadId> schedule, string relativePath)
        {
            var full_path = System.IO.Path.Combine(this.navigator.GetSchedules(), relativePath);

            using (var stream = new FileStream(full_path, FileMode.Create))
            using (StreamWriter sw = new StreamWriter(stream))
            {
                sw.Write(serializer.Serialize(schedule));
            }

            return full_path;
        }

        internal void ConvertToKml(string path, ScheduleJourney<TNodeId,TRoadId>? schedule,bool turnsMode)
        {
            if (schedule == null)
                schedule = this.serializer.Deserialize<ScheduleJourney<TNodeId,TRoadId>>(System.IO.File.ReadAllText(path))!;
            var core_path = System.IO.Path.ChangeExtension(path, null);

            for (int day_idx = 0; day_idx < schedule.Days.Count; ++day_idx)
            {
                var legs = schedule.GetDayLegs(day_idx).ToList();

                // we cannot use ".day.kml" (i.e. with dot) pattern because Google Maps fails with "not supported format" (2022-05-31)
                using (var stream = new FileStream($"{core_path}-day-{DataFormat.Adjust(day_idx + 1, schedule.Days.Count)}{(turnsMode?"-turns":"")}.kml", FileMode.Create)) // allowing overwrite
                {
                    var title = $"Day-{day_idx + 1} {DataFormat.Format(legs.Select(it => it.RawTime).Sum())}, {DataFormat.FormatDistance(legs.Select(it => it.UnsimplifiedDistance).Sum(), withUnit: true)}";
                    List<TurnInfo<TNodeId,TRoadId>>? turns = null;
                    if (turnsMode) 
                        turns = schedule.Route.DailyTurns[day_idx];
                    TrackWriter.SaveAsKml(schedule.VisualPreferences, stream, title, legs, turns);
                }
            }
        }


        internal void SaveSummary(string path, ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            var core_path = System.IO.Path.ChangeExtension(path, null);

            SummaryJourney summary = schedule.GetSummary();

            int day_idx = -1;
            foreach (var day in summary.Days)
            {
                ++day_idx;

                using (var stream = new FileStream($"{core_path}-{DataFormat.Adjust(day_idx + 1, summary.Days.Count)}-summary.html", FileMode.Create)) // allowing overwrite
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.WriteLine(@$"<!DOCTYPE html>
<html>
                            <head>
                        <meta charset='utf-8'>
                        <title>Day {day_idx + 1} summary</title>
                        </head>
<body style='background-color: {this.serviceConfig.GetSummaryActiveTheme().BackgroundColor}; color:{this.serviceConfig.GetSummaryActiveTheme().TextColor}'>");

                        writer.WriteLine($"<h3>Day {day_idx + 1} summary</h3>");

                        int pt_idx = -1;
                        foreach (var checkpoint in day.Checkpoints)
                        {
                            ++pt_idx;
                            // writing surfaces
                            if (checkpoint.IncomingLegIndex is { } leg_idx)
                            {
                                var leg = schedule.Route.Legs[leg_idx];
                                writer.Write("<small><i>");
                                writer.Write("<div>");
                                var ele = checkpoint.ElevationStats;
                                writer.Write($"<b>Climbs</b> {DataFormat.FormatDistance(ele.ClimbDistance, withUnit:true)} / {DataFormat.FormatHeight(ele.ClimbHeight, withUnit: true)}, {ele.ClimbSlope}");
                                writer.Write("</div>");
                                writer.Write("<div>");
                                // surface info
                                writer.Write(String.Join(", ", leg.Fragments.Partition(it => it.Mode)
                                    .Select(it => $"{it.First().Mode.ToString().ToLowerInvariant()} {DataFormat.FormatDistance(it.Select(x => x.UnsimplifiedFlatDistance).Sum(), withUnit: true)}")));
                                writer.WriteLine("</div>");
                                writer.WriteLine("</i></small>");
                            }

                            writer.Write($"<div><b>{pt_idx + 1}. {checkpoint.Label}</b> ");
                            if (pt_idx == 0)
                                writer.Write($"{DataFormat.Format(checkpoint.Arrival)} {DataFormat.DecoFormatHeight(checkpoint.UserPoint.Altitude)}");
                            else
                                writer.Write($"{DataFormat.FormatDistance(checkpoint.IncomingDistance, true)} at {DataFormat.Format(checkpoint.Arrival)} {DataFormat.DecoFormatHeight(checkpoint.UserPoint.Altitude)}");
                            
                            if (checkpoint.TotalBreak != TimeSpan.Zero)
                                writer.Write($" &gt;&gt; {DataFormat.Format(checkpoint.Departure)}");
                            writer.WriteLine("</div>");
                            {
                                var events = String.Join(", ",
                                    ScheduleSummaryExtension.GetEventStats(checkpoint.EventCounters, summary.PlannerPreferences)
                                        .Select(it => $"{it.label}: {DataFormat.FormatEvent(it.count,it.duration)}"));

                                if (events != "")
                                {
                                    writer.WriteLine("<div>");
                                    writer.Write($"<i>{events}</i>");
                                    writer.WriteLine("</div>");
                                }
                            }
                        }

                        writer.Write($"<div><b>In total:</b> {DataFormat.FormatDistance(day.Distance, true)}");
                        if (day.LateCampingBy.HasValue)
                            writer.Write($", <span style='color:{this.serviceConfig.GetSummaryActiveTheme().WarningTextColor}'><b>running late by {DataFormat.Format(day.LateCampingBy.Value)}</b></span>");
                        writer.Write($"</div>");
                        writer.WriteLine(@" </body>
</html>");
                    }
                }
            }
        }

        [HttpPut(Methods.Put_ComputeTrack)]
        public RouteResponse<TNodeId,TRoadId> ComputeTrack([FromBody] PlanRequest<TNodeId> request)
        {
            this.logger.Info($"Received turner config : {request.TurnerPreferences}");

            if (!this.worker.TryComputeTrack(request, out var response))
            {
                this.logger.Info("Didn't find any route");
                return new RouteResponse<TNodeId,TRoadId>();
            }
            else
            {
                this.logger.Info("Route was found");

                return response;
            }
        }

        [HttpGet(Methods.Get_GetDirectory)]
        public ActionResult<DirectoryData> GetDirectoryEntries([FromQuery(Name = Parameters.Directory)] string? directory = null)
        {
            var main_dir = System.IO.Path.Combine(this.navigator.GetSchedules(),
                // for frontend it is root directory, but for us it is relative path
                (directory ?? "").TrimStart('/', '\\'));
            // get only relative portion
            var directories = System.IO.Directory.GetDirectories(main_dir).Select(it => System.IO.Path.GetFileName(it)!)
                .OrderBy(it => it.ToLowerInvariant()).ToArray();
            var files = System.IO.Directory.GetFiles(main_dir, "*" + SystemCommons.ProjectFileExtension).Select(it => System.IO.Path.GetFileName(it)!)
                .OrderBy(it => it!.ToLowerInvariant()!).ToArray();
            return new DirectoryData() {Directories = directories, Files = files};
        }

        internal bool TryLoadSchedule(string path, [MaybeNullWhen(false)] out ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            long start = Stopwatch.GetTimestamp();

            var json = System.IO.File.ReadAllText(System.IO.Path.Combine(this.navigator.GetSchedules(), path));
            long loaded = Stopwatch.GetTimestamp();
            schedule = this.serializer.Deserialize<ScheduleJourney<TNodeId,TRoadId>>(json);
            this.logger.Verbose($"Loaded in {(loaded - start) / Stopwatch.Frequency}s, deserialized in {(Stopwatch.GetTimestamp() - loaded) / Stopwatch.Frequency}s");

            if (schedule == null)
                return false;
            if (schedule.Settings == null)
            {
                this.logger.Warning($"No preferences saved, using defaults.");
                schedule.Settings = ScheduleSettings.Defaults();
            }
            if (schedule.Settings.RouterPreferences == null)
            {
                this.logger.Warning($"No preferences saved, using defaults.");
                schedule.Settings.RouterPreferences = ScheduleSettings.Defaults().RouterPreferences;
            }

            return true;
        }

        [HttpGet(Methods.Get_LoadSchedule)]
        public ActionResult<ScheduleJourney<TNodeId,TRoadId>> LoadSchedule([FromQuery(Name = Parameters.Path)] string path)
        {
            if (TryLoadSchedule(path, out var schedule))
                return schedule;
            else
                return NotFound();
        }
        
        [HttpPut(Methods.Put_ComputeAttractions)]
        public ActionResult<AttractionsResponse> GetAttractions([FromBody] AttractionsRequest request)
        {
            return new AttractionsResponse()
            {
                Attractions = this.worker.GetAttractions(request.CheckPoints, request.Range,request.ExcludeFeatures).ToList(),
            };
        }

        [HttpPut(Methods.Put_FindPeaks)]
        public ActionResult<PeaksResponse<TNodeId>> GetFindPeaks([FromBody] PeaksRequest request)
        {
            return new PeaksResponse<TNodeId>()
            {
                Peaks = this.worker.FindPeaks(request.FocusPoint, request.SearchRange,request.SeparationDistance,
                    request.Count).ToList(),
            };
        }
        
    }
}