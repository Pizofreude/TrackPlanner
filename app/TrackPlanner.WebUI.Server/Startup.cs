using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.WebUI.Server.Controllers;
using TrackPlanner.WebUI.Server.Stored;
using TrackPlanner.WebUI.Server.Workers;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Server
{
    public class Startup
    {
        private readonly Navigator navigator;
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            this.navigator = new Navigator(null);
        }

     
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
services
    .AddControllers()
    .AddControllersAsServices() // 1/2 step for controllers as services
    .AddNewtonsoftJson(json => NewtonOptionsFactory.CustomizeJsonOptions(json.SerializerSettings))
    ;

            //services.AddControllersWithViews();
            //services.AddRazorPages();
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "RestService", Version = "v1"}); });

            TrackPlanner.Shared.Logger.Create(System.IO.Path.Combine(navigator.GetOutput(), "log.txt"), out TrackPlanner.Shared.ILogger logger);

            services.AddSingleton<TrackPlanner.Shared.ILogger>(sp => logger);

            var rest_config = new RestServiceConfig();
            Configuration.GetSection(RestServiceConfig.SectionName).Bind(rest_config);
            
            if (false)
                SetupCors(services, rest_config);
            else
            {

            }


            if (rest_config.DummyRouting)
            {
                services.AddSingleton<IWorker<TNodeId,TRoadId>>(sp => new DummyWorker<TNodeId,TRoadId>(sp.GetService<TrackPlanner.Shared.ILogger>()));
            }
            else
            {
                services.AddSingleton<IWorker<TNodeId,TRoadId>>(sp =>
                {
                    RouteManager<TNodeId,TRoadId>.Create(sp.GetService<TrackPlanner.Shared.ILogger>(), 
                        navigator,
                        osmSubdirectory: rest_config.OsmSubdirectory, 
                        rest_config.SystemConfiguration,
                        out RouteManager<TNodeId,TRoadId> manager);
                    return new RealWorker<TNodeId,TRoadId>(sp.GetService<TrackPlanner.Shared.ILogger>(), manager);
                });
            }

            services.AddTransient<PlannerController>(sp => new PlannerController(sp.GetService<TrackPlanner.Shared.ILogger>(), sp.GetService<IWorker<TNodeId,TRoadId>>(),
                rest_config,
                navigator));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RestService v1"));
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            if (false)
            {
                app.UseCors(RestServiceConfig.CorsPolicyName);
            }
            else
            {
                // https://stackoverflow.com/questions/48285408/how-to-disable-cors-completely-in-webapi
                app.UseCors(x => x
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    //.WithOrigins("https://localhost:44351")); // Allow only this origin can also have multiple origins seperated with comma
                    .SetIsOriginAllowed(origin => true)); // Allow any origin
            }
            
            app.UseHttpsRedirection();

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
        
        public static void SetupCors(IServiceCollection services, RestServiceConfig restServiceConfig)
        {
            if (restServiceConfig.CorsOrigins == null)
                return;

            // todo: CORS https://pastebin.com/uxQT8g4A

            CorsServiceCollectionExtensions.AddCors(services, o => o.AddPolicy(RestServiceConfig.CorsPolicyName, builder =>
            {
                builder
                    .WithOrigins(restServiceConfig.CorsOrigins)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));
        }

    }
}
