﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Turner;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;

namespace TrackPlanner.WebUI.Server.Workers
{
    public sealed class RealWorker<TNodeId, TRoadId> : IWorker<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly RouteManager<TNodeId, TRoadId> manager;
        private readonly AttractionsWorker<TNodeId, TRoadId> attrWorker;

        public RealWorker(ILogger? logger, RouteManager<TNodeId, TRoadId>? manager)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.manager = manager ?? throw new ArgumentNullException(nameof(manager));
            this.attrWorker = new AttractionsWorker<TNodeId, TRoadId>(logger, manager.Map, manager.Calculator);
            logger.Info($"Starting {this}");
        }

        public bool TryComputeTrack(PlanRequest<TNodeId> request,
            [MaybeNullWhen(false)] out RouteResponse<TNodeId, TRoadId> response)
        {
            //foreach (var pt in request.Points)
//                this.logger.Info($"{pt.UserPoint.Latitude} {pt.UserPoint.Longitude}");

            List<RequestPoint<TNodeId>> req_points = request.GetPointsSequence().ToList();
            if (!manager.TryFindFlattenRoute(request.RouterPreferences,req_points,
                    CancellationToken.None, out List<LegRun<TNodeId, TRoadId>>? legs,
                    out string? problem))
            {
                response = null;
                return false;
            }

            var turner = new NodeTurnWorker<TNodeId, TRoadId>(logger, manager.Map,
                new SystemTurnerConfig() {DebugDirectory = manager.DebugDirectory!},
                request.TurnerPreferences);

            var daily_turns = new List<List<TurnInfo<TNodeId, TRoadId>>>();

            {
                int leg_offset = 0;
                for (int day_idx = 0; day_idx < request.DailyPoints.Count; ++day_idx)
                {
                    int leg_count = ScheduleExtension.GetLegCount(day_idx, request.DailyPoints[day_idx].Count,
                        // the anchor is already added at the end when creating request
                        addLoopedAnchor: false);

                    daily_turns.Add(turner.ComputeTurnPoints(legs.Skip(leg_offset).Take(leg_count)
                        .SelectMany(leg => leg.Steps.Select(it => it.Place)), ref problem));

                    leg_offset += leg_count;
                }
            }

            var plan = this.manager.CompactFlattenRoute(request.RouterPreferences, legs);
            if (problem != null)
                plan.ProblemMessage = problem;
            plan.DailyTurns = daily_turns;

            response = new RouteResponse<TNodeId,TRoadId>()
            {
                Route =plan,// MapTranslator.ConvertToOsm(this.manager.Map, plan),
                Names = this.manager.FindNames(request, plan),
            };

            return true;
        }

        public IEnumerable<List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, TouristAttraction.Feature excludeFeatures)
        {
            foreach (var attr_leg in this.attrWorker.FindAttractions(route, range, excludeFeatures))
                yield return attr_leg.Select(it =>
                        new PlacedAttraction(MapTranslator.ConvertToOsm(this.manager.Map, it.place), it.attraction))
                    .ToList();
        }

        public IEnumerable<MapPoint<TNodeId>> FindPeaks(GeoPoint focusPoint, Length searchRange,Length separationDistance, int count)
        {
            return this.manager.FindPeaks(focusPoint, searchRange, separationDistance, count);
        }
    }
}