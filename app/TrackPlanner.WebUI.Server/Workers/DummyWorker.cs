using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;

namespace TrackPlanner.WebUI.Server.Workers
{
    public sealed class DummyWorker<TNodeId,TRoadId> : IWorker<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly ILogger? logger;
        private readonly DraftHelper<TNodeId,TRoadId> helper;

        internal DummyWorker(ILogger? logger)
        {
            this.logger = logger ?? throw  new ArgumentNullException(nameof(logger));
            this.helper = new DraftHelper<TNodeId,TRoadId>(new ApproximateCalculator());
            logger.Info($"Starting {this}");
        }

        public bool TryComputeTrack(PlanRequest<TNodeId> request, 
            [MaybeNullWhen(false)] out RouteResponse<TNodeId, TRoadId> response)
        {
            response = this.helper.BuildDraftPlan(request);
            return true;
        }
    
        public IEnumerable< List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, TouristAttraction.Feature excludeFeatures)
        {
            yield break;
        }

        public IEnumerable<MapPoint<TNodeId>> FindPeaks(GeoPoint focusPoint, Length searchRange,Length separationDistance, int count)
        {
            yield break;
        }
    }

}