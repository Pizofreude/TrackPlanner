using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;


namespace TrackPlanner.WebUI.Server.Workers
{
    public interface IWorker<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        bool TryComputeTrack(PlanRequest<TNodeId> request, [MaybeNullWhen(false)] out RouteResponse<TNodeId, TRoadId> plan);

        IEnumerable<List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, TouristAttraction.Feature excludeFeatures);

        IEnumerable<MapPoint<TNodeId>> FindPeaks(GeoPoint focusPoint, Length searchRange,Length separationDistance,
            int count);
    }
}