using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;

namespace TrackPlanner.RestClient
{
    public sealed class RestWorker<TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId: struct
    {
        private readonly JsonRestClient rest;
        private readonly string serverAddress;
        private readonly DraftHelper<TNodeId,TRoadId> draftHelper;

        public RestWorker( JsonRestClient rest ,string serverAddress)
        {
            this.rest = rest;
            this.serverAddress = serverAddress;
            this.draftHelper = new DraftHelper<TNodeId,TRoadId>(new ApproximateCalculator());
        }

        public async ValueTask<( string? failure, AttractionsResponse? attractions)> GetAttractionsAsync(AttractionsRequest request,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_ComputeAttractions);
            return await rest.PutAsync<AttractionsResponse>(api_url, request, token).ConfigureAwait(false);
        }
        
        public async ValueTask<( string? failure, PeaksResponse<TNodeId>? attractions)> GetPeaksAsync(PeaksRequest request,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_FindPeaks);
            return await rest.PutAsync<PeaksResponse<TNodeId>>(api_url, request, token).ConfigureAwait(false);
        }
        public async Task<( string? failure, RouteResponse<TNodeId,TRoadId>? plan)>  GetPlanAsync(PlanRequest<TNodeId> request,
            bool calcReal,CancellationToken token)
        {
            RouteResponse<TNodeId,TRoadId>? response;
            
            if (calcReal)
            {
                Console.WriteLine("Sending plan request");
                var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_ComputeTrack);
                (string? failure, response) = await rest.PutAsync<RouteResponse<TNodeId,TRoadId>>(api_url,
                    request, token).ConfigureAwait(false);
                 if (failure != null)
                     return (failure, null);
                 
                 Console.WriteLine($"DEBUG, we have some auto anchors {response!.Route.Legs.Any(it => it.AutoAnchored)}");
            }
            else
            {
                response = this.draftHelper.BuildDraftPlan(request);
            }

            return (null,response);
        }

    }
}