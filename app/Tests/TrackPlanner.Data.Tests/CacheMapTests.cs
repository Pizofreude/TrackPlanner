using System;
using TrackPlanner.Structures;
using Xunit;

namespace TrackPlanner.Shared.Data.Tests
{

    public class CacheMapTests
    {
        [Fact]
        public void RemovalWhenAddingTest()
        {
            var map = new CacheMap<int, string>(_ => { }, 5);
            map.Add(1,"hello");
            map.Add(2,"a");
            map.Add(3,"q");
            map.Add(4,"s");
            map.Add(5,"t");
            map.Add(6,"y");
            map.TryGetValue(5, out _);
            map.Add(7,"u");
            map.Add(8,"m");
            map.Add(9,"hello");
        }
    }
    
}