using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Equivalency;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;
using Xunit;
using TimeSpan = System.TimeSpan;

namespace TrackPlanner.Shared.Data.Tests
{
    public class NewtonSerializationTests
    {
        [Fact]
        public void UserTurnerPreferencesSerializationTest()
        {
            var input = new UserTurnerPreferences()
            {
                TurnArmLength = Length.FromKilometers(3),
            };

            var json_options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, json_options);

            var output = JsonConvert.DeserializeObject<UserTurnerPreferences>(json_string, json_options);

            output.Should().BeEquivalentTo(input, setApproximateAngleOptions);
        }

        [Fact]
        public void UserRouterPreferencesSerializationTest()
        {
            var input = new UserRouterPreferences()
            {
                TrafficSuppression = Length.FromMeters(2),
                Speeds = new Dictionary<SpeedMode, SpeedInfo>()
                {
                    {SpeedMode.Asphalt, new SpeedInfo( Speed.FromMetersPerSecond(50), Speed.FromMetersPerSecond(70))},
                }
            };
            
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<UserRouterPreferences>(json_string, options);

            output.Should().BeEquivalentTo(input, setApproximateAngleOptions);
        }

        [Fact]
        public void RequestPointSerializationTest()
        {
            var input = new RequestPoint<long>(GeoPoint.FromDegrees(53.024, 18.60917),true, findLabel:true);
            
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<RequestPoint<long>>(json_string, options);

            output.Should().BeEquivalentTo(input, setApproximateAngleOptions);
        }

        [Fact]
        public void PlanRequestSerializationTest()
        {
            var user_points = new[] {GeoPoint.FromDegrees(53.024, 18.60917), GeoPoint.FromDegrees(53.15528, 18.61338),}
                .Select(it => new RequestPoint<long>(it, true,true))
                .ToList();
            
            var input = new PlanRequest<long>()
            {
                DailyPoints = new List<List<RequestPoint<long>>>(){user_points},
                TurnerPreferences = new UserTurnerPreferences()
                {
                    TurnArmLength = Length.FromKilometers(3),
                },
                RouterPreferences = new UserRouterPreferences()
                {
                    TrafficSuppression = Length.FromMeters(2),
                    Speeds = new Dictionary<SpeedMode, SpeedInfo>()
                    {
                        {SpeedMode.Asphalt, new SpeedInfo( Speed.FromMetersPerSecond(50), Speed.FromMetersPerSecond(70))},
                    }
                }
            };

            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<PlanRequest<long>>(json_string, options);

            output.Should().BeEquivalentTo(input, setApproximateAngleOptions);
        }

        [Fact]
        public void TurnInfoSerializationTest()
        {
            var input = new TurnInfo<long,long>(TurnInfo.EntityReference.Roundabout, 123,456,
                GeoPoint.FromDegrees(20, 30), 3, 14, true, true,reason:"hello");

            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<TurnInfo<long,long>>(json_string, options);

            input.Should().BeEquivalentTo(output);
        }
        
        [Fact]
        public void ScheduleAnchorTest()
        {
            var input = new ScheduleAnchor() {Label = "foo"};
            
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<ScheduleAnchor>(json_string, options);

            input.Should().BeEquivalentTo(output);
        }

        [Fact]
        public void SegmentDataSerializationTest()
        {
            var input = new LegFragment<long,long>()
            {
                IsForbidden = true,
                Steps = new List<FragmentStep<long>>(){new FragmentStep<long>(GeoZPoint.FromDegreesMeters(12, 34, 56),87, Length.FromMeters(333)) },
                UnsimplifiedFlatDistance = Length.FromMeters(67),
                RawTime = TimeSpan.FromSeconds(90),
                RoadIds = new HashSet<long>() {13},
            }
                    .SetSpeedMode(SpeedMode.Paved)
                ;
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<LegFragment<long,long>>(json_string, options);

            input.Should().BeEquivalentTo(output);
        }

        [Fact]
        public void RoutePlanSerializationTest()
        {

            var input = new RoutePlan<long,long>()
            {
                DailyTurns = new List<List<TurnInfo<long,long>>>()
                {
                   new List<TurnInfo<long,long>>()
                   {
                       new TurnInfo<long,long>( TurnInfo.EntityReference.Roundabout, 580,300, 
                           GeoPoint.FromDegrees(20, 30), 3, 14, true, true,reason:"world")
                   },
                },
                Legs = new List<LegPlan<long,long>>()
                {
                    new LegPlan<long,long>()
                    {
                        Fragments = new List<LegFragment<long,long>>()
                        {
                            new LegFragment<long,long>()
                            {
                                IsForbidden = true,
                                Steps = new List<FragmentStep<long>>(){new FragmentStep<long>(GeoZPoint.FromDegreesMeters(12, 34, 56),87, Length.FromMeters(333)) },
                                UnsimplifiedFlatDistance = Length.FromMeters(67),
                                RawTime = TimeSpan.FromSeconds(90),
                                RoadIds = new HashSet<long>() {13},
                            }
                                .SetSpeedMode(SpeedMode.Paved)
                        },


                    }

                },

            };

            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<RoutePlan<long,long>>(json_string, options);

            input.Should().BeEquivalentTo(output);
        }

        [Fact]
        public void AngleSerializationTest()
        {
            var input = Angle.FromDegrees(150);
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<Angle>(json_string, options);

            input.Should().BeEquivalentTo( output, setApproximateAngleOptions);
        }

        private static EquivalencyAssertionOptions<TExpectation> setApproximateAngleOptions<TExpectation>(EquivalencyAssertionOptions<TExpectation> options)
        {
            const int precision = 10;

            // https://stackoverflow.com/questions/36782975/fluent-assertions-approximately-compare-a-classes-properties
            options
                .Using<Angle>(ctx => ctx.Subject.Degrees.Should().BeApproximately(ctx.Expectation.Degrees, precision))
                .When(info => info.Type == typeof(Angle));

            return options;
        }


        [Fact]
        public void LengthSerializationTest()
        {
            var input = Length.FromMeters(120);
            var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonConvert.SerializeObject(input, options);

            var output = JsonConvert.DeserializeObject<Length>(json_string, options);

            Assert.Equal(input, output);

        }


        [Fact]
        public void GeoZPointSerializationTest()
        {
            {
                var input = GeoZPoint.Create(Angle.FromDegrees(90), Angle.FromDegrees(180), null);
                var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);
                var json_string = JsonConvert.SerializeObject(input, options);
                var output = JsonConvert.DeserializeObject<GeoZPoint>(json_string, options);

                Assert.Equal(input, output);
            }

            {
                var input = GeoZPoint.Create(Angle.FromDegrees(90), Angle.FromDegrees(180), Length.FromMeters(100));
                var options = NewtonOptionsFactory.BuildJsonOptions(compact:false);
                var json_string = JsonConvert.SerializeObject(input, options);
                var output = JsonConvert.DeserializeObject<GeoZPoint>(json_string, options);

                Assert.Equal(input, output);
            }
        }

    }
}
