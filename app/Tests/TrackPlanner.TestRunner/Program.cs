﻿using System.IO;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Tests;
using SystemConfiguration = TrackPlanner.PathFinder.Stored.SystemConfiguration;

namespace TrackPlanner.TestRunner
{
    class Program
    {
        private static readonly Length extractionRange = Length.FromMeters(25);

        private static readonly Navigator navigator = new Navigator("../../../../../..");

        static void Main(string[] args)
        {
            //var data = new TurnTestData();
            //new MiniWorldTurnTest().BrzozowoRoundaboutExitsTest();
            //new MiniWorldPlanTest().SliwiceShortcutTest();
            //test.BiskupiceSwitchFromCyclewayTest();
            //var test = new PackedMapTest();
            //test.AdditionsTest();
            //new CompactDictionaryTest().LongResizingTest();

            //   new Program().ExtractMiniMapFromFile(args, "gaski.kml");
            if (false)
            {
                new Program().extractMiniMapFromPoints("sliwice-shortcut.kml",
                    GeoPoint.FromDegrees(                                53.74666, 18.2053), //!
                    GeoPoint.FromDegrees(53.74556, 18.20648),
                    GeoPoint.FromDegrees(53.746, 18.20844),
                    GeoPoint.FromDegrees(53.74559, 18.21212),
                    GeoPoint.FromDegrees(                            53.72296, 18.21355), //!
                    GeoPoint.FromDegrees(53.72338, 18.20783),
                    GeoPoint.FromDegrees(53.72135, 18.19597),
                GeoPoint.FromDegrees(                            53.70839, 18.17358), // sliwice
                    GeoPoint.FromDegrees( 53.71378, 18.17259),
                    GeoPoint.FromDegrees(53.71914, 18.1778),
                    GeoPoint.FromDegrees(53.72352, 18.17874),
                    GeoPoint.FromDegrees(53.72963, 18.18617),
                GeoPoint.FromDegrees(                                53.74666, 18.2053) // last
                );
            }
            //  new MiniWorldTurnTest().ChoppedRoundaboutTest();

            //  new CompactDictionaryTest().RemovalTest(new CompactDictionaryFill<long, string>());
            //new MiniWorldPlanTest().FijewoShortcutTest();
         //   new SummaryTest().SingleAnchorPointSummaryTest();
          //  new MiniWorldElevationTests().ElevationSpeedTest();
        }

        public void ExtractMiniMapFromFile(string[] args, params string[] planFilenames)
        {
            var snap_limit = Length.FromMeters(5);
/*
            var config_builder = new ConfigurationBuilder()
                .AddJsonFile(EnvironmentConfiguration.Filename, optional: false)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            var env_config = new EnvironmentConfiguration();
            config_builder.GetSection(EnvironmentConfiguration.SectionName).CustomBind(env_config);
            //logger.Info($"{nameof(env_config)} {env_config}");
            env_config.Check();
*/
            var visual_prefs = new UserVisualPreferences();

            using (Logger.Create(System.IO.Path.Combine(navigator.GetOutput(), "log.txt"), out ILogger logger))
            {
                var sys_config = new SystemConfiguration() {CompactPreservesRoads = true};
                using (RouteManager<long, long>.Create(logger, navigator, "kujawsko-pomorskie",
                           sys_config, out var manager))
                {
                    foreach (var filename in planFilenames)
                    {
                        GeoPoint[] raw_track_plan = TrackReader.LEGACY_Read(
                            System.IO.Path.Combine(navigator.GetLegacyTracks(), filename))
                            .Select(it => it.Convert())
                            .ToArray();

                        var mini_map = manager.Map.ExtractMiniMap(logger, manager.Calculator, extractionRange,
                            sys_config.MemoryParams.GridCellSize, navigator.GetDebug(), onlyRoads: true,
                            raw_track_plan);
                        mini_map.SaveAsKml(visual_prefs,
                            System.IO.Path.Combine(navigator.GetMiniMaps(), filename), flatRoads: true);
                    }
                }
            }
        }

        private void extractMiniMapFromPoints(string filename, params GeoPoint[] points)
        {
            var title = System.IO.Path.GetFileNameWithoutExtension(filename);

            using (var mem_stream = new MemoryStream())
            {
                MiniWorld.ExtractMiniMapFromPoints(title, mem_stream, extractionRange, points);
                mem_stream.Position = 0;
                var target_path = DiskHelper.GetUniquePath(navigator.GetOutput(), filename);
                using (var fs_stream = new FileStream(target_path, FileMode.CreateNew, FileAccess.Write))                
                {
                    mem_stream.CopyTo(fs_stream);
                }

            }
        }
    }
}