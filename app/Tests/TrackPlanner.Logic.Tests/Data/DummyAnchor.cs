using System;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Logic.Tests.Data
{
    public class DummyAnchor : IAnchor
    {
        public TimeSpan UserBreak { get; set; }
        public string Label { get; set; } = default!;
        public bool IsUserLabel { get; set; }
        public bool IsPinned { get; set; }
        public GeoPoint UserPoint { get; set; }
    }
}