using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Logic.Tests.Data;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;

namespace TrackPlanner.Logic.Tests;

public class SummaryTest
{
    private static DummySchedule createDummySchedule()
    {
        var schedule = new DummySchedule()
        {
            Days = new List<DummyDay>()
            {
                new DummyDay()
                {
                    Anchors = new List<IAnchor>()
                    {
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(10, 10)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(10, 11)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(11, 11)}
                    }
                },
                new DummyDay()
                {
                    Anchors = new List<IAnchor>()
                    {
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(12, 11)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(12, 10)}
                    }
                },
            }
        };
        schedule.Settings.LoopRoute = true;

        if (false)
        {
            schedule.Route = new RoutePlan<long, long>()
            {
                Legs = new List<LegPlan<long, long>>()
                {
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(100)},
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(200)},

                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(400)},
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(800)},
                    // and looped leg
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(20)},
                }
            };
        }
        else
        {
            var helper = new DraftHelper<long, long>(new ApproximateCalculator());
            var response = helper.BuildDraftPlan(schedule.BuildPlanRequest<long>());
            schedule.Route = response.Route;
        }

        return schedule;
    }

    [Fact]
    public void SingleAnchorSummaryTest()
    {
        var schedule = new DummySchedule()
        {
            Days = new List<DummyDay>()
            {
                new DummyDay() {Anchors = new List<IAnchor>() {new DummyAnchor()}},
            },
            Route = new RoutePlan<long, long>(),
        };
        schedule.Settings.LoopRoute = true;

        var summary = schedule.GetSummary();

        // nothing crashed = success
    }

    [Fact]
    public void SummaryDayDistancesTest()
    {
        var schedule = createDummySchedule();

        var summary = schedule.GetSummary();

        Assert.Equal(220701, summary.Days[0].Distance.Meters,0);
        Assert.Equal(442350, summary.Days[1].Distance.Meters,0);

        Assert.Equal(663050, summary.Distance.Meters,0);
    }
    
    [Fact]
    public void SingleAnchorPointSummaryTest()
    {
        var schedule = new DummySchedule();
        schedule.Settings.LoopRoute = true;
        schedule.Days.Add(new DummyDay());
        schedule.Days[0].Anchors.Add(new DummyAnchor(){ UserPoint = GeoPoint.FromDegrees(10,20)});

        var summary = schedule.GetSummary();

        var point = summary.Days[0].Checkpoints[0].UserPoint;
        Assert.Equal(10,point.Latitude.Degrees);
        Assert.Equal(20,point.Longitude.Degrees);
    }

    [Fact]
    public void DayDistancesByLegsTest()
    {
        var schedule = createDummySchedule();

        var legs_0 = schedule.GetDayLegs(0);
        var legs_1 = schedule.GetDayLegs(1);

        Assert.Equal(220701, legs_0.Select(it => it.UnsimplifiedDistance).Sum().Meters,0);
        Assert.Equal(442350, legs_1.Select(it => it.UnsimplifiedDistance).Sum().Meters,0);
    }

    [Fact]
    public void LastCheckpointPerDayTest()
    {
        var schedule = createDummySchedule();

        var summary = schedule.GetSummary();

        Assert.False(summary.Days[0].Checkpoints.Last().GetAtomicEvents(summary).Any());
        Assert.False(summary.Days[1].Checkpoints.Last().GetAtomicEvents(summary).Any());
    }
}