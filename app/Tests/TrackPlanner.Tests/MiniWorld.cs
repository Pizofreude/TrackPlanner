using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using SharpKml.Base;
using TrackPlanner.Turner;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Mapping.Stored;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Tests.Implementation;
using TrackPlanner.WebUI.Server.Workers;
using Xunit;
using SystemConfiguration = TrackPlanner.PathFinder.Stored.SystemConfiguration;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Tests
{
    public abstract class MiniWorld : MiniWorld<WorldIdentifier, WorldIdentifier>
    {
        
    }
    
    public abstract class MiniWorld<TNodeId,TRoadId>
        where TNodeId:struct,IEquatable<TNodeId>
        where TRoadId:struct,IEquatable<TRoadId>
    {
        protected const int Precision = 15;

        internal static Navigator Navigator { get; } = new Navigator("../../../../../..");
        private readonly MapMode mapMode;
        protected static ILogger NoLogger { get; } = new NoLogger();

        protected MiniWorld()
        {
            this.mapMode = WorldMapExtension.GetMapMode<TNodeId, TRoadId>();
        }


        public ScheduleJourney<TNodeId,TRoadId> CreateSchedule(params GeoPoint[] points)
        {
            var schedule = new ScheduleJourney<TNodeId,TRoadId>()
            {
                Settings = ScheduleSettings.Defaults(),
            };
            schedule.Settings.LoopRoute = false;
            schedule.Settings.RouterPreferences = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();
            // no compacting
            schedule.Settings.RouterPreferences.CompactingDistanceDeviation = Length.Zero;
            
            var day = new ScheduleDay()
            {
                Anchors = points.Select(pt => new ScheduleAnchor(){UserPoint = pt, IsPinned = true}).ToList(),
            }; 
            schedule.Days.Add(day);
            return schedule;
        }
        
        public static void ExtractMiniMapFromPoints(string title, Stream stream, Length extractionRange,
            params GeoPoint[] points)
        {
            var visual_prefs = new UserVisualPreferences();
            
                var sys_config = new SystemConfiguration(){ CompactPreservesRoads = true};
                sys_config.MemoryParams.SetEnableOsmId(true);
                using (RouteManager<WorldIdentifier,WorldIdentifier>.Create(NoLogger, Navigator,  "misc", 
                           sys_config, out var manager))
                {
                    {
                        var mini_map = manager.Map.ExtractMiniMap(NoLogger, manager.Calculator, extractionRange,
                            sys_config.MemoryParams.GridCellSize,Navigator.GetDebug(),onlyRoads:true,
                            points);
                        mini_map.SaveAsKml(visual_prefs, title,stream,flatRoads:true);
                    }
                }
            
        }

        protected static void SaveRoute<TMapPoint>(RoutePlan<long,long> plan, string mapFilename)
        {
            var input = new TrackWriterInput() {Title = null};
            int index = -1;
            Length? prev = null;
            foreach (var step in plan.Legs.SelectMany(it => it.AllSteps()))
            {
                ++index;
                string alt_diff;
                if (prev == null)
                    alt_diff = $"::{DataFormat.FormatHeight(step.Point.Altitude, false)}";
                else
                {
                    if (prev==step.Point.Altitude)
                        alt_diff = $"={DataFormat.FormatHeight(Length.Zero, false)}";
                    else if (prev>step.Point.Altitude)
                        alt_diff = $"v{DataFormat.FormatHeight(prev.Value-step.Point.Altitude, false)}";
                    else
                        alt_diff = $"^{DataFormat.FormatHeight(step.Point.Altitude-prev.Value, false)}";
                }
                
                input.AddPoint(step.Point, $"[{index}] #{step.NodeId}", comment: $">{DataFormat.FormatDistance(step.IncomingFlatDistance,withUnit:false)} {alt_diff}", PointIcon.DotIcon);
                prev = step.Point.Altitude;
            }

            saveToKml(input,mapFilename);
        }

        protected static void SaveData<TMapPoint>(IEnumerable<TMapPoint> plan, string mapFilename) 
        where TMapPoint:IMapPoint
        {
            var input = new TrackWriterInput() {Title = null};
            int index = -1;
            foreach (var place in plan)
            {
                ++index;
                input.AddPoint(place.Point, $"[{index}] {place}", comment: null, PointIcon.DotIcon);
            }

            saveToKml(input,mapFilename);
        }

        private static void saveToKml(TrackWriterInput input,string mapFilename)
        {
            var kml = input.BuildDecoratedKml();

            using (var stream = new FileStream(DiskHelper.GetUniquePath(Navigator.GetOutput(), 
                           "test-" + System.IO.Path.GetFileName(mapFilename)),
                       FileMode.CreateNew, FileAccess.Write))
            {
                kml.Save(stream);
            }
        }

        protected static void SaveData<TPlace>(IEnumerable<TPlace> plan, 
            IEnumerable<TurnInfo<long,long>> turns, string mapFilename)
        where TPlace:IMapPoint
        {
            var input = new TrackWriterInput() {Title = null};
            input.AddLine(plan.Select(it => it.Point).ToArray(), name: null, 
                new KmlLineDecoration(new Color32(0, 0, 0, 255), 1));
            input.AddTurns(turns, PointIcon.CircleIcon);

            saveToKml(input,mapFilename);
        }

        private static WorldMapMemory loadMiniMap(ILogger logger, Stream stream,
            int gridCellSize)
        {
            var kml_track = TrackReader.ReadUnstyled(stream);

            var nodes = HashMap.Create<long, GeoZPoint>();
            var rev_nodes = HashMap.Create<GeoPoint, long>();
            var roads = HashMap.Create<long, RoadInfo<long>>();
            var dangerous = new HashSet<long>();

            foreach (var waypoint in kml_track.Waypoints)
            {
                var id = long.Parse(waypoint.Name!);
                nodes.Add(id, waypoint.Point);
                rev_nodes.Add(waypoint.Point.Convert(), id);

                if (waypoint.Description == TrackPlanner.Mapping.WorldMapExtension.KmlDangerousTag)
                    dangerous.Add(id);
            }

            // when binding roads to nodes we don't use elevation on purpose, but in future
            // we should use ROAD elevation as indicator which node we should use (0, 1, 2)
            // in case of bridges etc.
            foreach (var line in kml_track.Lines)
            {
                var road_id = long.Parse(line.Name!);
                var info = RoadInfo<long>.Parse( line.Points.Select(it => rev_nodes[it.Convert()]).ToList(), line.Description!);
                roads.Add(road_id, info);
            }

            var world_map = WorldMapMemory.CreateOnlyRoads(logger, nodes, roads, 
                new NodeRoadsDictionary<long,long>(nodes, roads),
                new List<(long, TouristAttraction)>(),
                new List<(long, CityInfo)>(),
                gridCellSize,Navigator.GetDebug());
            world_map.SetDangerous(dangerous);
            return world_map;
        }

        private IDisposable computePlaces( string mapTitleSource, Stream stream,
            out RouteManager<TNodeId,TRoadId> manager, 
            out IReadOnlyList<Placement<TNodeId,TRoadId>> placements, params GeoPoint[] userPoints)
        {
            if (userPoints.Length < 2)
                throw new ArgumentOutOfRangeException();

            var result = prepareManager(mapTitleSource, stream, out manager);

            var user_configuration = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();
            
            RequestPoint<TNodeId>[] req_points = userPoints.Select(it => new RequestPoint<TNodeId>(it,
                false,false)).ToArray();
            for (int i = 1; i < req_points.Length - 1; ++i)
            {
                req_points[i] = req_points[i] with {AllowSmoothing = true};
            }

            List<LegRun<TNodeId,TRoadId>>? plan;
            if (!manager.TryFindFlattenRoute(user_configuration, req_points, 
                    CancellationToken.None, out plan,out var problem))
                throw new Exception("Route not found");
            if (problem != null)
                throw new Exception(problem);

            placements = plan.SelectMany(leg => leg.Steps.Select(it => it.Place)).ToList();

            return result;
        }

        private void processSchedule( string mapTitleSource, Stream stream, ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            using (prepareManager(mapTitleSource, stream, out var manager))
            {
                var worker = new RealWorker<TNodeId, TRoadId>(NoLogger, manager);
                if (!worker.TryComputeTrack(schedule.BuildPlanRequest<TNodeId>(), out var response))
                    throw new Exception(response?.Route.ProblemMessage);

                schedule.Route = response.Route;
                schedule.RefreshAnchorLabels(0,response.Names);
            }
        }

        private IDisposable prepareManager(string mapTitleSource, Stream stream, out RouteManager<TNodeId, TRoadId> manager)
        {
            var sys_config = new SystemConfiguration()
            {
                CompactPreservesRoads = true,
                MemoryParams = new MemorySettings() { }
            };
            sys_config.MemoryParams.SetMapMode(this.mapMode);
            sys_config.MemoryParams.SetEnableOsmId(true);

            var logger = new NoLogger();
            var result = CompositeDisposable.None;

            IWorldMap<TNodeId, TRoadId> mini_map;
            {
                var mem_map = loadMiniMap(logger, stream, sys_config.MemoryParams.GridCellSize);

                if (this.mapMode != MapMode.TrueDisk)
                    throw new ArgumentException();

                result = CompositeDisposable.Stack(result, createTrueMap(logger, mem_map, 
                    titleSource: mapTitleSource,
                    sys_config.MemoryParams, out var disk_map));
                mini_map = HACK.CastMap<TNodeId, TRoadId>(disk_map);
            }

            result = CompositeDisposable.Stack(result, RouteManager<TNodeId, TRoadId>.Create(logger, Navigator,
                mini_map,
                sys_config, out manager));
            return result;
        }

        private IDisposable createTrueMap(ILogger logger, WorldMapMemory memMap, string titleSource,
            MemorySettings memorySettings,  out TrueGridWorldMap trueMap)
        {
            var source_nodes_trans = new IdTranslationTable(capacity:memMap.GetAllNodes().Count());
            var source_roads_trans = new IdTranslationTable(capacity:memMap.GetAllRoads().Count());
            var true_source = TrueGridWorldMap.Create(logger,  memMap,new MiniElevationMap(memMap.GetAllNodes().Select(it => it.Value)),
                source_nodes_trans, source_roads_trans, Navigator.GetDebug());
            Stream map_stream = new MemoryStream();
            var result = new CompositeDisposable(map_stream);
            using (Stream nodes_stream = new MemoryStream())
            {
                using (Stream roads_stream = new MemoryStream())
                {
                    TrueGridWorldMap.WriteTranslationTable(nodes_stream,source_nodes_trans);
                    nodes_stream.Position = 0;
                    TrueGridWorldMap.WriteTranslationTable(roads_stream,source_roads_trans);
                    roads_stream.Position = 0;
                    
                    true_source.WriteMap( map_stream);
                    map_stream.Position = 0;

                    var nodes_table = TrueGridWorldMap.ReadIdTranslationTable(nodes_stream);
                    var roads_table = TrueGridWorldMap.ReadIdTranslationTable(roads_stream);
                    
                    result = result.Stack(TrueGridWorldMap.ReadMap(logger, 
                        new[] {(stream: map_stream, fileName: titleSource)}.ToArray(),
                        memorySettings,
                            nodes_table,roads_table,
                        Navigator.GetDebug(),
                        out trueMap, out var invalid_files));
                    if (invalid_files.Any())
                        throw new NotSupportedException();
                    return result;
                }
            }
        }

        protected IReadOnlyList<MapPoint<long>> ComputeRoute(string filename, 
            params GeoPoint[] userPoints)
        {
            using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(
                       System.IO.Path.Combine(Navigator.GetMiniMaps(), filename))))
            using (computePlaces( filename, stream, out var manager, out var placements, userPoints))
            {
                return convertToOsm(manager.Map, placements);
            }
        }

        protected void ProcessSchedule(string filename, ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(
                       System.IO.Path.Combine(Navigator.GetMiniMaps(), filename))))
            {
                processSchedule(filename, stream, schedule);
            }
        }

        private static List<MapPoint<long>> convertToOsm(IWorldMap<TNodeId, TRoadId> map, 
            IEnumerable<Placement<TNodeId, TRoadId>> places)
        {
            return places.Select(p => new MapPoint<long>(p.Point, p.IsNode ? map.GetOsmNodeId(p.NodeId) : null)).ToList();
        }

        protected (IReadOnlyList<MapPoint<long>> plan, IReadOnlyList<TurnInfo<long, long>> turns) ComputeTurns(
            string filename, params GeoPoint[] userPoints)
        {
            using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(
                       System.IO.Path.Combine(Navigator.GetMiniMaps(), filename))))
                return ComputeTurns(filename, stream, userPoints);
        }

        protected (IReadOnlyList<MapPoint<long>> plan, IReadOnlyList<TurnInfo<long, long>> turns) ComputeTurns(
            string title, Stream stream, params GeoPoint[] userPoints)
        {
            var logger = new NoLogger();

            using (computePlaces(title, stream, out var manager, out var plan_nodes, userPoints))
            {
                var turner_preferences = new UserTurnerPreferences();

                var turner = new NodeTurnWorker<TNodeId, TRoadId>(logger, manager.Map,
                    new SystemTurnerConfig() {DebugDirectory = manager.DebugDirectory!},
                    turner_preferences);

                List<TurnInfo<long, long>> regular = computeTurnPoints(title, turner, plan_nodes);

                {
                    // checking reversal as well
                    IReadOnlyList<TurnInfo<long, long>> reversed = computeTurnPoints($"REV-{title}",
                            turner, plan_nodes.Reverse().ToList())
                        .AsEnumerable()
                        .Reverse()
                        // reversing internal data
                        // quality ignores  track index and we couldn't simply mirror it because internally some of the points can be initially removed
                        .Select(it => new TurnInfo<long, long>(it.Entity, it.AtNode() ? it.NodeId : null,
                            it.AtRoundabout() ? it.RoadId : null, it.Point, trackIndex: -1,
                            it.RoundaboutCounter, it.Backward, it.Forward, reason: it.Reason))
                        .ToList();

                    Assert.Equal(expected: regular.Count, actual: reversed.Count);
                    Assert.Equal(expected: regular.Count(it => it.RoundaboutCounter.HasValue), 
                        actual: reversed.Count(it => it.RoundaboutCounter.HasValue));
                    foreach (var (reg, rev) in regular.Zip(reversed, (reg, rev) => (reg, rev)))
                    {
                        Assert.Equal(reg.Point.Latitude.Degrees, rev.Point.Latitude.Degrees, Precision);
                        Assert.Equal(reg.Point.Longitude.Degrees, rev.Point.Longitude.Degrees, Precision);
                    //    Assert.Equal(reg.RoundaboutCounter, rev.RoundaboutCounter);
                        Assert.Equal(reg.Forward, rev.Forward);
                        Assert.Equal(reg.Backward, rev.Backward);
                    }

                    return (convertToOsm(turner.Map, plan_nodes), regular);
                }
            }
        }

        private static List<TurnInfo<long,long>> computeTurnPoints(string caseMapFilename,
            NodeTurnWorker<TNodeId,TRoadId> turner, 
            IReadOnlyList<Placement<TNodeId,TRoadId>> plan)
        {
            string? problem = null;
            var turns = turner.ComputeTurnPoints(plan, ref problem);
            if (problem != null)
            {
                SaveData(convertToOsm(turner.Map, plan),caseMapFilename);
                throw new Exception(problem);
            }

            return turner.Map.TurnsToOsm( turns).ToList();
        }
    }
}