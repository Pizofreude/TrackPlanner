using System.Linq;
using Geo;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using Xunit;

namespace TrackPlanner.Tests
{
    public class MiniWorldPlanTest : MiniWorld
    {
        [Fact]
        public void ZakrzewkoNoGoingBackTest()
        {
            // in first version program got to middle point, went back, then went again forward 

            var map_filename = "zakrzewko.kml";

            var plan = ComputeRoute(map_filename,
                GeoPoint.FromDegrees(53.097324, 18.640022),
                GeoPoint.FromDegrees(53.102116, 18.646202),
                GeoPoint.FromDegrees(53.110565, 18.661394)
            );

            Assert.Equal(plan.Count-1,// there is one joint point (duplicated) here 
                plan.Select(it => it.Point).Distinct().Count());
        }
        
    }
}