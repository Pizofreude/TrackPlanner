using TrackPlanner.Shared;

namespace TrackPlanner.Tests.Implementation
{
    internal sealed class NoLogger : ILogger
    {
        public void Log(LogLevel level,string message)
        {
        }

    }
}