window.markerCluster = {
    createGroup: function () {
        var js_group = L.markerClusterGroup();
        return js_group;
    },
    addLayer: function (group,layer) {
        group.addLayer(layer);
    },
    removeLayer: function (group,layer) {
        group.removeLayer(layer);
    },
};