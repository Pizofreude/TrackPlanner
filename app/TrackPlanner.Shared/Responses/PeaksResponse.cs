﻿using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Responses
{
    public sealed class PeaksResponse<TNodeId>
        where TNodeId : struct
    {
        public List<MapPoint<TNodeId>> Peaks { get; set; } = default!;

        public PeaksResponse()
        {
        }
    }
}
