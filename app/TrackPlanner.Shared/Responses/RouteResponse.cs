﻿using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Responses
{
    public sealed class RouteResponse<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public RoutePlan<TNodeId, TRoadId> Route { get; set; } = default!;
        public List<string?> Names { get; set; } = default!;
    }
}
