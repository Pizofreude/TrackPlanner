﻿using MathUnit;
using System;
using System.Collections.Generic;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared
{
    public static class UserRouterPreferencesHelper
    {
        public static UserRouterPreferences CreateBikeOriented()
        {
            return new UserRouterPreferences()
            {
                AddedMotorDangerousTrafficFactor = 0.80,
                AddedBikeFootHighTrafficFactor = 0.75,
                AddedMotorUncomfortableTrafficFactor = 0.2,
                TrafficSuppression = Length.FromKilometers(3),

                // this is actual help to avoid high-traffic road crossing, but works also well to avoid "shortcuts" while planning
                // without it program woud prefer to make "shortcut" (avoiding high-traffic) on short service road just to return to the main road after 30 meters or so
                JoiningHighTraffic = TimeSpan.FromMinutes(3),
            };
        }

        public static UserRouterPreferences SetCustomSpeeds(this UserRouterPreferences prefs)
        {
            prefs.Speeds = new Dictionary<SpeedMode, SpeedInfo>()
            {
                [SpeedMode.Asphalt] = new SpeedInfo( MathUnit.Speed.FromKilometersPerHour(16.5),MathUnit.Speed.FromKilometersPerHour(35)),
                [SpeedMode.HardBlocks] = new SpeedInfo(MathUnit.Speed.FromKilometersPerHour(7.5),MathUnit.Speed.FromKilometersPerHour(12)),

                [SpeedMode.Ground] = new SpeedInfo(MathUnit.Speed.FromKilometersPerHour(13),MathUnit.Speed.FromKilometersPerHour(20)),
                [SpeedMode.Sand] = new SpeedInfo(MathUnit.Speed.FromKilometersPerHour(6),MathUnit.Speed.FromKilometersPerHour(8)),

                [SpeedMode.CableFerry] = SpeedInfo.Constant(MathUnit.Speed.FromKilometersPerHour(4.5)),

                [SpeedMode.CarryBike] = SpeedInfo.Constant(Speed.FromKilometersPerHour(1.6)),
                [SpeedMode.Walk] = SpeedInfo.Constant(MathUnit.Speed.FromKilometersPerHour(5.5)),
            };

            return prefs.Complete();
        }

        public static UserRouterPreferences SetUniformSpeeds(this UserRouterPreferences prefs)
        {
            foreach (var mode in Enum.GetValues<SpeedMode>())
            {
                prefs.Speeds[mode] = SpeedInfo.Constant( MathUnit.Speed.FromKilometersPerHour(13));
            }

            return prefs.Complete();
        }

    }

}