using System;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.Shared.Stored
{
    public sealed class ScheduleSettings
    {
        public static string SectionName => "ScheduleSettings";


        public UserPlannerPreferences PlannerPreferences { get; set; } = default!;
        public UserTurnerPreferences TurnerPreferences { get; set; } = default!;
        public UserRouterPreferences RouterPreferences { get; set; } = default!;
        private bool loopRoute;
        public bool LoopRoute
        {
            get { return this.loopRoute; }
            set
            {
                if (value)
                    endsAtHome = this.StartsAtHome;
                this.loopRoute = value;
            }
        }

        private bool startsAtHome;
        public bool StartsAtHome
        {
            get { return this.startsAtHome;}
            set
            {
                if (this.LoopRoute)
                    this.endsAtHome = value;
                this.startsAtHome = value;
            }
        }
        private bool endsAtHome;
        public bool EndsAtHome
        {
            get { return this.endsAtHome;}
            set {                if (this.LoopRoute)
                    this.startsAtHome = value;
                this.endsAtHome = value;
            }
        }

       

        public ScheduleSettings()
        {
            // do NOT put any defaults here, ConfigurationBinder is buggy
        }
        public static ScheduleSettings Defaults()
        {
            return new ScheduleSettings()
            {
                StartsAtHome = true,
                EndsAtHome = true,
                LoopRoute = true,
                RouterPreferences = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds(),
                PlannerPreferences = new UserPlannerPreferences(),
                TurnerPreferences = new UserTurnerPreferences(),
            };
        }

        public void Check()
        {
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
    }
}
