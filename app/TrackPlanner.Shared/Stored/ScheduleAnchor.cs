using System;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Stored
{
    public sealed class ScheduleAnchor : IAnchor
    {
        public GeoPoint UserPoint { get; set; }
        public TimeSpan UserBreak { get; set; }
        public string Label { get; set; }
        public bool IsUserLabel { get; set; }
        public bool IsPinned { get; set; }

        public ScheduleAnchor()
        {
            Label = "";
        }
    }
}