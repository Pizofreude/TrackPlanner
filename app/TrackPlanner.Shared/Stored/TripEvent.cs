﻿using System;

namespace TrackPlanner.Shared.Stored
{
    public sealed class TripEvent
    {
        public string Identifier { get; set; } = "";// should be unique
        private string? group;
        public string Group  // variants; can be duplicated among events (resupply = two events: morning + evening ressuplies)
        {
            get { return this.group ?? Identifier; }
            set { this.group = value; }
        }
        private string? category;
        public string Category // for sure it will be duplicated ("shopping" covers few events)
        {
            get { return this.category ?? Group; }
            set { this.category = value; }
        }
        public string ClassIcon { get; set; } = "";
        public int EveryDay { get; set; } = 1;
        public TimeSpan Duration { get; set; }
        public bool SkipBeforeHome { get; set; }
        public bool SkipAfterHome { get; set; }
        // only one can be set, if both are not set, the system assume the event is one time only (per day)
        // within the same category, interval-based events have lower priority
        public TimeSpan? Interval { get; set; }
        // double meaning of zero and null -- both mean start as soon as possible, but null should not start the day
        public TimeSpan? ClockTime { get; set; }
        public bool Enabled { get; set; }

        public TripEvent()
        {
            this.Enabled = true;
        }
    }
}


