using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Stored
{
    public sealed class ScheduleJourney<TNodeId,TRoadId> :ISchedule<ScheduleDay,ScheduleAnchor,TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId: struct
    {
        public string Comment { get; set; } = "";
        public RoutePlan<TNodeId,TRoadId> Route { get; set; } = default!;
        IReadOnlyList<IReadOnlyDay> IReadOnlySchedule.Days => this.Days;
        public List<ScheduleDay> Days { get; set; } = default!;
        public ScheduleSettings Settings { get; set; } = default!;
        // todo: remove this outside -- to the controller level
        public UserVisualPreferences VisualPreferences { get; set; } = default!;
        
        public ScheduleJourney()
        {
            this.Days = new List<ScheduleDay>();
        }
        
    }
}