﻿namespace TrackPlanner.Shared
{
    public interface ILogger
    {
        void Log(LogLevel level, string message);
    }
}