using System;
using System.Globalization;
using System.Linq;
using Geo;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public static class DataFormat
    {
        public static GeoPoint? TryParseUserPoint(string? input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            var parts = input.Split(",").Select(it => it.Trim()).ToList();
            if (parts.Count != 2)
            {
        //        this.logger.LogError("Expected 'lat,long' input.");
                return null;
            }

            if (!float.TryParse(parts[0], out var lat))
            {
             //   this.logger.LogError($"Unable to parse latitude: {parts[0]}");
                return null;
            }

            if (!float.TryParse(parts[1], out var lon))
            {
//                this.logger.LogError($"Unable to parse longitude: {parts[1]}");
                return null;
            }

            return GeoPoint.FromDegrees(lat, lon);
        }

        public static string? FormatUserInput(GeoPoint? position)
        {
            string? input = null;
            if (position != null)
                input = $"{position.Value.Latitude.Degrees}, {position.Value.Longitude.Degrees}";
            return input;
        }
        public static string FormatEvent(int count, TimeSpan duration)
        {
            return $"{Format(duration)}{(count==1?"":$" ({count})")}";
        }
        public static string Format(TimeSpan time)
        {
            return time.ToString(@"hh\:mm");
        }
        public static string Format(DateTimeOffset dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
        }
        public static string Format(Angle angle)
        {
            return angle.Degrees.ToString("0")+"°";
        }
        public static string FormatDistance(Length distance,bool withUnit)
        {
            return distance.Kilometers.ToString("0.#")+(withUnit?"km":"");
        }
        public static string FormatHeight(Length height,bool withUnit)
        {
            return height.Meters.ToString("0")+(withUnit?"m":"");
        }
        public static string DecoFormatHeight(Length height)
        {
            // little triangle (^) character in front
            return new ( $"\u25B2 {FormatHeight(height, true)}");
        }
        public static string Format(Speed speed,bool withUnit)
        {
            return speed.KilometersPerHour.ToString("0.#")+(withUnit?"km/h":"");
        }

        public static string Adjust(int number, int range)
        {
            if (number > range)
                throw new ArgumentOutOfRangeException($"{nameof(number)}={number}, {nameof(range)}={range}.");
            int total_width =1+(range==0? 0: (int)Math.Floor( Math.Log(range, 10)));
            return number.ToString().PadLeft(total_width, '0');
        }
    }

}