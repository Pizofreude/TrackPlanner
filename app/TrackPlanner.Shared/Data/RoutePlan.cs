﻿using System;
using System.Collections.Generic;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public static class RoutePlan
    {
        internal static Length ValidationLengthErrorLimit => Length.FromCentimeters(1);
    }

    public sealed class RoutePlan<TNodeId,TRoadId>
        where TNodeId:struct
    where TRoadId: struct
    {
        public List<LegPlan<TNodeId,TRoadId>> Legs { get; set; }
        public List<List<TurnInfo<TNodeId,TRoadId>>> DailyTurns { get; set; } // turns within days
        public string? ProblemMessage { get; set; }

        public RoutePlan()
        {
            this.Legs = new List<LegPlan<TNodeId,TRoadId>>();
            this.DailyTurns = new List<List<TurnInfo<TNodeId,TRoadId>>>();
        }

        public bool IsEmpty() => this.Legs.Count == 0;

        public string? DEBUG_Validate()
        {
            if (ProblemMessage != null)
                return ProblemMessage;
            
            for (int i=0;i<this.Legs.Count;++i)
            {
              var failure =  Legs[i].DEBUG_Validate(legIndex:i);
              if (failure != null)
                  return failure;
              
              if (i > 0 && this.Legs[i - 1].LastStep().Point != this.Legs[i].FirstStep().Point)
                  return $"We have gap between leg {i-1} and {i}.";
            }

            return null;
        }

        public LegPlan<TNodeId,TRoadId> GetLeg(int legIndex,string? DEBUG_context = null)
        {
            if (legIndex < 0 || legIndex >= this.Legs.Count)
                throw new ArgumentOutOfRangeException($"{nameof(legIndex)}={legIndex} when having {this.Legs.Count} legs ({DEBUG_context}).");

            return this.Legs[legIndex];
        }

    }
}
