﻿using Geo;
using MathUnit;


namespace TrackPlanner.Shared.Data
{
    public static class PointExtension
    {
        public static Geo.GeoPoint Convert(this in GeoZPoint pt) => new Geo.GeoPoint(pt.Latitude, pt.Longitude);
        public static GeoZPoint Convert(this in GeoPoint pt) =>  GeoZPoint.Create(pt.Latitude, pt.Longitude, altitude: null);
        public static GeoZPoint Convert(this in GeoPoint pt,Length altitude) =>  GeoZPoint.Create(pt.Latitude, pt.Longitude, altitude: altitude);
    }

}
