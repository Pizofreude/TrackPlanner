using System;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct Slope : IComparable<Slope>
    {
        public static Slope FromPercents(float percents)
        {
            return new Slope(percents);
        }
        public static Slope FromData(Length distance,Length height)
        {
            return FromPercents((float)(100.0*height.Meters/distance.Meters));
        }
        
        public float Percents { get; }

        private Slope(float percents)
        {
            this.Percents = percents;
        }

        public int CompareTo(Slope other)
        {
            return this.Percents.CompareTo(other.Percents);
        }
        
        public override string ToString()
        {
            return Math.Round( this.Percents).ToString("0")+"%";
        }

        public static bool operator >=(Slope a, Slope b)
        {
            return a.Percents >= b.Percents;
        }

        public static bool operator <=(Slope a, Slope b)
        {
            return a.Percents <= b.Percents;
        }
    }
}