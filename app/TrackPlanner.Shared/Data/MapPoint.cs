﻿namespace TrackPlanner.Shared.Data
{
    public interface IMapPoint
    {
        GeoZPoint Point { get; }
    }

    public readonly record struct MapPoint<TNodeId>(GeoZPoint Point, TNodeId? NodeId) : IMapPoint
        where TNodeId : struct
    {
        public bool IsNode() => NodeId.HasValue;

        public override string ToString()
        {
            if (this.IsNode())
                return $"#{this.NodeId}";
            else
                return $"{this.Point}";
        }
    }
}