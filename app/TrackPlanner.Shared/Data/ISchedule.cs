using System.Collections.Generic;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public interface IReadOnlySchedule
    {
        ScheduleSettings Settings { get; }
        IReadOnlyList<IReadOnlyDay> Days { get; }
    }
    public interface IReadOnlySchedule<TNodeId,TRoadId> : IReadOnlySchedule
        where TNodeId:struct
        where TRoadId: struct
    {
        RoutePlan<TNodeId,TRoadId> Route { get; }
    }

    public interface ISchedule<TDay,TAnchor,TNodeId,TRoadId> : IReadOnlySchedule<TNodeId,TRoadId>
    where TDay:IDay<TAnchor>
    where TAnchor:IAnchor
    where TNodeId:struct
    where TRoadId: struct
    {
        new List<TDay> Days { get; }
    }
}