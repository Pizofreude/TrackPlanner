﻿namespace TrackPlanner.Shared.Data
{
    // class, not struct, because we will keep references to it in visual elements
    public sealed  class PlacedAttraction
    {
        public MapPoint<long> Place { get; }
        public TouristAttraction Attraction { get; }

        public PlacedAttraction(MapPoint<long> place, TouristAttraction attraction)
        {
            Place = place;
            Attraction = attraction;
        }
        
        public void Deconstruct(out MapPoint<long> place, out TouristAttraction attraction)
        {
            place = Place;
            attraction = Attraction;
        }
    }
    
    
}
