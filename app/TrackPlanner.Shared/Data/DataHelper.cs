﻿using System;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Data
{
    public static class DataHelper
    {
        public static void SnapToFragment<TNodeId,TRoadId>(IGeoCalculator calc,
            UserRouterPreferences routePrefs, GeoPoint point, LegFragment<TNodeId,TRoadId> fragment,
            out Length alongFragmentDistance,out TimeSpan alongFragmentTime)
            where TNodeId:struct
        {
            var z_point = point.Convert();

            alongFragmentDistance = Length.Zero;
            alongFragmentTime = TimeSpan.Zero;

            var min_distance = Length.MaxValue;
            GeoZPoint DEBUG_segment_start = default;
            Length DEBUG_along_segment = default;
            var running_distance = Length.Zero;
            var running_time = TimeSpan.Zero;
            foreach (var (prev, next) in fragment.Steps.Slide())
            {
                (Length snap_dist, GeoZPoint cross_point, Length along) = calc.GetDistanceToArcSegment(z_point, prev.Point, next.Point);
                if (min_distance > snap_dist)
                {
                    min_distance = snap_dist;
                    alongFragmentDistance = along + running_distance;
                    alongFragmentTime = routePrefs.GetRideTime(along, fragment.Mode, prev.Point.Altitude, 
                        cross_point.Altitude,out _)+running_time;
                    DEBUG_along_segment = along;
                    DEBUG_segment_start = prev.Point;
                }

                running_distance += next.IncomingFlatDistance;
                running_time += routePrefs.GetRideTime(next.IncomingFlatDistance, fragment.Mode, prev.Point.Altitude,
                    next.Point.Altitude,out _);
            }
            // Console.WriteLine($"SnapToFragment segment start {DEBUG_segment_start}, from fragment {fragment.Places.First().Point} to {fragment.Places.Last().Point}; {TrackPlanner.Shared.Data.DataFormat.Format(DEBUG_along_segment,false)} / {TrackPlanner.Shared.Data.DataFormat.Format(alongFragmentDistance,false)} / {TrackPlanner.Shared.Data.DataFormat.Format(running_distance,false)} / {DataFormat.Format(fragment.UnsimplifiedDistance,false)}");
        }

        public static TimeSpan GetRideTime(this UserRouterPreferences prefs, Length distance,
            SpeedMode mode, Length altitudeFrom, Length targetAltitude, out Speed speed)
        {
            Length true_distance = ApproximateCalculator.GetTrueDistance(distance, altitudeFrom, targetAltitude);

            speed = prefs.getSpeed(distance,
                mode, altitudeFrom, targetAltitude);
            return true_distance / speed;
        }

        private static Speed getSpeed(this UserRouterPreferences prefs, Length distance,
            SpeedMode mode, Length altitudeFrom, Length targetAltitude)
        {
            var info = prefs.Speeds[mode];
            double slope_factor = Slope.FromData(distance, targetAltitude - altitudeFrom).Percents;
            var speed = info.Regular - Speed.FromKilometersPerHour(slope_factor);
            speed = speed.Min(info.Max).Max(prefs.GetLowRidingSpeedLimit());
            return speed;
        }
        
        public static TimeSpan CalcTrueTime(TimeSpan rollingTime, Length distance, TimeSpan rawTime,
            Speed lowSpeedLimit, double hourlyStamina)
        {
            if (distance == Length.Zero)
                return TimeSpan.Zero;

            var raw_speed = distance / rawTime;

            var start_speed = raw_speed * Math.Pow(hourlyStamina, rollingTime.TotalHours);
            // todo: from here math is bad, fix it
            var final_speed = raw_speed * Math.Pow(hourlyStamina, rollingTime.TotalHours + rawTime.TotalHours);

            final_speed = final_speed.Max(lowSpeedLimit); // don't allow absurdly low values

            var avg_speed = (start_speed + final_speed) / 2;

            TimeSpan true_time;
            try
            {
                true_time = distance / avg_speed;
            }
            catch
            {
                Console.WriteLine($"True time failed, distance {distance.Meters}, avg_speed {avg_speed.MetersPerSecond}");
                throw;
            }

            return true_time;
        }
    }
}