using System;
using System.Collections.Generic;
using System.Linq;
using Force.DeepCloner;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Data
{
    public static class ScheduleExtension
    {
        public static void RefreshAnchorLabels<TDay,TAnchor,TNodeId,TRoadId>(this ISchedule<TDay,TAnchor,TNodeId,TRoadId> schedule, int legIndex, IReadOnlyList<string?> names)
            where TDay:IDay<TAnchor>
            where TAnchor:IAnchor
            where TNodeId:struct
            where TRoadId: struct
        {
            (int day_idx, int anchor_idx, _) = schedule.LegIndexToStartingDayAnchor(legIndex);

            int name_idx = 0;
            for (; day_idx < schedule.Days.Count; ++day_idx)
            {
                for (; anchor_idx < schedule.Days[day_idx].Anchors.Count; ++anchor_idx)
                {
                    if (name_idx == names.Count)
                        return;

                    var name = names[name_idx++];
                    var anchor = schedule.Days[day_idx].Anchors[anchor_idx];
                    if (name != null && !anchor.IsUserLabel)
                    {
                        anchor.Label = name;
                        anchor.IsUserLabel = false;
                    }
                }

                anchor_idx = 0;
            }
        }

        public static ScheduleJourney<TNodeId,TRoadId> CreateJourneySchedule<TNodeId,TRoadId>(this IReadOnlySchedule<TNodeId,TRoadId> source, 
            UserVisualPreferences visualPreferences, bool onlyPinned)
            where TNodeId:struct
            where TRoadId: struct
        {
            // we need deep cloning everything for undo/redo
            var schedule = new ScheduleJourney<TNodeId,TRoadId>
            {
                Days = source.GetScheduleDays(onlyPinned),
                Route = source.Route.DeepClone(),
                Settings = source.Settings.DeepClone(),
                VisualPreferences = visualPreferences.DeepClone(),
            };

            return schedule;
        }


        public static bool FindAttrIndex<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule, PlacedAttraction attraction, out int legIndex, 
            out int attrIndex)
            where TNodeId:struct
            where TRoadId: struct
        {
            for (legIndex = 0; legIndex < schedule.Route.Legs.Count; ++legIndex)
            {
                attrIndex = schedule.Route.Legs[legIndex].Attractions.IndexOf(attraction);
                if (attrIndex != -1)
                {
                    return true;
                }
            }

            attrIndex = default;
            return false;
        }
        public static List<ScheduleDay> GetScheduleDays (this IReadOnlySchedule schedule, bool onlyPinned)
        {
            return schedule.Days.Select(d => new ScheduleDay()
            {
                Start = d.Start,
                Anchors = d.Anchors
                    .Where(it => !onlyPinned || it.IsPinned)
                    .Select(it => new ScheduleAnchor()
                    {
                        Label = it.Label,
                        UserBreak = it.UserBreak,
                        IsUserLabel = it.IsUserLabel,
                        IsPinned = it.IsPinned,
                        UserPoint = it.UserPoint,
                    }).ToList()
            }).ToList();
        }
        public static bool HasEnoughPointsForBuild(this IReadOnlySchedule schedule)
        {
            return schedule.Days.SelectMany(it => it.Anchors).Where(it => it.IsPinned).HasMany();
        }


        private static bool tryIndexOf<TNodeId,TRoadId>(RoutePlan<TNodeId,TRoadId> route, LegPlan<TNodeId,TRoadId> leg, 
            LegFragment<TNodeId,TRoadId> fragment, out int legIndex,
            out int fragmentIndex)
        where TNodeId:struct
        where TRoadId:struct
        {
            legIndex = route.Legs.IndexOf(leg);
            fragmentIndex = leg.Fragments.IndexOf(fragment);
            if (legIndex == -1 || fragmentIndex == -1)
            {
//                logger.LogDebug($"Leg={legIndex}/fragment={fragmentIndex} not found, hitting {(leg == LegPlan<long, long>.Missing ? "invalidated" : (leg.IsDrafted ? "drafted" : "regular"))} leg.");
                return false;
            }
            else
                return true;
        }

        public static void ComputeProgress<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule, 
            IGeoCalculator calc,
            GeoPoint point, LegPlan<TNodeId, TRoadId> activeLeg,
            LegFragment<TNodeId, TRoadId> activeFragment,
            out Length runningDistance,
            out TimeSpan dayTime,
            out Length remainingDistance,
            out TimeSpan remainingTime)
            where TNodeId:struct
            where TRoadId: struct
        {
            if (!tryIndexOf(schedule.Route, activeLeg, activeFragment, out int leg_idx, out int fragment_idx))
                throw new ArgumentException();

            DataHelper.SnapToFragment(calc, schedule.Settings.RouterPreferences,
                point, activeFragment,
                out Length along_dist, out TimeSpan along_time);

            int day_starting_leg = schedule.GetDayByLeg(schedule.GetSummary(), leg_idx,
                out SummaryDay day, out TimeSpan covered_day_breaks);
            runningDistance = Length.Zero;
            var running_time = TimeSpan.Zero;

            // sum up legs from start of the day
            foreach (var leg in schedule.Route.Legs.Skip(day_starting_leg).Take(leg_idx - day_starting_leg))
            {
                runningDistance += leg.UnsimplifiedDistance;
                var true_time = DataHelper.CalcTrueTime(running_time, leg.UnsimplifiedDistance, leg.RawTime,
                    schedule.Settings.RouterPreferences.GetLowRidingSpeedLimit(),
                    schedule.Settings.PlannerPreferences.HourlyStamina);
                running_time += true_time;
            }

            {
                Length partial_leg_distance = Length.Zero;
                TimeSpan partial_leg_time = TimeSpan.Zero;

// and then add segments from active leg up to the active segment
                foreach (var fragment in activeLeg.Fragments.Take(fragment_idx))
                {
                    partial_leg_distance += fragment.UnsimplifiedFlatDistance;
                    partial_leg_time += fragment.RawTime;
                }

                //logger.LogDebug($"LineOnOnMouseOverAsync: day_starting_leg {day_starting_leg}, partial leg {DataFormat.FormatDistance(partial_leg_distance, true)}/{DataFormat.FormatDistance(active_leg.UnsimplifiedDistance, false)}, along {TrackPlanner.Shared.Data.DataFormat.FormatDistance(along_dist, false)}/{DataFormat.FormatDistance(active_fragment.UnsimplifiedFlatDistance, false)}, prev anchor at {DataFormat.Format(day.Start + running_time + covered_day_breaks)}, point {e.LatLng.ToPointF()}");

                // and finally the last portion along the segment
                partial_leg_distance += along_dist;
                partial_leg_time += along_time;

                var true_time = DataHelper.CalcTrueTime(running_time, partial_leg_distance, partial_leg_time,
                    schedule.Settings.RouterPreferences.GetLowRidingSpeedLimit(),
                    schedule.Settings.PlannerPreferences.HourlyStamina);
                running_time += true_time;
            }

            running_time += covered_day_breaks;

            remainingDistance = day.Distance - runningDistance;
            remainingTime = day.TrueDuration - running_time;

            dayTime = day.Start + running_time;
        }

        public static bool IsAnchorImportant(this IReadOnlySchedule schedule, int dayIndex, int anchorIndex)
        {
            bool is_important = ((dayIndex == 0 && anchorIndex == 0) // starting point 
                                 ||
                                 // or the last of the day except for the last day (unless it is not looped route)
                                 (anchorIndex == schedule.Days[dayIndex].Anchors.Count - 1 && (!schedule.IsLoopActivated() || dayIndex < schedule.Days.Count - 1)));

            return is_important;
        }

    
        public static PlanRequest<TNodeId> BuildPlanRequest<TNodeId>(this IReadOnlySchedule schedule)
        where TNodeId:struct
        {
            var daily_points = new List<List<RequestPoint<TNodeId>>>();
            for (int day_idx = 0; day_idx < schedule.Days.Count; ++day_idx)
            {
                var points = new List<RequestPoint<TNodeId>>();
                for (int anchor_idx = 0; anchor_idx < schedule.EffectiveAnchorCount(day_idx); ++anchor_idx)
                {
                    schedule.GetAnchorDetails(day_idx, anchor_idx, out _, out var is_last_of_day);
                    bool allow_smoothing = !is_last_of_day && !(day_idx == 0 && anchor_idx == 0);
                    var anchor = schedule.GetEffectiveAnchor(day_idx, anchor_idx);
                    points.Add(new RequestPoint<TNodeId>(anchor.UserPoint, allow_smoothing, 
                        findLabel: !anchor.IsUserLabel));
                }

                daily_points.Add(points);
            }

            var plan_request = new PlanRequest<TNodeId>()
            {
                DailyPoints = daily_points,
                TurnerPreferences = schedule.Settings.TurnerPreferences,
                RouterPreferences = schedule.Settings.RouterPreferences,
            };

            return plan_request;
        }

        public static bool IsLoopActivated(this IReadOnlySchedule _this)
        {
            return _this.Settings.LoopRoute && _this.Days.SelectMany(it => it.Anchors).HasMany();
        }

        public static bool IsLoopedDay(this IReadOnlySchedule readOnlySchedule, int dayIndex)
        {
            return readOnlySchedule.IsLoopActivated() && dayIndex == readOnlySchedule.Days.Count - 1;
        }

        public static int GetLegCount(this IReadOnlySchedule readOnlySchedule, int dayIndex)
        {
            return GetLegCount(dayIndex, readOnlySchedule.Days[dayIndex].Anchors.Count,
                addLoopedAnchor: readOnlySchedule.IsLoopedDay(dayIndex));
        }

        public static int GetLegCount(int dayIndex, int anchorsCount, bool addLoopedAnchor)
        {
            int legs_count = anchorsCount;
            if (dayIndex == 0) // first day contains both start and end (for that day)
                --legs_count;

            // DO NOT add "else" here, we could have single day, looped, in such case count(legs)=count(anchors)
            if (addLoopedAnchor)
                ++legs_count;

            return legs_count;
        }

        public static IEnumerable<LegPlan<TNodeId, TRoadId>> GetDayLegs<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> readOnlySchedule, int dayIndex)
            where TNodeId:struct
            where TRoadId: struct
        {
            int leg_offset = GetFirstLegIndex(readOnlySchedule, dayIndex);

            var leg_count = readOnlySchedule.GetLegCount(dayIndex);

            var legs = readOnlySchedule.Route.Legs.Skip(leg_offset).Take(leg_count);

            return legs;
        }


        public static int GetFirstLegIndex(this IReadOnlySchedule schedule, int dayIndex)
        {
            return schedule.GetIncomingLegIndexByAnchor(dayIndex, dayIndex == 0 ? 1 : 0)!.Value;
        }

        public static int? GetIncomingLegIndexByAnchor(this IReadOnlySchedule schedule, int dayIndex, int anchorIndex)
        {
            return schedule.GetIncomingLegIndexByCheckpoint(dayIndex, schedule.AnchorIndexToCheckpoint(dayIndex, anchorIndex));
            /*if (dayIndex == 0 && anchorIndex == 0)
                return null;

            int count = 0;
            for (int d = 0; d < dayIndex; ++d)
                count += GetLegCount(schedule, d);
            // first day includes starting anchor, for every next day the starting anchor is the last anchor from the previous day
            return count + anchorIndex - (dayIndex == 0 ? 1 : 0);*/
        }
        public static int? GetIncomingLegIndexByCheckpoint(this IReadOnlySchedule schedule, int dayIndex, int checkpointIndex)
        {
            if (dayIndex == 0 && checkpointIndex == 0)
                return null;

            int count = 0;
            for (int d = 0; d < dayIndex; ++d)
                count += GetLegCount(schedule, d);
            return count + checkpointIndex - 1;
        }

        public static string DEBUG_PinsToString<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule)
            where TNodeId:struct
            where TRoadId: struct
        {
            return $"DAYS {schedule.Days.Count} " + String.Join(Environment.NewLine, schedule.Days.ZipIndex().Select(it => $"d{it.index}: {(String.Join(", ", it.item.Anchors.Select(a => a.IsPinned ? "P" : "a")))}"))
                                                          + Environment.NewLine
                                                          + $"LEGS {schedule.Route.Legs.Count} " + String.Join(" ",
                                                              schedule.Route.Legs.Select(it => (it.AutoAnchored ? "A" : "P") + (it.IsDrafted ? "d" : "c")));
            /*+"LEGS "+String.Join(Environment.NewLine,
                schedule.TrackPlan.Legs.Select(it => it.AutoAnchored?"a":"P")
                    .Partition(Enumerable.Range(0,schedule.Days.Count).Select(schedule.GetLegCount))
                        .ZipIndex()
                        .Select(it => $"d{it.index}: {(String.Join(" ",  it.item))}"));*/
        }


        public static (int dayIndex, int anchorIndex, int legStartingDayIndex) LegIndexToStartingDayAnchor<TNodeId,TRoadId>(
            this IReadOnlySchedule<TNodeId,TRoadId> schedule, int legIndex)
            where TNodeId:struct
            where TRoadId: struct
        {
            int leg_idx_copy = legIndex;
            for (int day_idx = 0; day_idx < schedule.Days.Count; ++day_idx)
            {
                int leg_count = schedule.GetLegCount(day_idx);
                if (leg_idx_copy >= leg_count)
                {
                    leg_idx_copy -= leg_count;
                    continue;
                }

                var starting_day_idx = legIndex - leg_idx_copy;

                if (day_idx == 0)
                {
                    return (day_idx, leg_idx_copy, starting_day_idx);
                }
                else
                {
                    // for opening legs on next days, the starting anchor is last one from previous day
                    // we indicate it by returning effectively -1 for such case
                    return (day_idx, leg_idx_copy - 1, starting_day_idx);
                }
            }

            throw new InvalidOperationException($"Couldn't compute day for leg {leg_idx_copy}.");
        }

        public static int GetDayByLeg<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule, SummaryJourney summary,
            int legIndex, out SummaryDay day, out TimeSpan coveredDayBreaks)
            where TNodeId:struct
            where TRoadId: struct
        {
            if (legIndex >= schedule.Route.Legs.Count)
                throw new ArgumentOutOfRangeException($"Leg index {legIndex} is greater than entire route {schedule.Route.Legs.Count}.");

            var (day_idx, anchor_idx, leg_starting_day_idx) = schedule.LegIndexToStartingDayAnchor(legIndex);

            day = summary.Days[day_idx];

            var leg_idx_within_day = legIndex - leg_starting_day_idx;
            coveredDayBreaks = day.Checkpoints
                .Take(leg_idx_within_day + 1)
                .Select(it => it.TotalBreak)
                .Sum();
            return leg_starting_day_idx;
        }

        public static int EffectiveAnchorCount(this IReadOnlySchedule schedule, int dayIndex)
        {
            int count = schedule.Days[dayIndex].Anchors.Count;
            if (IsLoopActivated(schedule) && dayIndex == schedule.Days.Count - 1)
                ++count;
            return count;
        }

        public static IReadOnlyAnchor GetEffectiveAnchor(this IReadOnlySchedule schedule, int dayIndex, int superAnchorIndex)
        {
            if (dayIndex == schedule.Days.Count - 1 && superAnchorIndex == schedule.Days[dayIndex].Anchors.Count)
            {
                if (!IsLoopActivated(schedule))
                    throw new ArgumentOutOfRangeException("Loop is not activated");
                else
                    return schedule.Days[0].Anchors[0];
            }
            else
                return schedule.Days[dayIndex].Anchors[superAnchorIndex];
        }

        public static void GetAnchorDetails(this IReadOnlySchedule schedule, int dayIndex, int anchorIndex,
            out bool isLoopedAnchor,
            out bool isLastOfDay)
        {
            var day = schedule.Days[dayIndex];
            isLoopedAnchor = dayIndex == schedule.Days.Count - 1 && anchorIndex == day.Anchors.Count;
            // anchor[0] on every "next" day is not really first anchor, those starting anchor have their own render method
            isLastOfDay = isLoopedAnchor
                          || (anchorIndex == day.Anchors.Count - 1 &&
                              (dayIndex < schedule.Days.Count - 1 || !schedule.IsLoopActivated()));
        }

        public static IEnumerable<GeoPoint> GetDayCheckpoints_OBSOLETE(this IReadOnlySchedule schedule, int dayIndex)
        {
            if (dayIndex > 0)
                yield return schedule.Days[dayIndex - 1].Anchors[^1].UserPoint;
            foreach (var anchor in schedule.Days[dayIndex].Anchors)
                yield return anchor.UserPoint;
            if (dayIndex == schedule.Days.Count - 1 && schedule.Settings.LoopRoute)
                yield return schedule.Days[0].Anchors[0].UserPoint;
        }
    }
}