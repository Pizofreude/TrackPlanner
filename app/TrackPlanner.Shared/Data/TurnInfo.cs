﻿
using System;
using Geo;
using TrackPlanner.Shared;

namespace TrackPlanner.Shared.Data
{
    public static class TurnInfo
    {
        public enum EntityReference
        {
            Node,
            // in case of turn on roundabout the point should be its center (so it will not have underlying OSM node) 
            Roundabout
        }
    }

    public sealed class TurnInfo<TNodeId,TRoadId>
    where TNodeId:struct
    where TRoadId:struct
    {
        
        public static TurnInfo<TNodeId,TRoadId> CreateRegular(TNodeId nodeId, GeoPoint point, int trackIndex, bool forward, bool backward,string? reason)
        {
            return new TurnInfo<TNodeId,TRoadId>(TurnInfo.EntityReference.Node, nodeId,null, point, trackIndex, null, forward, backward,reason);
        }
        public static TurnInfo<TNodeId,TRoadId> CreateRoundabout(TRoadId roadId, GeoPoint point, int trackIndex, int? roundaboutCounter)
        {
            return new TurnInfo<TNodeId,TRoadId>(TurnInfo.EntityReference.Roundabout,null, roadId, point, trackIndex, roundaboutCounter, 
                true, true,reason:"roundabout");
        }

        public GeoPoint Point { get; }
        public int TrackIndex { get; }
        public int? RoundaboutCounter { get; }
        public bool Forward { get; }
        public bool Backward { get; }
        public string? Reason { get; }
        public TurnInfo.EntityReference Entity { get; }
        public TNodeId? NodeId { get; }
        public TRoadId? RoadId { get; }
        public bool AtNode() => this.Entity== TurnInfo.EntityReference.Node;
        public bool AtRoundabout() => this.Entity== TurnInfo.EntityReference.Roundabout;

        public TurnInfo(TurnInfo.EntityReference entity,TNodeId? nodeId, TRoadId? roadId, GeoPoint point,
            int trackIndex, 
            int? roundaboutCounter, bool forward, bool backward,string? reason)
        {
            this.RoadId = roadId;
            Entity = entity;
            this.NodeId = nodeId;
            Point = point;
            TrackIndex = trackIndex;
            RoundaboutCounter = roundaboutCounter;
            Forward = forward;
            Backward = backward;
            Reason = reason;
        }

        public string GetLabel()
        {
            string label = $"{(Entity== TurnInfo.EntityReference.Node?"N":"R")}-{(Entity== TurnInfo.EntityReference.Node?NodeId:RoadId)} ";
            if (this.RoundaboutCounter.HasValue)
                label += $"(G{this.RoundaboutCounter}) ";
            else if (this.Backward != this.Forward)
            {
                label += $"({(this.Forward ? "F" : "B")}) ";
            }

            return label + $"track index {TrackIndex}";
        }

        public override bool Equals(object? obj)
        {
            return obj is TurnInfo<TNodeId,TRoadId> info &&  Equals(info);
        }

        public bool Equals(TurnInfo<TNodeId,TRoadId> obj)
        {
            // track index is not included so we can easily compare track with its reversed counterpart
            return Point.Equals(obj.Point) && RoundaboutCounter == obj.RoundaboutCounter && Forward == obj.Forward 
                   && Backward == obj.Backward;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( Point, RoundaboutCounter,Forward, Backward);
        }
    }
}
