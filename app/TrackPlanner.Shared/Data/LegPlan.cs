﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    // leg are the segments from anchor to anchor
    public sealed class LegPlan<TNodeId,TRoadId>
        where TNodeId:struct
    {
        public static LegPlan<TNodeId,TRoadId> Missing { get; } = new() {IsDrafted = true};

        public List<LegFragment<TNodeId,TRoadId>> Fragments { get; set; }

        public Length UnsimplifiedDistance { get; set; }
        public TimeSpan RawTime { get; set; }
        public bool IsDrafted { get; set; }
        public bool AutoAnchored { get; set; }
        public List<PlacedAttraction> Attractions { get; set; }

        public IEnumerable<FragmentStep<TNodeId>> AllSteps() => Fragments.SelectMany(it => it.Steps);
        public FragmentStep<TNodeId> FirstStep() => Fragments[0].Steps[0];
        public FragmentStep<TNodeId> LastStep() => Fragments[^1].Steps[^1];
        
        public LegPlan()
        {
            this.Fragments = new List<LegFragment<TNodeId,TRoadId>>();
            this.Attractions = new List<PlacedAttraction>();
        }

        public string? DEBUG_Validate(int legIndex)
        {
            var distances = Length.Zero;
            for (int i=0;i<Fragments.Count;++i)
            {
                var fragment = this.Fragments[i];
                
                distances += fragment.UnsimplifiedFlatDistance;
                var failure = fragment.DEBUG_Validate(legIndex,i);
                if (failure != null)
                    return failure;

                if (i > 0 && Fragments[i - 1].Steps.Last().Point != fragment.Steps.First().Point)
                    return $"We have gap between fragments {i-1} and {i} at leg {legIndex}.";
            }

            if ((distances - UnsimplifiedDistance).Abs() > RoutePlan.ValidationLengthErrorLimit)
                return ($"{this}.{nameof(UnsimplifiedDistance)}={UnsimplifiedDistance} when sum of elements = {distances}");

            return null;
        }

        public void ComputeLegTotals()
        {
            var distance = Length.Zero;
            var time = TimeSpan.Zero;
            
            foreach (var fragment in Fragments)
            {
                distance += fragment.UnsimplifiedFlatDistance;
                time += fragment.RawTime;
            }

            this.UnsimplifiedDistance = distance;
            this.RawTime = time;
        }

        public bool NeedsRebuild(bool calcReal)
        {
            return this== Missing || (this.IsDrafted && calcReal);
        }
    }
}
