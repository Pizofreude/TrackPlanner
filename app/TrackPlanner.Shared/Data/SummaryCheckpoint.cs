﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public sealed class SummaryCheckpoint
    {
        public string Code { get; set; } 
        public string Label { get; set; }
        public TimeSpan Arrival { get; set; }
        public GeoZPoint UserPoint { get; set; }
        public TimeSpan Departure { get; set; }
        public Length IncomingDistance { get; set; }
        public TimeSpan TotalBreak { get; set; } // including events
        public TimeSpan RollingTime { get; set; }
        public int? IncomingLegIndex { get; set; }
        //public bool IsLooped { get; set; }
        public int[] EventCounters { get; set; }
        public ElevationStats ElevationStats { get; set; }

        public IEnumerable<(string label, string iconClass, TimeSpan duration)> GetAtomicEvents(SummaryJourney summary) =>
            Enumerable.Range(0,EventCounters.Length).SelectMany(it => Enumerable.Range(0,this.EventCounters[ it])
                    .Select(_ => (  Label: summary.PlannerPreferences.TripEvents[it].Group,
                        IconClass: summary.PlannerPreferences.TripEvents[it].ClassIcon, 
                        summary.PlannerPreferences.TripEvents[it].Duration)))
            .OrderBy(it => it.Item1);
        
        public SummaryCheckpoint(int eventsCount)
        {
            Label = "";
            Code = "";
            this.EventCounters = new int[eventsCount];
        }
    }
}
