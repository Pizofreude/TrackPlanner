﻿using Geo;
using MathUnit;

#nullable enable

namespace TrackPlanner.Shared.Data
{
    public readonly record struct Region
    {
        public Angle North { get; }
        public Angle East { get; }
        public Angle South { get; }
        public Angle West { get; }

        public Region(Angle north, Angle east, Angle south, Angle west)
        {
            North = north;
            East = east;
            South = south;
            West = west;
        }

        public bool IsWithin(GeoPoint point)
        {
            return point.Latitude <= North && point.Latitude >= South && point.Longitude <= East && point.Longitude >= West;
        }
    }
}