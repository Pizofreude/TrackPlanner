using System;
using Geo;

namespace TrackPlanner.Shared.Data
{
   

    public interface IAnchor : IReadOnlyAnchor
    {
        new TimeSpan UserBreak { get; set; }
        new string Label { get; set; }
        new bool IsUserLabel { get; set; }
        new bool IsPinned { get; set; }
        new GeoPoint UserPoint { get; set; }
    }
 
}