using System;
using Geo;

namespace TrackPlanner.Shared.Data
{
    public interface IReadOnlyAnchor
    {
        TimeSpan UserBreak { get; }
        string Label { get; }
        bool IsUserLabel { get; }
        bool IsPinned { get; }
        GeoPoint UserPoint { get;  }
    }

    
 
}