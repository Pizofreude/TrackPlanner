﻿namespace TrackPlanner.Shared.Data
{
    public sealed  class DirectoryData
    {
        public string[] Directories { get; set; } = default!;
        public string[] Files { get; set; } = default!;

        public DirectoryData()
        {
        }
    }
}
