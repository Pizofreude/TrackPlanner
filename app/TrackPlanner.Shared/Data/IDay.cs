using System;
using System.Collections.Generic;

namespace TrackPlanner.Shared.Data
{
   
    public interface IReadOnlyDay
    {
        TimeSpan Start { get; }
        IReadOnlyList<IReadOnlyAnchor> Anchors { get; }
    }
  
    public interface IDay<TAnchor> : IReadOnlyDay
    where TAnchor : IAnchor
    {
        new TimeSpan Start { get; set; }
        new List<TAnchor> Anchors { get; }
    }
}