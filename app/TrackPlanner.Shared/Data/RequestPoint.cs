﻿using Geo;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct RequestPoint<TNodeId> 
    where TNodeId:struct
    {
        public GeoPoint UserPoint { get; init; }
        public bool EnforcePoint { get; init; }
        public bool AllowSmoothing { get; init; }
        public bool FindLabel { get; init; }

        public RequestPoint(GeoPoint userPoint,  bool allowSmoothing, bool findLabel)
        {
            UserPoint = userPoint;
            this.AllowSmoothing = allowSmoothing;
            FindLabel = findLabel;
            EnforcePoint = false;
        }
    }
}