﻿using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct FragmentStep<TNodeId>
        where TNodeId : struct
    {
        public GeoZPoint Point { get; init; }
        public TNodeId? NodeId { get; init; }
        public Length IncomingFlatDistance { get; init; }

        public FragmentStep( GeoZPoint point, TNodeId? nodeId, Length incomingFlatDistance)
        {
            Point = point;
            NodeId = nodeId;
            IncomingFlatDistance = incomingFlatDistance;
        }

        public FragmentStep( MapPoint<TNodeId> mapPoint,Length incomingFlatDistance) 
            : this(mapPoint.Point,mapPoint.NodeId,incomingFlatDistance)
        {
        }

        public MapPoint<TNodeId> GetMapPoint()
        {
            return new MapPoint<TNodeId>(Point, NodeId);
        }
    }
    
    
}