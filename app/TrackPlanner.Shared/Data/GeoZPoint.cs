﻿using MathUnit;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

#nullable enable

namespace TrackPlanner.Shared.Data
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public readonly struct GeoZPoint : IEquatable<GeoZPoint>, ISerializable
    {
        //  public static GeoZPoint Invalid { get; } = FromDegreesMeters(100, 0, ushort.MaxValue);

        public static GeoZPoint FromDegreesMeters(double latitude, double longitude, double? altitude)
        {
            return new GeoZPoint((float) latitude, (float) longitude,
                altitude == null ? (ushort) 0 : scaleAltitude(altitude.Value));
        }

        private static ushort scaleAltitude(double altitudeMeters)
        {
            return (ushort) Math.Round((altitudeMeters - altitudeOffset) * altitudeScale);
        }

        public static GeoZPoint Create(Angle latitude, Angle longitude, Length? altitude)
        {
            return FromDegreesMeters(latitude.Degrees, longitude.Degrees, altitude?.Meters);
        }

        private readonly float latitudeDegrees;
        private readonly float longitudeDegrees;
        private const double altitudeOffset = -100;
        private const double altitudeScale = 7;
        private readonly ushort scaledAltitude;

        public Angle Latitude => Angle.FromDegrees(latitudeDegrees);
        public Angle Longitude => Angle.FromDegrees(longitudeDegrees);
        public Length Altitude => Length.FromMeters(this.scaledAltitude / altitudeScale + altitudeOffset);

        public bool HasAltitude => this.scaledAltitude > 0;
        
        private GeoZPoint(float latitude, float longitude, ushort scaledAltitude)
        {
            this.latitudeDegrees = latitude;
            this.longitudeDegrees = longitude;
            this.scaledAltitude = scaledAltitude;

        }

        public GeoZPoint WithAltitude(Length altitude)
        {
            return FromDegreesMeters(this.latitudeDegrees, this.longitudeDegrees, altitude.Meters);
        }

        public GeoZPoint(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            this.latitudeDegrees = info.GetSingle(nameof(Latitude));
            this.longitudeDegrees = info.GetSingle(nameof(Longitude));
            this.scaledAltitude =scaleAltitude( info.GetSingle(nameof(Altitude)));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue(nameof(Latitude), latitudeDegrees);
            info.AddValue(nameof(Longitude), longitudeDegrees);
            // serialization will be used for JSON as well, it means human-readable, so let's use regular meters
            info.AddValue(nameof(Altitude), (float) this.Altitude.Meters);
        }


        public void Deconstruct(out Angle latitude, out Angle longitude, out Length? altitude)
        {
            latitude = this.Latitude;
            longitude = this.Longitude;
            altitude = this.Altitude;
        }

        public override string ToString()
        {
            string result = $"{Latitude}, {Longitude}";
            if (HasAltitude)
                result += $", {Altitude}";
            return result;
        }

        public override bool Equals(object? obj)
        {
            return obj is GeoZPoint pt && Equals(pt);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.latitudeDegrees, this.longitudeDegrees, this.scaledAltitude);
        }

        public bool Equals(GeoZPoint obj)
        {
            return this.latitudeDegrees == obj.latitudeDegrees
                   && this.longitudeDegrees == obj.longitudeDegrees
                   && this.scaledAltitude == obj.scaledAltitude;
        }

        public static bool operator ==(in GeoZPoint a, in GeoZPoint b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(in GeoZPoint a, in GeoZPoint b)
        {
            return !(a == b);
        }

        public static void WriteFloatAngle(BinaryWriter writer, Angle angle)
        {
            writer.Write((float) angle.Degrees);
        }

        public static Angle ReadFloatAngle(BinaryReader reader)
        {
            return Angle.FromDegrees(reader.ReadSingle());
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(latitudeDegrees);
            writer.Write(longitudeDegrees);
            writer.Write(this.scaledAltitude);
        }

        public static GeoZPoint Read(BinaryReader reader)
        {
            var lat = reader.ReadSingle();
            var lon = reader.ReadSingle();
            var alt = reader.ReadUInt16();

            var point = new GeoZPoint(lat, lon, alt);
            return point;
        }
    }
}