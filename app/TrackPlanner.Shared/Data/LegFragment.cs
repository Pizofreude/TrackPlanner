﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public sealed class LegFragment<TNodeId,TRoadId>
    where TNodeId:struct
    {
        public Risk Risk { get; set; }
        public SpeedMode Mode { get; set; }
        public bool IsForbidden { get; set; }
        public List<FragmentStep<TNodeId>> Steps { get; set; }
        public Length UnsimplifiedFlatDistance { get; set; } // distance before points removal (simplification)
        public TimeSpan RawTime { get; set; }
        public HashSet<TRoadId> RoadIds { get; set; }

        public LegFragment()
        {
            this.RoadIds = new HashSet<TRoadId>();
            this.Steps = new List<FragmentStep<TNodeId>>();
        }

        public LegFragment<TNodeId,TRoadId> SetSpeedMode(SpeedMode mode)
        {
            this.Mode = mode;
            return this;
        }

        public string? DEBUG_Validate(int legIndex,int fragmentIndex)
        {
            var distances = Length.Zero;
            foreach (var step_dist in Steps.Select(it => it.IncomingFlatDistance))
            {
                distances += step_dist;
            }

           // if ((distances - UnsimplifiedDistance).Abs() > TrackPlan.LengthErrorLimit)
             //   throw new InvalidOperationException($"{this}.{nameof(UnsimplifiedDistance)}={UnsimplifiedDistance} when sum of elements = {distances}");

            if (Steps.Count <= 1)
                return $"Fragment {fragmentIndex} at {legIndex} leg contains less than 2 points.";
            
            return null;
        }
    }
}