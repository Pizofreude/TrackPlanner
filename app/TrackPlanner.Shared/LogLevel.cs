﻿namespace TrackPlanner.Shared
{
    public enum LogLevel
    {
        Verbose,
        Info,
        Warning,
        Error,
    }
}