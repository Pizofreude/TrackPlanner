﻿using System.Collections.Generic;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Requests
{
    public sealed class AttractionsRequest
    {
        // simplification, we only send anchor points, not actual route
        public List<GeoPoint> CheckPoints { get; set; } = default!;
        public Length Range { get; set; }
        public TouristAttraction.Feature ExcludeFeatures { get; set; }

        public AttractionsRequest()
        {
        }
    }
}