﻿using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Requests
{
    public sealed class PlanRequest<TNodeId>
        where TNodeId:struct
    {
        // the end of the given list is NOT repeated as the start of the following list
        public List<List<RequestPoint<TNodeId>>> DailyPoints { get; set; } = default!; 
        public UserRouterPreferences RouterPreferences { get; set; } = default!;
        public UserTurnerPreferences TurnerPreferences { get; set; } = default!;

        public PlanRequest()
        {
        }

        public IEnumerable<RequestPoint<TNodeId>> GetPointsSequence()
        {
            return DailyPoints.SelectMany(x => x);
        }

        public IEnumerable<RequestPoint<TNodeId>> GetSequenceWithAutoPoints<TRoadId>(RoutePlan<TNodeId, TRoadId> plan)
            where TRoadId: struct
        {
            int req_idx = 0;
            var req_points = GetPointsSequence().ToList();
            foreach (var leg in plan.Legs)
            {
                if (leg.AutoAnchored)
                    yield return new RequestPoint<TNodeId>(leg.FirstStep().Point.Convert(), 
                        allowSmoothing: true, findLabel: true);
                else
                    yield return req_points[req_idx++];
            }

            yield return req_points[^1];
        }
    }
}
