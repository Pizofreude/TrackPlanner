﻿using System.IO;
using System.Reflection;

namespace TrackPlanner.Shared
{
    public readonly record struct Navigator
    {
        public string BaseDirectory { get; }

        public Navigator(string? baseDirectory)
        {
            if (baseDirectory == null)
            {
                string bin_directory = Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!;
                baseDirectory = System.IO.Path.Combine(bin_directory, "../../../../..");
            }

            this.BaseDirectory = System.IO.Path.GetFullPath(baseDirectory);
        }

        public string GetMiniMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "app/mini-maps");
        }

        public string GetLegacyTracks()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "app/tracks");
        }

        public string GetCustomMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "maps");
        }

        public string GetOsmMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "osm");
        }

        public string GetSrtmMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "srtm");
        }

        public string GetOutput()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "output");
        }

        public string GetDebug()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "debug");
        }

        public void CreateSchedules()
        {
            System.IO.Directory.CreateDirectory(GetSchedules());
        }

        public string GetSchedules()
        {
            var schedules = System.IO.Path.Combine(this.BaseDirectory, "schedules");
            return schedules;
        }

        public string? GetDebug(bool enabled)
        {
            return enabled?GetDebug():null;
        }
    }
}