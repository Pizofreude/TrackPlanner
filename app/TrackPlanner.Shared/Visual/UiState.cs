using System.Collections.Generic;

namespace TrackPlanner.Shared.Visual
{
    public sealed class UiState
    {
        public List<bool> CollapsedDays { get;  }

        public UiState()
        {
            this.CollapsedDays = new List<bool>();
        }

        public void Assign(UiState state)
        {
            Assign(state.CollapsedDays);
        }
        public void Assign(IEnumerable<bool> days)
        {
            this.CollapsedDays.Clear();
            this.CollapsedDays.AddRange(days);
        }
    }
}