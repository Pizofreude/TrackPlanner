using System;
using System.Threading.Tasks;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Visual
{
    public interface IDataState
    {
        string Label { get; }
    }
  
}