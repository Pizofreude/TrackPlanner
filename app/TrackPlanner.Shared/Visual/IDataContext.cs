using System;

namespace TrackPlanner.Shared.Visual
{
    //public delegate void DataChangedEventHandler(object sender, string propertyName);
    public interface IDataContext
    {
        IDisposable NotifyOnChanging(object sender, string propertyName);
        void NotifyOnChanged(object sender, string propertyName);
    }

}