using System;
using System.Runtime.CompilerServices;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public sealed class VisualAnchor : IAnchor
    {
        private readonly IDataContext context;
        public bool IsUserLabel { get; set; }

        private TimeSpan userBreak;
        public TimeSpan UserBreak
        {
            get { return this.userBreak; }
            set
            {
                if (this.userBreak == value)
                    return;

                using (onDataChanging())
                {
                    this.userBreak = value;
                    onDataChanged();
                }
            }
        }

        private string label;
        public string Label
        {
            get { return this.label; }
            set
            {
                if (this.label==value)
                    return;

                using (onDataChanging())
                {
                    this.label = value;
                    this.IsUserLabel = true;
                    onDataChanged();
                }
            }
        }

        private bool isPinned;
        public bool IsPinned 
        {
            get { return this.isPinned; }
            set
            {
                if (this.isPinned == value) 
                    return;

                using (onDataChanging())
                {
                    this.isPinned = value;
                    onDataChanged();
                }
            }
        }

        private GeoPoint userPoint;
        public GeoPoint UserPoint  {
            get { return this.userPoint; }
            set
            {
                if (this.userPoint == value)
                    return;

                using (onDataChanging())
                {
                    this.userPoint = value;
                    onDataChanged();
                }
            }
        }

        internal VisualAnchor(IDataContext context,GeoPoint userPoint, string label,TimeSpan userBreak,bool isPinned,bool isUserLabel)
        {
            this.context = context;
            this.userPoint = userPoint;
            this.label = label;
            this.userBreak = userBreak;
            this.isPinned = isPinned;
            this.IsUserLabel = isUserLabel;
        }
        
        private void onDataChanged([CallerMemberName] string? name = null)
        {
            this.context.NotifyOnChanged(this, name!);
        }
        private IDisposable onDataChanging([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChanging(this, name!);
        }
    }
}