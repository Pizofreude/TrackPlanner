using System;
using System.Threading.Tasks;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Visual
{
    public sealed class StateManager<TState>
    where TState:IDataState
    {
        private sealed class HistoryScope : IDisposable
        {
            private readonly StateManager<TState> manager;

            public HistoryScope(StateManager<TState> manager)
            {
                this.manager = manager;
            }
            public void Dispose()
            {
                this.manager.stateHistory.Add(this.manager.stateGetter(this.manager.stateLabel));
                this.manager.alreadyExecuting = false;
            }
        }
        
        private bool alreadyExecuting;
        private readonly StateHistory<TState> stateHistory;
        private readonly Func<string,TState> stateGetter;
        private readonly Func<TState,ValueTask> stateAsyncSetter;
        private readonly HistoryScope scopeOpener;
        private string stateLabel;


        public bool CanUndo => this.stateHistory.CanUndo;
        public bool CanRedo => this.stateHistory.CanRedo;
        public string? UndoLabel => this.stateHistory.CanUndo ? this.stateHistory.PeekCurrent().Label : null;
        public string? RedoLabel => this.stateHistory.CanRedo ? this.stateHistory.PeekRedo().Label : null;


        public StateManager(Func<string,TState> stateGetter, Func<TState,ValueTask> stateAsyncSetter,int limit)
        {
            this.stateLabel = "";
            this.stateGetter = stateGetter;
            this.stateAsyncSetter = stateAsyncSetter;
            this.scopeOpener = new HistoryScope(this);
            
            this.stateHistory = new StateHistory<TState>(stateGetter(this.stateLabel),limit);
        }
        
        public IDisposable OpenScope(string label)
        {
            if (this.alreadyExecuting)
                return CompositeDisposable.None;
            else
            {
                this.stateLabel = label;
                this.alreadyExecuting = true;
                return this.scopeOpener;
            }
        }
        
        public ValueTask TryUndoAsync()
        {
            if (!this.stateHistory.CanUndo)
                return ValueTask.CompletedTask;

            return this.stateAsyncSetter(this.stateHistory.Undo());
        }

        public ValueTask TryRedoAsync()
        {
            if (!this.stateHistory.CanRedo)
                return ValueTask.CompletedTask;

            return this.stateAsyncSetter(this.stateHistory.Redo());
        }


    }
}