using System.Collections.Generic;

namespace TrackPlanner.Shared.Visual
{
    public sealed class StateHistory<T>
    {
        private readonly int limit;
        private readonly List<T> states;
        private int insertionIndex;

        public bool CanUndo => this.insertionIndex > 1;
        public bool CanRedo => this.insertionIndex < this.states.Count;

        public StateHistory(T initState,int limit)
        {
            this.limit = limit;
            this.states = new List<T>();
            Add(initState);
        }

        public void Add(T state)
        {
            if (this.insertionIndex < this.states.Count)
                this.states.RemoveRange(this.insertionIndex, this.states.Count - this.insertionIndex);
            if (this.states.Count == limit)
            {
                this.states.RemoveAt(0);
                --this.insertionIndex;
            }
            this.states.Add(state);
            ++this.insertionIndex;
        }

        public T Undo()
        {
            var state = PeekUndo();
            --this.insertionIndex;
            return state;
        }

        public T Redo()
        {
            var state = PeekRedo();
            ++this.insertionIndex;
            return state;
        }
        
        public T PeekUndo()
        {
            return this.states[this.insertionIndex - 2];
        }

        public T PeekCurrent()
        {
            return this.states[this.insertionIndex - 1];
        }

        public T PeekRedo()
        {
            return this.states[this.insertionIndex];
        }
    }
}