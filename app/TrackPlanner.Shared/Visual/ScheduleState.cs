using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Visual
{
    public readonly record struct ScheduleState<TVisualState,TNodeId,TRoadId>(string Label, TVisualState uiState, 
        string? filename,
        ScheduleJourney<TNodeId,TRoadId> schedule, 
        bool isModified) : IDataState
        where TNodeId:struct
        where TRoadId: struct
    {

    }
}