using System.Threading.Tasks;

namespace TrackPlanner.Shared.Visual
{
    public interface IVisualController :IDataContext
    {

        ValueTask OnAnchorInsertedAsync(VisualAnchor anchor, int dayIdx, int anchorIdx);
        ValueTask OnAnchorChangedAsync(int dayIndex, int anchorIndex);
        ValueTask OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex);
        ValueTask OnRawAnchorRemovingAsync(VisualAnchor anchor);
        ValueTask OnAnchorMovedAsync(VisualAnchor anchor);

        ValueTask OnPlanChangedAsync();

        void OnDaySplit(int dayIndex);
        void OnDayMerge(int dayIndex);

        ValueTask OnLegRemovingAsync(int legIndex);

        ValueTask OnAttractionsRemovingAsync(int firstLegIndex, int lastLegIndex);
        ValueTask OnRemovingLegAttractionsAsync(int legIndex);
        ValueTask OnAttractionsSetAsync();

        void OnSummarySet();

        void OnPlaceholderChanged();
        ValueTask OnLoopChangedAsync();

        ValueTask OnScheduleSettingAsync();
        ValueTask OnScheduleSetAsync();
    }
}