using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public sealed class VisualDay : IDay<VisualAnchor>,IVisualDay
    {
        private readonly IDataContext context;
      
        
        private static int DEBUG_idCounter;
        private readonly int DEBUG_id = DEBUG_idCounter++;
        
        public List<VisualAnchor> Anchors { get;  }
        IReadOnlyList<VisualAnchor> IVisualDay.Anchors => this.Anchors;
        IReadOnlyList<IReadOnlyAnchor> IReadOnlyDay.Anchors => this.Anchors;
        
        private TimeSpan start;
        public TimeSpan Start
        {
            get { return this.start; }
            set
            {
                if (this.start == value)
                    return;

                using (onDataChanging())
                {
                    this.start = value;
                    onDataChanged();
                }
            }
        }
        
       // internal event DataChangedEventHandler? DataChanged;
        
        internal VisualDay(IDataContext context,TimeSpan start) : this(context,start,new List<VisualAnchor>())
        {
        }
        internal VisualDay(IDataContext context, TimeSpan start,List<VisualAnchor> anchors)
        {
            this.context = context;
            this.start = start;
            this.Anchors = anchors;
        }
        private void onDataChanged([CallerMemberName] string? name = null)
        {
            this.context.NotifyOnChanged(this, name!);
        }
        private IDisposable onDataChanging([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChanging(this, name!);
        }

    }
}