using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Geo;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Visual
{
    public class VisualSchedule<TNodeId,TRoadId> : ISchedule<VisualDay, VisualAnchor,TNodeId,TRoadId>, IDataContext//, ILiveSchedule
        where TNodeId:struct
        where TRoadId: struct
    {
        private readonly CacheGetter<SummaryJourney> summary;

        public SummaryJourney Summary
        {
            get { return this.summary.Value; }
        }

        private readonly IVisualController visualController;
        public ScheduleSettings Settings { get; private set; }
        public (int day, int index)? AnchorPlaceholder { get; private set; }

        public UserPlannerPreferences PlannerPreferences => this.Settings.PlannerPreferences;
        public UserTurnerPreferences TurnerPreferences => this.Settings.TurnerPreferences;
        public UserRouterPreferences RouterPreferences => this.Settings.RouterPreferences;

        public List<VisualDay> Days { get; }
        IReadOnlyList<IReadOnlyDay> IReadOnlySchedule.Days => this.Days;
        //IReadOnlyList<IVisualDay> ILiveSchedule.Days => this.Days;

        public RoutePlan<TNodeId, TRoadId> Route { get; set; } = default!;

        public string? Filename { get; private set; }

        public bool IsModified { get; set; }

        public VisualSchedule( IVisualController visualController, ScheduleSettings settings)
        {
            this.visualController = visualController;
            this.summary = new CacheGetter<SummaryJourney>(this.GetSummary);
            Settings = settings;

            this.Days = new List<VisualDay>();
            
            initData();
        }


        /*private ValueTask onPropertyChangedAsync([CallerMemberName] string? name = null)
        {
            return this.visualController.OnPropertyChangedAsync(name!);
        }*/

        public async ValueTask StartNewAsync()
        {
            await this.visualController.OnAttractionsRemovingAsync(0, this.Route.Legs.Count);

            this.Filename = null;

            initData();

            this.IsModified = false;

            this.resetSummary();
        }

        private void initData()
        {
            this.Route = new RoutePlan<TNodeId, TRoadId>();

            this.Days.Clear();
            this.Days.Add(new VisualDay(this, PlannerPreferences.JourneyStart));
            this.AnchorPlaceholder = (0, 0);
        }

        private void refreshAnchorLabels(int legIndex, IReadOnlyList<string?> names)
        {
            ScheduleExtension.RefreshAnchorLabels(this, legIndex, names);
        }

        public async ValueTask SetRouteAsync(RoutePlan<TNodeId, TRoadId> route, IReadOnlyList<string?> labels)
        {
            this.Route = route;
            await this.rebuildAutoAnchorsAsync().ConfigureAwait(false);
            this.refreshAnchorLabels(0, labels);
        }


        public async ValueTask AssignAttractionsAsync(int dayIndex, IReadOnlyList<List<PlacedAttraction>> attractions)
        {
            var leg_idx = this.GetFirstLegIndex(dayIndex);
            var DEBUG_leg_count = this.GetLegCount(dayIndex);
            if (DEBUG_leg_count != attractions.Count)
                throw new ArgumentException($"Legs attractions mismatch {DEBUG_leg_count} vs. {attractions.Count}");
            for (int i = 0; i < attractions.Count; ++i)
                this.Route.Legs![leg_idx + i].Attractions = attractions[i];

            await this.visualController.OnAttractionsSetAsync().ConfigureAwait(false);
        }


        public async ValueTask SplitDayAsync(int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            this.AnchorPlaceholder = null;

            var curr_day = this.Days[dayIndex];
            var new_day = new VisualDay(this, dayIndex == 0 ? this.PlannerPreferences.NextDayStart : curr_day.Start);
            var split_idx = anchorIndex + 1;
            new_day.Anchors.AddRange(curr_day.Anchors.Skip(split_idx));
            curr_day.Anchors.RemoveRange(split_idx, curr_day.Anchors.Count - split_idx);

            curr_day.Anchors.Last().IsPinned = true;

            this.Days.Insert(dayIndex + 1, new_day);

            //return (curr_day, new_day);

            this.visualController.OnDaySplit(dayIndex);

            // start from the end of the day
            await this.visualController.OnAnchorChangedAsync(dayIndex, curr_day.Anchors.Count - 1).ConfigureAwait(false);
            this.resetSummary();
        }

        public async ValueTask MergeDayToPreviousAsync(int dayIndex)
        {
            this.IsModified = true;

            var prev_day = this.Days[dayIndex - 1];
            var prev_count = prev_day.Anchors.Count;
            var cur_day = this.Days[dayIndex];
            prev_day.Anchors.AddRange(cur_day.Anchors);
            this.Days.RemoveAt(dayIndex);

            this.AnchorPlaceholder = null;

            this.visualController.OnDayMerge(dayIndex);

            // start from the last anchor that day
            await this.visualController.OnAnchorChangedAsync(dayIndex - 1, prev_count - 1).ConfigureAwait(false);
            this.resetSummary();
        }

        public async ValueTask SetScheduleAsync(IReadOnlySchedule<TNodeId,TRoadId> source,string? schedulePath,bool modified)
        {
           await this.visualController.OnScheduleSettingAsync().ConfigureAwait(false);
         
           this.Filename = schedulePath;
           
            this.Settings = source.Settings;
            this.Route = source.Route;

            this.AnchorPlaceholder = null;

            this.Days.Clear();

            // we have to create a empty skeleton first so creating marker will know if the marker is important or not
            this.Days.AddRange(source.Days.Select(d =>
                new VisualDay(this, d.Start, d.Anchors.Select(_ =>
                    default(VisualAnchor)!).ToList())));

            int day_idx = -1;
            foreach (var day_src in source.Days)
            {
                ++day_idx;
                var day = this.Days[day_idx];
                int anchor_idx = -1;
                foreach (ScheduleAnchor anchor_src in day_src.Anchors)
                {
                    if (!string.IsNullOrEmpty(anchor_src.Label))
                    {
                        Console.WriteLine($"DEBUG Loaded non-empty label {anchor_src.Label}");
                    }

                    var vis_anchor = new VisualAnchor(this,
                        anchor_src.UserPoint,
                        anchor_src.Label,
                        anchor_src.UserBreak,
                        isPinned: anchor_src.IsPinned,
                        isUserLabel: anchor_src.IsUserLabel);

                    ++anchor_idx;
                    day.Anchors[anchor_idx] = vis_anchor;

                    await this.visualController.OnAnchorAddedAsync(vis_anchor, day_idx, anchor_idx).ConfigureAwait(false);
                }
            }

            this.IsModified = modified;

            this.resetSummary();

            await this.visualController.OnScheduleSetAsync().ConfigureAwait(false);
        }


        private void addStubLegForAnchor(int dayIndex, int anchorIndex)
        {
            if (this.HasEnoughPointsForBuild())
            {
                if (this.Route.IsEmpty()) // when starting with plan go easy and just remember to add loop leg
                {
                    this.Route.Legs.Insert(0, LegPlan<TNodeId, TRoadId>.Missing);
                    if (this.Settings.LoopRoute)
                        this.Route.Legs.Insert(0, LegPlan<TNodeId, TRoadId>.Missing);
                }
                else
                {
                    int leg_idx = this.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex) ?? 0;
                    this.Route.Legs.Insert(leg_idx, LegPlan<TNodeId, TRoadId>.Missing);
                }

                Console.WriteLine($"DEBUG AddMarkerAsync legs count {this.Route.Legs.Count}");
            }
        }


        private async ValueTask rebuildAutoAnchorsAsync()
        {
            // here we remove all existing auto anchors and recreate them
            // according to currently computed plan

            //  logger.LogDebug($"before RebuildAutoAnchors {this.DEBUG_PinsToString()}");

            for (int day_idx = 0; day_idx < this.Days.Count; ++day_idx)
            {
                var day = this.Days[day_idx];
                for (int anchor_idx = day.Anchors.Count - 1; anchor_idx >= 0; --anchor_idx)
                    if (!day.Anchors[anchor_idx].IsPinned)
                        await removeRawAnchorAsync(day_idx, anchor_idx).ConfigureAwait(false);
            }

            int leg_idx = 0;
            for (int day_idx = 0; day_idx < this.Days.Count; ++day_idx)
            {
                int anchor_starting_leg_idx = day_idx == 0 ? 0 : -1; // only first day has its own starting anchor

                // we need to iterate over fixed+1 legs because we don't know in advance how many auto legs
                // were added after last (valid for current day) fixed leg. So we will iterate into the first
                // leg of the next day
                int fixed_leg_count = this.GetLegCount(day_idx);
                // logger.LogDebug($"RebuildAutoAnchors Day {day_idx} with {fixed_leg_count} fixed legs, starting at {leg_idx}/{route.Legs.Count} leg");
                int DEBUG_iter = 0;
                for (; fixed_leg_count >= 0 && leg_idx < Route.Legs.Count; ++anchor_starting_leg_idx, ++DEBUG_iter)
                {
                    if (this.Route.Legs[leg_idx].AutoAnchored)
                    {
                        //logger.LogDebug($"Inserting auto anchors at {day_idx}/{anchor_starting_leg_idx}");
                        await insertAutoAnchorOnLegStartAsync(Route.Legs[leg_idx], day_idx,
                            anchor_starting_leg_idx).ConfigureAwait(false);
                    }
                    else
                    {
                        --fixed_leg_count;
                    }

                    ++leg_idx;
                }

                //  logger.LogDebug($"loop ended after {DEBUG_iter} iterations with leg_idx {leg_idx} and fixed len {fixed_leg_count}");
                --leg_idx; // on every day we go one (fixed) leg too far, so we need to go back one leg
            }

            //  logger.LogDebug($"after RebuildAutoAnchors {this.DEBUG_PinsToString()}");

            // 0,0 -- it is a lie, we simply need to recreate all marker titles
            await this.visualController.OnAnchorChangedAsync(0, 0).ConfigureAwait(false);
            this.resetSummary();
        }

        public async ValueTask ReplaceLegAsync(int legIndex,
            // first one is fixed, the following (if any) are auto-legs
            IEnumerable<LegPlan<TNodeId, TRoadId>> replacementLegs,
            IReadOnlyList<string?> names)
        {
            (int day_idx, int anchor_idx, int _) = this.LegIndexToStartingDayAnchor(legIndex);
            // first add auto-anchors
            foreach (var auto_leg in replacementLegs.Skip(1))
            {
                ++anchor_idx;
                await insertAutoAnchorOnLegStartAsync(auto_leg, day_idx, anchor_idx).ConfigureAwait(false);
            }

            this.Route.Legs.RemoveAt(legIndex);
            this.Route.Legs.InsertRange(legIndex, replacementLegs);
            
            refreshAnchorLabels(legIndex,names );
        }

        private ValueTask insertAutoAnchorOnLegStartAsync(LegPlan<TNodeId, TRoadId> leg, int dayIndex, int anchorIndex)
        {
            var coords = leg.FirstStep().Point.Convert();
            VisualAnchor anchor = insertRawAnchor(coords, "", dayIndex, anchorIndex, isPinned: false);

            return this.visualController.OnAnchorAddedAsync(anchor, dayIndex, anchorIndex);
        }

        private VisualAnchor insertRawAnchor(GeoPoint coords, string title,
            int dayIndex, int anchorIndex, bool isPinned)
        {
            var anchor = new VisualAnchor(this,
                coords,
                title,
                PlannerPreferences.DefaultAnchorBreak,
                isPinned: isPinned,
                isUserLabel: false);

            var day = this.Days[dayIndex];
            if (anchorIndex < 0 || anchorIndex > day.Anchors.Count)
                throw new ArgumentOutOfRangeException($"{nameof(anchorIndex)}={anchorIndex} out of range {day.Anchors.Count}.");

            this.IsModified = true;
            day.Anchors.Insert(anchorIndex, anchor);

            return anchor;
        }

        public async ValueTask ClearAttractionsAsync(int dayIndex)
        {
            int first_leg_idx = this.GetFirstLegIndex(dayIndex);
            int last_leg_idx = first_leg_idx + this.GetLegCount(dayIndex);

            await visualController.OnAttractionsRemovingAsync(first_leg_idx, last_leg_idx).ConfigureAwait(false);

            for (int leg_idx = first_leg_idx; leg_idx < last_leg_idx; ++leg_idx)
            {
                this.Route.Legs[leg_idx].Attractions.Clear();
            }
        }

        public async ValueTask PinMarkerAsync(int dayIndex, int anchorIndex)
        {
            var anchor = this.Days[dayIndex].Anchors[anchorIndex];
            anchor.IsPinned = true;

            await this.visualController.OnAnchorChangedAsync(dayIndex, anchorIndex).ConfigureAwait(false);
            this.resetSummary();
        }

        public async ValueTask RemoveSingleAnchorAsync(int dayIndex, int anchorIndex)
        {
            int DEBUG_leg_count = this.Route.Legs.Count;

            var day = this.Days[dayIndex];

            (bool result, anchorIndex, int incoming_leg_idx) = await resetAnchorSurroundingsAsync(dayIndex, anchorIndex, removeAttractions: true).ConfigureAwait(false);
            if (result)
            {
                if (incoming_leg_idx < this.Route.Legs.Count - 1)
                    await removeLegAsync(incoming_leg_idx + 1).ConfigureAwait(false);
            }

            await removeRawAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

            await this.visualController.OnPlanChangedAsync().ConfigureAwait(false);

            resetSummary();
            
            //   StateHasChanged();

            //  logger.LogDebug($"After deleting marker we have {this.route.Legs.Count}, previously {DEBUG_leg_count}");
        }

        private async ValueTask removeRawAnchorAsync(int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            var anchor = this.Days[dayIndex].Anchors[anchorIndex];

            await this.visualController.OnRawAnchorRemovingAsync(anchor).ConfigureAwait(false);
            this.Days[dayIndex].Anchors.RemoveAt(anchorIndex);

            var day = this.Days[dayIndex];

            if (day.Anchors.Count == 0 && this.Days.Count > 1)
            {
                this.Days.RemoveAt(dayIndex);
                if (anchor.IsPinned)
                    this.AnchorPlaceholder = null;
            }
            else
            {
                if (anchor.IsPinned)
                    this.AnchorPlaceholder = (dayIndex, anchorIndex);
            }
        }

        private void resetSummary()
        {
            if (!this.summary.Reset())
                return;

            this.visualController.OnSummarySet();
        }

        public async ValueTask<bool> TryConvertAttractionToAnchor(PlacedAttraction attraction)
        {
            if (!this.FindAttrIndex(attraction, out var leg_idx, out var attr_idx))
                return false;

            this.IsModified = true;

            List<PlacedAttraction> leg_attr = this.Route.Legs[leg_idx].Attractions;
            var before_attr = leg_attr.Take(attr_idx).ToList();
            var after_attr = leg_attr.Skip(attr_idx + 1).ToList();

            string? title = attraction.Attraction.Name;
            var features = String.Join(", ", attraction.Attraction.GetFeatures());
            if (title == null)
                title = features;
            else
                title += $" ({features})";

            var (day_idx, anchor_idx, _) = this.LegIndexToStartingDayAnchor(leg_idx);
            ++anchor_idx;
            var anchor = insertRawAnchor(attraction.Place.Point.Convert(), title, day_idx, anchor_idx, isPinned: true);
            addStubLegForAnchor(day_idx, anchor_idx);

            await this.visualController.OnAnchorInsertedAsync(anchor, day_idx, anchor_idx).ConfigureAwait(false);

            this.Route.Legs[leg_idx].Attractions = before_attr;
            this.Route.Legs[leg_idx + 1].Attractions = after_attr;

            await this.resetAnchorSurroundingsAsync(day_idx, anchor_idx, removeAttractions: false).ConfigureAwait(false);

            // start from previous one this day, because we could add at the end of the day, meaning we need to change the icon
            // of the previous marker from "final" to "regular"
            //           await RecreateMarkersAsync(dayIndex, Math.Max(0, anchorIndex - 1));

            // at this point marker reference is invalidated because of the refresh

            await visualController.OnPlanChangedAsync().ConfigureAwait(false);

            //         StateHasChanged();


            return true;
        }

        private async ValueTask resetLegAsync(int legIndex, bool removeAttractions)
        {
            if (removeAttractions)
                await this.visualController.OnRemovingLegAttractionsAsync(legIndex).ConfigureAwait(false);

            this.Route.Legs[legIndex] = LegPlan<TNodeId, TRoadId>.Missing;
        }

        private async ValueTask<(bool, int anchorIndex, int incomingLegIndex)> resetAnchorSurroundingsAsync(int dayIndex,
            int anchorIndex, bool removeAttractions)
        {
            if (this.Route.IsEmpty())
            {
                return (false, anchorIndex, -1);
            }

            const int incoming_pre_leg = -1;

            var incomingLegIndex = this.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex) ?? incoming_pre_leg;
            //logger.LogDebug($"Reset adjacent legs leg:{incomingLegIndex}/{this.Route.Legs.Count} anchor:{anchorIndex}");
            if (incomingLegIndex >= 0) // incoming
                await resetLegAsync(incomingLegIndex, removeAttractions).ConfigureAwait(false);
            if (incomingLegIndex < this.Route.Legs.Count - 1) // outgoing
                await resetLegAsync(incomingLegIndex + 1, removeAttractions).ConfigureAwait(false);

            if (dayIndex == 0 && anchorIndex == 0 && this.Settings.LoopRoute) // looped
                await resetLegAsync(this.Route.Legs.Count - 1, removeAttractions).ConfigureAwait(false);


            {
                var anchor_idx_next = anchorIndex + 1;
                var day_idx_next = dayIndex;
                // if the current anchor is last of the day we have to remove adjacent auto-anchors
                // from the next day
                if (anchor_idx_next == this.Days[day_idx_next].Anchors.Count && day_idx_next < this.Days.Count - 1)
                {
                    ++day_idx_next;
                    anchor_idx_next = 0;
                }

                var anchors = this.Days[day_idx_next].Anchors;
                // logger.LogDebug($"Reset auto anchors {anchors.Count} {String.Join(",", anchors.Select(it => it.IsPinned))}");
                while (anchor_idx_next < anchors.Count && !anchors[anchor_idx_next].IsPinned) // removing next auto-anchors
                {
                    //   logger.LogDebug($"Removing next auto anchor at {day_idx_next}:{anchor_idx_next} with leg {incomingLegIndex + 2}");
                    await removeRawAnchorAsync(day_idx_next, anchor_idx_next).ConfigureAwait(false);
                    await removeLegAsync(incomingLegIndex + 2);
                }
            }


            {
                var is_loop_start = dayIndex == 0 && anchorIndex == 0 && this.Settings.LoopRoute;

                var anchors = this.Days[dayIndex].Anchors;
                if (is_loop_start)
                {
                    dayIndex = this.Days.Count - 1;
                    anchors = this.Days[dayIndex].Anchors;

                    anchorIndex = anchors.Count; // set anchor as next to last
                    incomingLegIndex = this.Route.Legs.Count;
                    // logger.LogDebug($"switch to the end of anchors {dayIndex}:{anchorIndex}");
                }

                while (anchorIndex > 0 && !anchors[anchorIndex - 1].IsPinned) // removing previous auto-anchors
                {
                    // logger.LogDebug($"Removing prev auto anchor at {anchorIndex - 1} with leg {incomingLegIndex - 1}");
                    await removeRawAnchorAsync(dayIndex, anchorIndex - 1).ConfigureAwait(false);
                    await removeLegAsync(incomingLegIndex - 1).ConfigureAwait(false);
                    --incomingLegIndex;
                    --anchorIndex;
                }

                if (is_loop_start)
                {
                    anchorIndex = 0;
                    incomingLegIndex = incoming_pre_leg;
                }
            }

            // logger.LogDebug($"resetAnchorSurroundings finished with {this.DEBUG_PinsToString()}");

            return (true, anchorIndex, incomingLegIndex);
        }

        private async ValueTask removeLegAsync(int legIndex)
        {
            this.IsModified = true;
            await this.visualController.OnLegRemovingAsync(legIndex).ConfigureAwait(false);
            this.Route.Legs.RemoveAt(legIndex);
        }

        public async ValueTask InsertAnchorAtPlaceholderAsync(GeoPoint coords, string title)
        {
            if (this.AnchorPlaceholder == null)
                return;

            var (day_idx, anchor_idx) = this.AnchorPlaceholder.Value;
            this.AnchorPlaceholder = (day_idx, anchor_idx + 1);

            VisualAnchor anchor = insertRawAnchor(coords, title, day_idx, anchor_idx, isPinned: true);

            await this.visualController.OnAnchorAddedAsync(anchor, day_idx, anchor_idx).ConfigureAwait(false);

            addStubLegForAnchor(day_idx, anchor_idx);

            (_, anchor_idx, _) = await resetAnchorSurroundingsAsync(day_idx, anchor_idx,
                removeAttractions: true).ConfigureAwait(false);

            // start from previous one this day, because we could add at the end of the day, meaning we need to change the icon
            // of the previous marker from "final" to "regular"
            // await MapManager.RecreateMarkersAsync(dayIndex, Math.Max(0, anchorIndex - 1));

            // at this point marker reference is invalidated because of the refresh

            await visualController.OnPlanChangedAsync().ConfigureAwait(false);
            
            resetSummary();
        }

        public async ValueTask SetAnchorPositionAsync(GeoPoint position, int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            var anchor = this.Days[dayIndex].Anchors[anchorIndex];

            anchor.UserPoint = position;
            if (!anchor.IsUserLabel)
            {
                anchor.Label = "";
                anchor.IsUserLabel = false;
            }

            await resetAnchorSurroundingsAsync(dayIndex, anchorIndex, removeAttractions: true).ConfigureAwait(false);

            await visualController.OnAnchorMovedAsync(anchor).ConfigureAwait(false);
        }

        public void SetAnchorPlaceholder(int dayIndex, int anchorIndex)
        {
            this.AnchorPlaceholder = (dayIndex, anchorIndex);
            //logger.LogDebug($"DEBUG setting insert at {this.VisualSchedule.anchorPlaceholder}");

            this.visualController.OnPlaceholderChanged();
        }

        public void DeletePlaceholder()
        {
            this.AnchorPlaceholder = null;
            this.visualController.OnPlaceholderChanged();
        }

        private async Task onLoopedChangeAsync(bool newValue)
        {
            if (!this.Route.IsEmpty())
            {
                if (newValue)
                {
                    this.Route.Legs.Add(LegPlan<TNodeId, TRoadId>.Missing);
                }
                else
                {
                    while (true)
                    {
                        await this.removeLegAsync(this.Route.Legs.Count - 1).ConfigureAwait(false);
                        var anchors = this.Days[^1].Anchors;
                        if (anchors[^1].IsPinned)
                            break;
                        await this.removeRawAnchorAsync(this.Days.Count - 1, anchors.Count - 1).ConfigureAwait(false);
                    }
                }
            }
            
            await this.visualController.OnLoopChangedAsync().ConfigureAwait(false);
        }


        public async ValueTask SetSettingsAsync(ScheduleSettings prefs)
        {
            var previous = this.Settings;
            this.Settings = prefs;
            if (previous.LoopRoute != prefs.LoopRoute)
                await onLoopedChangeAsync(prefs.LoopRoute).ConfigureAwait(false);
            if (this.RouterPreferences.UseStableRoads != previous.RouterPreferences.UseStableRoads)
                await this.visualController.OnPlanChangedAsync().ConfigureAwait(false);
            
            this.resetSummary();
        }

        public void NotifyOnChanged(object sender, string propertyName)
        {
            if ((sender is VisualAnchor anchor && propertyName == nameof(anchor.UserBreak))
                || (sender is VisualDay day && propertyName == nameof(day.Start)))
                resetSummary();
            
            this.visualController.NotifyOnChanged(sender,propertyName);
        }

        public IDisposable NotifyOnChanging(object sender, string propertyName)
        {
            IsModified = true;
            
            return this.visualController.NotifyOnChanging(sender,propertyName);
        }

        public void DataSaved(string schedulePath)
        {
            if (this.Filename == null)
                this.Filename = schedulePath;
            IsModified = false;
        }
        
        private void onDataChanged([CallerMemberName] string? name = null)
        {
            this.NotifyOnChanged(this,name!);
        }
        private IDisposable onDataChanging([CallerMemberName] string? name = null)
        {
            return this.NotifyOnChanging(this,name!);
        }

    }
}