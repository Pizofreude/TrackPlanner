﻿using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared
{
    public sealed class DraftHelper<TNodeId,TRoadId>
    where TNodeId:struct
    where TRoadId: struct
    {
        private readonly IGeoCalculator calc;

        public DraftHelper(IGeoCalculator calc)
        {
            this.calc = calc;
        }
     

        
      /*  private void splitIntoPieces(LegFragment segment, int startIndex, UserPreferences userPrefs)
        {
            var dist = calc.GetDistance(segment.Places[startIndex].Point, segment.Places[startIndex+1].Point);
            var raw_time = dist / userPrefs.Speeds[segment.Mode];

            if (raw_time <= userPrefs.CheckpointIntervalLimit)
                return;

            var mid = this.calc.GetMidPoint(segment.Places[startIndex].Point, segment.Places[startIndex + 1].Point);
            segment.Places.Insert(startIndex+1,new MapPoint(mid,null));
            splitIntoPieces(segment,startIndex+1,userPrefs);
            splitIntoPieces(segment,startIndex,userPrefs);
        }*/
        
        private LegPlan<TNodeId,TRoadId> buildDraftLeg(GeoPoint start, GeoPoint end, UserRouterPreferences prefs)
        {
            var fragment = new LegFragment<TNodeId,TRoadId>()
                {
                    Steps = new List<FragmentStep<TNodeId>>()
                    {
                        new FragmentStep<TNodeId>(start.Convert(Length.Zero),null,Length.Zero),
                        new FragmentStep<TNodeId>(end.Convert(Length.Zero),null,calc.GetDistance(start.Convert(), end.Convert())),
                    }
                }
                .SetSpeedMode(SpeedMode.Ground);

           // splitIntoPieces(fragment, 0,userPrefs);
           fragment.UnsimplifiedFlatDistance = fragment.Steps.Select(it => it.IncomingFlatDistance).Sum();

           fragment.RawTime = prefs.GetRideTime(fragment.UnsimplifiedFlatDistance, fragment.Mode,
               Length.Zero, Length.Zero,out _);

            var leg = new LegPlan<TNodeId,TRoadId>()
            {
                IsDrafted = true,
                Fragments = new List<LegFragment<TNodeId,TRoadId>>() {fragment},
            };
            
            leg.ComputeLegTotals();
            
            return leg;
        }


        public  RouteResponse<TNodeId,TRoadId> BuildDraftPlan(PlanRequest<TNodeId> request)
        {
            var plan = new RoutePlan<TNodeId,TRoadId>()
            {
                Legs = new List<LegPlan<TNodeId,TRoadId>>(),
            };
            foreach (var (prev, next) in request.GetPointsSequence().Slide())
            {
                var leg = buildDraftLeg(prev.UserPoint, next.UserPoint, request.RouterPreferences);
                plan.Legs.Add(leg);
            }

            return new RouteResponse<TNodeId, TRoadId>()
            {
                Route = plan,
                Names = request.GetSequenceWithAutoPoints(plan).Select(_ => (string?)null).ToList(),
            };
        }

        
    }
}
