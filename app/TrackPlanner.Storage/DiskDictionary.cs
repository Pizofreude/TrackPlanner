﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using TrackPlanner.Structures;

namespace TrackPlanner.Storage
{
    public sealed class DiskDictionary<TKey,TValue> : IReadOnlyEnumerableDictionary<TKey, TValue>
        where TKey : notnull
    {
        // https://thargy.com/2014/03/memory-mapped-file-performance/
        
        private readonly CacheMap<TKey, TValue> cache;
        private readonly IReadOnlyList<ReaderOffsets<TKey>> sourceReaders;
        private readonly Func<TKey, IReadOnlyList<BinaryReader>, TValue> loader;

        // we store offsets for given entities, but in this particular dictionary
        // we might be interested only in some part of it, thus special extra offset
        private readonly int extraOffset;
        
        public TValue this[TKey key]
        {
            get
            {
                if (!TryGetValue(key, out var result))
                    throw new ArgumentException($"Key not found {key}");

                return result;
            }
        }
        
        public DiskDictionary(IReadOnlyList<ReaderOffsets<TKey>> sourceReaders, int extraOffset, 
            Func<TKey,IReadOnlyList<BinaryReader>,TValue> loader,Action<TValue> disposer, int memoryLimit)
        {
            this.sourceReaders = sourceReaders;
            this.extraOffset = extraOffset;
            this.loader = loader;
            this.cache = new CacheMap<TKey, TValue>(disposer, memoryLimit:memoryLimit);
        }

        public bool ContainsKey(TKey key)
        {
            // do NOT use our TryGetValue, because it actually reads the value from the disk
            // and here we are interested only whether the key exists

            if (this.cache.TryGetValue(key, out _))
            {
                return true;
            }

            foreach (var (_, offsets) in this.sourceReaders)
            {
                if (offsets.TryGetValue(key, out _))
                    return true;
            }

            return false;
        }
        
        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            if (this.cache.TryGetValue(key, out value))
            {
                return true;
            }

            var active = new List<BinaryReader >(capacity: this.sourceReaders.Count);

            foreach (var (reader, offsets) in this.sourceReaders)
            {
                if (!offsets.TryGetValue(key, out long offset))
                    continue;

                active.Add(reader);
                reader.BaseStream.Seek(offset+this.extraOffset, SeekOrigin.Begin);
            }

            if (active.Count == 0)
            {
                value = default;
                return false;
            }

            value = this.loader(key,active);

            this.cache.Add(key, value);
            
            return true;
        }

        public string GetStats()
        {
            return cache.GetStats();
        }

        public override string ToString()
        {
            return GetStats();
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            throw new NotSupportedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}