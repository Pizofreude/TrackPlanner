﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Turner.Implementation

{
    internal sealed class TrackNode <TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId:struct
    {
        public static TrackNode<TNodeId,TRoadId> Create(IWorldMap<TNodeId,TRoadId> map, TNodeId nodeId)
        {
            return new TrackNode<TNodeId,TRoadId>(map,nodeId, map.GetRoadsAtNode(nodeId)
                .GroupBy(it => it.RoadMapIndex)
                .ToDictionary(it => it.Key, it => it.Select(it => it.IndexAlongRoad).ToList().ReadOnlyList()));
        }

        private readonly IWorldMap<TNodeId,TRoadId> map;

        // road id -> indices along road nodes (1 or -- in case of roundabout start/end -- 2)
        private readonly IReadOnlyDictionary<TRoadId, IReadOnlyList<ushort>> dict;
        public TRoadId? RoundaboutId { get; }


        public TNodeId NodeId { get; }
        public GeoZPoint Point { get; }

        public int Count => this.dict.Count;

        public bool CycleWayExit { get; set; }
        public bool ForwardCycleWayCorrected { get; set; }
        public bool BackwardCycleWayCorrected { get; set; }
        public bool CyclewaySwitch { get; set; }
        public bool BackwardCyclewayUncertain { get; set; }
        public bool ForwardCyclewayUncertain { get; set; }
        public DirectionalArray<RoadIndexLong<TRoadId>> Segment { get; }

        private TrackNode(IWorldMap<TNodeId,TRoadId> map,TNodeId nodeId,
            // road id -> indices along road
            IReadOnlyDictionary<TRoadId, IReadOnlyList<ushort>> dict)
        {
            this.map = map;
            // remove all point-roads (like crossings)
            this.dict = dict
                .Where(it => map.GetRoad(it.Key).Nodes.Count > 1)
                .ToDictionary(it => it.Key, it => it.Value);

            if (this.dict.Values.Any(it => it.Count < 1 || it.Count > 2))
                throw new ArgumentException();

            NodeId = nodeId;
            
            this.Point = map.GetPoint(NodeId);

            foreach (var entry in this.dict)
            {
                if (map.GetRoad(entry.Key).IsRoundabout)
                {
                    RoundaboutId = entry.Key;
                    break;
                }
            }

            this.Segment = new DirectionalArray<RoadIndexLong<TRoadId>>();
        }

        public IReadOnlyDictionary<TRoadId, (ushort currentIndex, ushort nextIndex)> ShortestSegmentsIntersection(TrackNode<TNodeId,TRoadId> other)
        {
            var intersect = Linqer.Intersect(this.dict, other.dict, (a, b) => (a, b));
            // this is naive, we assume the highest index from current node and the lowest index from the next node are the closest ones
            // but roads can have loops and knots so this is VERY shaky

            // todo: compute all permutations and return the actual closests one, by computing segment distance
            // todo: this is wrong also because we cannot assume we are along (and not in reverse) of given road
            return intersect.ToDictionary(it => it.Key, it => (it.Value.a.OrderByDescending(x => x).First(), it.Value.b.OrderBy(x => x).First()));
        }

        internal bool IsDirectionAllowed(TRoadId roadId, in RoadIndexLong<TRoadId> dest)
        {
            return map.IsDirectionAllowed(this.map.GetRoadsAtNode(NodeId)
                .First(it => EqualityComparer<TRoadId>.Default.Equals( it.RoadMapIndex,roadId)), dest);
        }

    }
}
